extern Txt K;
extern Txt KTS__Get__PE;
extern Txt Kbracket;
extern Txt Kdt;
extern Txt KtheStamper;
extern Txt KtimeStamp;
extern Txt Kwait;
extern unsigned char D_proc_TS__GETTIMESTAMP__PE[];
void proc_TS__GETTIMESTAMP__PE( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_TS__GETTIMESTAMP__PE);
	if (!ctx->doingAbort) {
		Bool lsignaled__b;
		Txt lReturnedValue__t;
		Obj lsignal__o;
		Long lnumParameters__i;
		Long lDateTimeDecrement__i;
		Obj l__4D__auto__mutex__0;
		Txt lBracketOfValues__t;
		new ( outResult) Txt();
		{
			Long t0;
			t0=inNbExplicitParam;
			lnumParameters__i=t0.get();
		}
		lReturnedValue__t=K.get();
		if (1!=lnumParameters__i.get()) goto _3;
		c.f.fLine=35;
		lBracketOfValues__t=Parm<Txt>(ctx,inParams,inNbParam,1).get();
		if (ctx->doingAbort) goto _0;
		goto _2;
_3:
		if (2!=lnumParameters__i.get()) goto _4;
		c.f.fLine=38;
		lBracketOfValues__t=Parm<Txt>(ctx,inParams,inNbParam,1).get();
		if (ctx->doingAbort) goto _0;
		c.f.fLine=39;
		lDateTimeDecrement__i=Parm<Long>(ctx,inParams,inNbParam,2).get();
		if (ctx->doingAbort) goto _0;
		goto _2;
_4:
_2:
		{
			Obj t3;
			c.f.fLine=45;
			if (g->Call(ctx,(PCV[]){t3.cv()},0,1641)) goto _0;
			g->Check(ctx);
			lsignal__o=t3.get();
		}
		{
			Obj t4;
			c.f.fLine=47;
			if (g->Call(ctx,(PCV[]){t4.cv(),lsignal__o.cv()},1,1529)) goto _0;
			g->Check(ctx);
			l__4D__auto__mutex__0=t4.get();
		}
		c.f.fLine=48;
		if (g->SetMember(ctx,lsignal__o.cv(),Kbracket.cv(),lBracketOfValues__t.cv())) goto _0;
		c.f.fLine=49;
		if (g->SetMember(ctx,lsignal__o.cv(),Kdt.cv(),lDateTimeDecrement__i.cv())) goto _0;
		{
			Obj t5;
			l__4D__auto__mutex__0=t5.get();
		}
		c.f.fLine=52;
		if (g->Call(ctx,(PCV[]){nullptr,KtheStamper.cv(),KTS__Get__PE.cv(),lsignal__o.cv()},3,1389)) goto _0;
		g->Check(ctx);
		{
			Variant t6;
			c.f.fLine=54;
			if (g->Call(ctx,(PCV[]){t6.cv(),lsignal__o.cv(),Kwait.cv()},2,1498)) goto _0;
			g->Check(ctx);
			Bool t7;
			if (!g->GetValue(ctx,(PCV[]){t7.cv(),t6.cv(),nullptr})) goto _0;
			lsignaled__b=t7.get();
		}
		if (!(lsignaled__b.get())) goto _5;
		{
			Variant t8;
			c.f.fLine=57;
			if (g->GetMember(ctx,lsignal__o.cv(),KtimeStamp.cv(),t8.cv())) goto _0;
			Txt t9;
			if (!g->GetValue(ctx,(PCV[]){t9.cv(),t8.cv(),nullptr})) goto _0;
			lReturnedValue__t=t9.get();
		}
_5:
		c.f.fLine=62;
		Res<Txt>(outResult)=lReturnedValue__t.get();
_0:
_1:
;
	}

}
