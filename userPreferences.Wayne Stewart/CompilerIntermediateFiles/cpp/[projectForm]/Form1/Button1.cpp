extern Txt K1_2F5;
extern Txt K2_2F5;
extern Txt K3_2F5;
extern Txt K4_2F5;
extern Txt K5_2F5;
extern Txt KCol__01;
extern Txt KCol__02;
extern Txt KCol__03;
extern Txt KCol__04;
extern Txt KCol__05;
extern Txt KDuration_3A_20;
extern Txt KPreemptive;
extern Txt K_20ms_2E;
extern int32_t R_proc_TS__GETTIMESTAMP;
extern int32_t R_proc_TS__GETTIMESTAMP__PE;
extern unsigned char D_obj_p_Form1_00Button1[];
void obj_p_Form1_00Button1( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_obj_p_Form1_00Button1);
	if (!ctx->doingAbort) try {
		Long v0;
		Long v1;
		Long v2;
		Long v3;
		Long lStart;
		Ptr lCol__03__ptr;
		Long lMaxSize;
		Ptr lCol__04__ptr;
		Ptr lCol__05__ptr;
		Ptr lCol__01__ptr;
		Ptr lCol__02__ptr;
		Long lSize;
		Ptr lbutton__ptr;
		lMaxSize=100;
		{
			Ptr t0;
			c.f.fLine=7;
			if (g->Call(ctx,(PCV[]){t0.cv(),Long(3).cv(),KPreemptive.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lbutton__ptr=t0.get();
		}
		{
			Ptr t1;
			c.f.fLine=9;
			if (g->Call(ctx,(PCV[]){t1.cv(),Long(3).cv(),KCol__01.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__01__ptr=t1.get();
		}
		{
			Ref t2;
			c.f.fLine=10;
			if (!g->CastPointerToRef(ctx,7,(PCV[]){t2.cv(),lCol__01__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t2.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		{
			Ptr t3;
			c.f.fLine=12;
			if (g->Call(ctx,(PCV[]){t3.cv(),Long(3).cv(),KCol__02.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__02__ptr=t3.get();
		}
		{
			Ref t4;
			c.f.fLine=13;
			if (!g->CastPointerToRef(ctx,7,(PCV[]){t4.cv(),lCol__02__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t4.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		{
			Ptr t5;
			c.f.fLine=15;
			if (g->Call(ctx,(PCV[]){t5.cv(),Long(3).cv(),KCol__03.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__03__ptr=t5.get();
		}
		{
			Ref t6;
			c.f.fLine=16;
			if (!g->CastPointerToRef(ctx,7,(PCV[]){t6.cv(),lCol__03__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t6.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		{
			Ptr t7;
			c.f.fLine=18;
			if (g->Call(ctx,(PCV[]){t7.cv(),Long(3).cv(),KCol__04.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__04__ptr=t7.get();
		}
		{
			Ref t8;
			c.f.fLine=19;
			if (!g->CastPointerToRef(ctx,7,(PCV[]){t8.cv(),lCol__04__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t8.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		{
			Ptr t9;
			c.f.fLine=21;
			if (g->Call(ctx,(PCV[]){t9.cv(),Long(3).cv(),KCol__05.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__05__ptr=t9.get();
		}
		{
			Ref t10;
			c.f.fLine=22;
			if (!g->CastPointerToRef(ctx,7,(PCV[]){t10.cv(),lCol__05__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t10.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		{
			Long t11;
			c.f.fLine=24;
			if (g->Call(ctx,(PCV[]){t11.cv()},0,459)) goto _0;
			lStart=t11.get();
		}
		{
			Variant t12;
			c.f.fLine=27;
			if (!g->GetValue(ctx,(PCV[]){t12.cv(),lbutton__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			Bool t13;
			if (g->OperationOnAny(ctx,6,t12.cv(),Num(1).cv(),t13.cv())) goto _0;
			if (!(t13.get())) goto _2;
		}
		lSize=1;
		v0=100;
		goto _3;
_4:
		{
			Txt t14;
			t14=K1_2F5.get();
			Txt t15;
			c.f.fLine=29;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP__PE,1,1,(PCV[]){t15.cv(),t14.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t15.cv(),lCol__01__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Txt t16;
			t16=K2_2F5.get();
			Txt t17;
			c.f.fLine=30;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP__PE,1,1,(PCV[]){t17.cv(),t16.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t17.cv(),lCol__02__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Txt t18;
			t18=K3_2F5.get();
			Txt t19;
			c.f.fLine=31;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP__PE,1,1,(PCV[]){t19.cv(),t18.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t19.cv(),lCol__03__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Txt t20;
			t20=K4_2F5.get();
			Txt t21;
			c.f.fLine=32;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP__PE,1,1,(PCV[]){t21.cv(),t20.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t21.cv(),lCol__04__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Txt t22;
			t22=K5_2F5.get();
			Txt t23;
			c.f.fLine=33;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP__PE,1,1,(PCV[]){t23.cv(),t22.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t23.cv(),lCol__05__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		lSize=lSize.get()+1;
_3:
		if (lSize.get()<=v0.get()) goto _4;
		goto _5;
_2:
		lSize=1;
		v2=100;
		goto _6;
_7:
		{
			Txt t26;
			t26=K1_2F5.get();
			Txt t27;
			c.f.fLine=37;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP,1,1,(PCV[]){t27.cv(),t26.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t27.cv(),lCol__01__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Txt t28;
			t28=K2_2F5.get();
			Txt t29;
			c.f.fLine=38;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP,1,1,(PCV[]){t29.cv(),t28.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t29.cv(),lCol__02__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Txt t30;
			t30=K3_2F5.get();
			Txt t31;
			c.f.fLine=39;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP,1,1,(PCV[]){t31.cv(),t30.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t31.cv(),lCol__03__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Txt t32;
			t32=K4_2F5.get();
			Txt t33;
			c.f.fLine=40;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP,1,1,(PCV[]){t33.cv(),t32.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t33.cv(),lCol__04__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Txt t34;
			t34=K5_2F5.get();
			Txt t35;
			c.f.fLine=41;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP,1,1,(PCV[]){t35.cv(),t34.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t35.cv(),lCol__05__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		lSize=lSize.get()+1;
_6:
		if (lSize.get()<=v2.get()) goto _7;
_5:
		{
			Long t38;
			c.f.fLine=47;
			if (g->Call(ctx,(PCV[]){t38.cv()},0,459)) goto _0;
			Long t39;
			t39=t38.get()-lStart.get();
			Txt t40;
			if (g->Call(ctx,(PCV[]){t40.cv(),t39.cv()},1,10)) goto _0;
			Txt t41;
			g->AddString(KDuration_3A_20.get(),t40.get(),t41.get());
			Txt t42;
			g->AddString(t41.get(),K_20ms_2E.get(),t42.get());
			if (g->Call(ctx,(PCV[]){nullptr,t42.cv()},1,41)) goto _0;
			g->Check(ctx);
		}
_0:
_1:
;
	} catch( Asm4d_error e) { g->Error( ctx, e); }

}
