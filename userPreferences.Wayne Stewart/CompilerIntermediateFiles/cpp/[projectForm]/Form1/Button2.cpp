extern Txt K1_2F5;
extern Txt K2_2F5;
extern Txt K3_2F5;
extern Txt K4_2F5;
extern Txt K5_2F5;
extern Txt KCol__01;
extern Txt KCol__02;
extern Txt KCol__03;
extern Txt KCol__04;
extern Txt KCol__05;
extern Txt KPreemptive;
extern Txt kin89jWOO9_0;
extern int32_t R_proc_TS__GETTIMESTAMP;
extern int32_t R_proc_TS__GETTIMESTAMP__PE;
extern unsigned char D_obj_p_Form1_00Button2[];
void obj_p_Form1_00Button2( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_obj_p_Form1_00Button2);
	if (!ctx->doingAbort) try {
		Long lStart;
		Ptr lCol__03__ptr;
		Long lMaxSize;
		Ptr lCol__04__ptr;
		Ptr lCol__05__ptr;
		Ptr lCol__01__ptr;
		Ptr lCol__02__ptr;
		Long lSize;
		Ptr lbutton__ptr;
		lMaxSize=100000;
		{
			Ptr t0;
			c.f.fLine=5;
			if (g->Call(ctx,(PCV[]){t0.cv(),Long(3).cv(),KPreemptive.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lbutton__ptr=t0.get();
		}
		{
			Ptr t1;
			c.f.fLine=7;
			if (g->Call(ctx,(PCV[]){t1.cv(),Long(3).cv(),KCol__01.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__01__ptr=t1.get();
		}
		{
			Ref t2;
			c.f.fLine=8;
			if (!g->CastPointerToRef(ctx,7,(PCV[]){t2.cv(),lCol__01__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t2.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		{
			Ptr t3;
			c.f.fLine=10;
			if (g->Call(ctx,(PCV[]){t3.cv(),Long(3).cv(),KCol__02.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__02__ptr=t3.get();
		}
		{
			Ref t4;
			c.f.fLine=11;
			if (!g->CastPointerToRef(ctx,7,(PCV[]){t4.cv(),lCol__02__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t4.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		{
			Ptr t5;
			c.f.fLine=13;
			if (g->Call(ctx,(PCV[]){t5.cv(),Long(3).cv(),KCol__03.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__03__ptr=t5.get();
		}
		{
			Ref t6;
			c.f.fLine=14;
			if (!g->CastPointerToRef(ctx,7,(PCV[]){t6.cv(),lCol__03__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t6.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		{
			Ptr t7;
			c.f.fLine=16;
			if (g->Call(ctx,(PCV[]){t7.cv(),Long(3).cv(),KCol__04.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__04__ptr=t7.get();
		}
		{
			Ref t8;
			c.f.fLine=17;
			if (!g->CastPointerToRef(ctx,7,(PCV[]){t8.cv(),lCol__04__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t8.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		{
			Ptr t9;
			c.f.fLine=19;
			if (g->Call(ctx,(PCV[]){t9.cv(),Long(3).cv(),KCol__05.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__05__ptr=t9.get();
		}
		{
			Ref t10;
			c.f.fLine=20;
			if (!g->CastPointerToRef(ctx,7,(PCV[]){t10.cv(),lCol__05__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t10.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		{
			Long t11;
			c.f.fLine=24;
			if (g->Call(ctx,(PCV[]){t11.cv()},0,459)) goto _0;
			lStart=t11.get()+1000;
		}
		lSize=0;
		{
			Variant t13;
			c.f.fLine=27;
			if (!g->GetValue(ctx,(PCV[]){t13.cv(),lbutton__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			Bool t14;
			if (g->OperationOnAny(ctx,6,t13.cv(),Num(1).cv(),t14.cv())) goto _0;
			if (!(t14.get())) goto _2;
		}
_3:
		lSize=lSize.get()+1;
		{
			Txt t16;
			t16=K1_2F5.get();
			Txt t17;
			c.f.fLine=30;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP__PE,1,1,(PCV[]){t17.cv(),t16.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t17.cv(),lCol__01__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Txt t18;
			t18=K2_2F5.get();
			Txt t19;
			c.f.fLine=31;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP__PE,1,1,(PCV[]){t19.cv(),t18.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t19.cv(),lCol__02__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Txt t20;
			t20=K3_2F5.get();
			Txt t21;
			c.f.fLine=32;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP__PE,1,1,(PCV[]){t21.cv(),t20.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t21.cv(),lCol__03__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Txt t22;
			t22=K4_2F5.get();
			Txt t23;
			c.f.fLine=33;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP__PE,1,1,(PCV[]){t23.cv(),t22.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t23.cv(),lCol__04__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Txt t24;
			t24=K5_2F5.get();
			Txt t25;
			c.f.fLine=34;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP__PE,1,1,(PCV[]){t25.cv(),t24.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t25.cv(),lCol__05__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Long t26;
			c.f.fLine=35;
			if (g->Call(ctx,(PCV[]){t26.cv()},0,459)) goto _0;
			Bool t27;
			t27=t26.get()>lStart.get();
			Bool t28;
			t28=lSize.get()>=lMaxSize.get();
			if (!( t27.get()||t28.get())) goto _3;
		}
		goto _4;
_2:
_5:
		lSize=lSize.get()+1;
		{
			Txt t31;
			t31=K1_2F5.get();
			Txt t32;
			c.f.fLine=39;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP,1,1,(PCV[]){t32.cv(),t31.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t32.cv(),lCol__01__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Txt t33;
			t33=K2_2F5.get();
			Txt t34;
			c.f.fLine=40;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP,1,1,(PCV[]){t34.cv(),t33.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t34.cv(),lCol__02__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Txt t35;
			t35=K3_2F5.get();
			Txt t36;
			c.f.fLine=41;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP,1,1,(PCV[]){t36.cv(),t35.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t36.cv(),lCol__03__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Txt t37;
			t37=K4_2F5.get();
			Txt t38;
			c.f.fLine=42;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP,1,1,(PCV[]){t38.cv(),t37.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t38.cv(),lCol__04__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Txt t39;
			t39=K5_2F5.get();
			Txt t40;
			c.f.fLine=43;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP,1,1,(PCV[]){t40.cv(),t39.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t40.cv(),lCol__05__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Long t41;
			c.f.fLine=44;
			if (g->Call(ctx,(PCV[]){t41.cv()},0,459)) goto _0;
			Bool t42;
			t42=t41.get()>lStart.get();
			Bool t43;
			t43=lSize.get()>=lMaxSize.get();
			if (!( t42.get()||t43.get())) goto _5;
		}
_4:
		{
			Ref t45;
			c.f.fLine=47;
			if (!g->CastPointerToRef(ctx,7,(PCV[]){t45.cv(),lCol__01__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t45.cv(),lSize.cv()},2,222)) goto _0;
		}
		{
			Ref t46;
			c.f.fLine=48;
			if (!g->CastPointerToRef(ctx,7,(PCV[]){t46.cv(),lCol__02__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t46.cv(),lSize.cv()},2,222)) goto _0;
		}
		{
			Ref t47;
			c.f.fLine=49;
			if (!g->CastPointerToRef(ctx,7,(PCV[]){t47.cv(),lCol__03__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t47.cv(),lSize.cv()},2,222)) goto _0;
		}
		{
			Ref t48;
			c.f.fLine=50;
			if (!g->CastPointerToRef(ctx,7,(PCV[]){t48.cv(),lCol__04__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t48.cv(),lSize.cv()},2,222)) goto _0;
		}
		{
			Ref t49;
			c.f.fLine=51;
			if (!g->CastPointerToRef(ctx,7,(PCV[]){t49.cv(),lCol__05__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t49.cv(),lSize.cv()},2,222)) goto _0;
		}
		{
			Txt t50;
			c.f.fLine=55;
			if (g->Call(ctx,(PCV[]){t50.cv(),lSize.cv()},1,10)) goto _0;
			Txt t51;
			g->AddString(kin89jWOO9_0.get(),t50.get(),t51.get());
			if (g->Call(ctx,(PCV[]){nullptr,t51.cv()},1,41)) goto _0;
			g->Check(ctx);
		}
_0:
_1:
;
	} catch( Asm4d_error e) { g->Error( ctx, e); }

}
