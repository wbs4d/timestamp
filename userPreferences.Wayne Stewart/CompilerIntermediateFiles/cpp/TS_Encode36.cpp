extern Txt K;
extern Txt K0;
extern Txt ku4EHCbBe$PE;
extern unsigned char D_proc_TS__ENCODE36[];
void proc_TS__ENCODE36( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_TS__ENCODE36);
	if (!ctx->doingAbort) {
		Txt lReturnedValue__t;
		Txt lKey__t;
		Long lBase__i;
		Long lEncodingLength__i;
		Long lTheNumberToEncode__i;
		Long lNumber2__r;
		new ( outResult) Txt();
		c.f.fLine=51;
		lEncodingLength__i=Parm<Long>(ctx,inParams,inNbParam,1).get();
		if (ctx->doingAbort) goto _0;
		c.f.fLine=52;
		lTheNumberToEncode__i=Parm<Long>(ctx,inParams,inNbParam,2).get();
		if (ctx->doingAbort) goto _0;
		lKey__t=ku4EHCbBe$PE.get();
		{
			Long t0;
			t0=lKey__t.get().fLength;
			lBase__i=t0.get();
		}
		if (0<=lTheNumberToEncode__i.get()) goto _2;
		{
			Num t2;
			t2=lTheNumberToEncode__i.get();
			Num t3;
			t3=0x1p+32+t2.get();
			lTheNumberToEncode__i=(sLONG)lrint(t3.get());
		}
_2:
		lReturnedValue__t=K.get();
_3:
		if (1>lTheNumberToEncode__i.get()) goto _4;
		{
			Long t6;
			t6=lTheNumberToEncode__i.get()/lBase__i.get();
			Long t7;
			t7=lBase__i.get()*t6.get();
			Long t8;
			t8=lTheNumberToEncode__i.get()-t7.get();
			Long t9;
			t9=t8.get()+1;
			Txt t10;
			c.f.fLine=65;
			g->GetStringChar(ctx,lKey__t.get(),(sLONG) t9.get(),t10.get());
			g->AddString(t10.get(),lReturnedValue__t.get(),lReturnedValue__t.get());
		}
		lTheNumberToEncode__i=lTheNumberToEncode__i.get()/lBase__i.get();
		goto _3;
_4:
		{
			Long t13;
			t13=lReturnedValue__t.get().fLength;
			if (t13.get()<=lEncodingLength__i.get()) goto _5;
		}
		lReturnedValue__t=K.get();
		goto _6;
_5:
		{
			Long t15;
			t15=lReturnedValue__t.get().fLength;
			Long t16;
			t16=lEncodingLength__i.get()-t15.get();
			Txt t17;
			g->MultiplyString(K0.get(),(sLONG)t16.get(),t17.get());
			g->AddString(t17.get(),lReturnedValue__t.get(),lReturnedValue__t.get());
		}
_6:
		c.f.fLine=76;
		Res<Txt>(outResult)=lReturnedValue__t.get();
_0:
_1:
;
	}

}
