extern int32_t gTS__NoOfTimesCalledThisSecond__i;
extern int32_t gTS__SecondLastCalled__i;
extern Txt K;
extern Txt K_2F;
extern Txt kwWsRQdd4SCg;
Asm4d_Proc proc_TS__ENCODE36;
extern unsigned char D_proc_TS__GETTIMESTAMP[];
void proc_TS__GETTIMESTAMP( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_TS__GETTIMESTAMP);
	if (!ctx->doingAbort) {
		Txt lUUID__t;
		Long lNum;
		Long lj;
		Long lPosition__i;
		Txt lReturnedValue__t;
		Long lRange__i;
		Long lBase__i;
		Long lSeconds__i;
		Long lDateTimeDecrement__i;
		Long lDays__i;
		Long lCounter__i;
		Long lTemp__i;
		Txt lBracketOfValues__t;
		Long lStation__i;
		Long lStart__i;
		new ( outResult) Txt();
		lStation__i=0;
		lBase__i=1;
		lRange__i=800000;
		{
			Long t0;
			t0=inNbExplicitParam;
			if (1>t0.get()) goto _2;
		}
		c.f.fLine=46;
		lBracketOfValues__t=Parm<Txt>(ctx,inParams,inNbParam,1).get();
		if (ctx->doingAbort) goto _0;
		{
			Bool t2;
			t2=g->CompareString(ctx,lBracketOfValues__t.get(),K.get())!=0;
			if (!(t2.get())) goto _3;
		}
		{
			Long t3;
			c.f.fLine=48;
			if (g->Call(ctx,(PCV[]){t3.cv(),K_2F.cv(),lBracketOfValues__t.cv()},2,15)) goto _0;
			lPosition__i=t3.get();
		}
		{
			Long t4;
			t4=lPosition__i.get()+1;
			Txt t5;
			c.f.fLine=49;
			if (g->Call(ctx,(PCV[]){t5.cv(),lBracketOfValues__t.cv(),t4.cv()},2,12)) goto _0;
			Num t6;
			if (g->Call(ctx,(PCV[]){t6.cv(),t5.cv()},1,11)) goto _0;
			lBase__i=(sLONG)lrint(t6.get());
		}
		{
			Long t8;
			t8=lPosition__i.get()-1;
			Txt t9;
			c.f.fLine=50;
			if (g->Call(ctx,(PCV[]){t9.cv(),lBracketOfValues__t.cv(),Long(1).cv(),t8.cv()},3,12)) goto _0;
			Num t10;
			if (g->Call(ctx,(PCV[]){t10.cv(),t9.cv()},1,11)) goto _0;
			lStation__i=(sLONG)lrint(t10.get());
		}
		if (lBase__i.get()>=lStation__i.get()) goto _4;
		lTemp__i=lBase__i.get();
		lBase__i=lStation__i.get();
		lStation__i=lBase__i.get();
_4:
		{
			Num t13;
			t13=lBase__i.get();
			Num t14;
			t14=800000/t13.get();
			Num t15;
			t15=t14.get()+0x1.ff7ced916872bp-1;
			Num t16;
			c.f.fLine=56;
			if (g->Call(ctx,(PCV[]){t16.cv(),t15.cv()},1,8)) goto _0;
			lRange__i=(sLONG)lrint(t16.get());
		}
_3:
_2:
		{
			Long t18;
			t18=inNbExplicitParam;
			if (2>t18.get()) goto _5;
		}
		c.f.fLine=62;
		lDateTimeDecrement__i=Parm<Long>(ctx,inParams,inNbParam,2).get();
		if (ctx->doingAbort) goto _0;
		goto _6;
_5:
		lDateTimeDecrement__i=0;
_6:
		{
			Bool t20;
			c.f.fLine=68;
			if (g->Call(ctx,(PCV[]){t20.cv(),kwWsRQdd4SCg.cv(),Long(60).cv()},2,143)) goto _0;
			g->Check(ctx);
			Bool t21;
			t21=t20.get();
			Bool t22;
			t22=!(t21.get());
			if (!(t22.get())) goto _7;
		}
_9:
		{
			Date t23;
			c.f.fLine=70;
			if (g->Call(ctx,(PCV[]){t23.cv()},0,33)) goto _0;
			Num t24;
			t24=g->SubstractDate(t23.get(),Date(1,1,1990).get());
			lDays__i=(sLONG)lrint(t24.get());
		}
		{
			Time t26;
			c.f.fLine=71;
			if (g->Call(ctx,(PCV[]){t26.cv()},0,178)) goto _0;
			lSeconds__i=t26.get()-lDateTimeDecrement__i.get();
		}
		if (0==lDateTimeDecrement__i.get()) goto _11;
		{
			Num t29;
			t29=lSeconds__i.get();
			Num t30;
			t30=t29.get()/86400;
			Num t31;
			c.f.fLine=75;
			if (g->Call(ctx,(PCV[]){t31.cv(),t30.cv()},1,8)) goto _0;
			Num t32;
			t32=lDays__i.get();
			Num t33;
			t33=t32.get()+t31.get();
			lDays__i=(sLONG)lrint(t33.get());
		}
		{
			Num t35;
			t35=lSeconds__i.get();
			Num t36;
			t36=t35.get()/86400;
			Num t37;
			c.f.fLine=76;
			if (g->Call(ctx,(PCV[]){t37.cv(),t36.cv()},1,8)) goto _0;
			Num t38;
			t38=t37.get()*86400;
			Num t39;
			t39=lSeconds__i.get();
			Num t40;
			t40=t39.get()-t38.get();
			lSeconds__i=(sLONG)lrint(t40.get());
		}
_11:
		if (lSeconds__i.get()==Var<Long>(glob,gTS__SecondLastCalled__i).get()) goto _12;
		Var<Long>(glob,gTS__SecondLastCalled__i)=lSeconds__i.get();
		Touch(glob,gTS__SecondLastCalled__i);
		Var<Long>(glob,gTS__NoOfTimesCalledThisSecond__i)=1;
		Touch(glob,gTS__NoOfTimesCalledThisSecond__i);
		if (1!=lBase__i.get()) goto _13;
		{
			Long t44;
			c.f.fLine=84;
			if (g->Call(ctx,(PCV[]){t44.cv()},0,100)) goto _0;
			Long t45;
			t45=t44.get()%10000;
			Var<Long>(glob,gTS__NoOfTimesCalledThisSecond__i)=Var<Long>(glob,gTS__NoOfTimesCalledThisSecond__i).get()+t45.get();
			Touch(glob,gTS__NoOfTimesCalledThisSecond__i);
		}
_13:
		goto _14;
_12:
		Var<Long>(glob,gTS__NoOfTimesCalledThisSecond__i)=Var<Long>(glob,gTS__NoOfTimesCalledThisSecond__i).get()+1;
		Touch(glob,gTS__NoOfTimesCalledThisSecond__i);
_14:
_8:
		if (Var<Long>(glob,gTS__NoOfTimesCalledThisSecond__i).get()>lRange__i.get()) goto _9;
_10:
		{
			Long t49;
			t49=lStation__i.get()*lRange__i.get();
			lCounter__i=Var<Long>(glob,gTS__NoOfTimesCalledThisSecond__i).get()+t49.get();
		}
		c.f.fLine=91;
		if (g->Call(ctx,(PCV[]){nullptr,kwWsRQdd4SCg.cv()},1,144)) goto _0;
		g->Check(ctx);
		{
			Long t51;
			t51=lDays__i.get();
			Long t52;
			t52=3;
			Txt t53;
			c.f.fLine=93;
			proc_TS__ENCODE36(glob,ctx,2,2,(PCV[]){t52.cv(),t51.cv()},t53.cv());
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
			lReturnedValue__t=t53.get();
		}
		{
			Long t54;
			t54=lSeconds__i.get()*18;
			Long t55;
			t55=lCounter__i.get()/46000;
			Long t56;
			t56=t54.get()+t55.get();
			Long t57;
			t57=4;
			Txt t58;
			c.f.fLine=94;
			proc_TS__ENCODE36(glob,ctx,2,2,(PCV[]){t57.cv(),t56.cv()},t58.cv());
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
			g->AddString(lReturnedValue__t.get(),t58.get(),lReturnedValue__t.get());
		}
		{
			Long t60;
			t60=lCounter__i.get()%46000;
			Long t61;
			t61=3;
			Txt t62;
			c.f.fLine=95;
			proc_TS__ENCODE36(glob,ctx,2,2,(PCV[]){t61.cv(),t60.cv()},t62.cv());
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
			g->AddString(lReturnedValue__t.get(),t62.get(),lReturnedValue__t.get());
		}
		goto _15;
_7:
_15:
		c.f.fLine=152;
		Res<Txt>(outResult)=lReturnedValue__t.get();
_0:
_1:
;
	}

}
