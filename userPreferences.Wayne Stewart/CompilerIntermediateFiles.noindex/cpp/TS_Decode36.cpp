extern Txt ku4EHCbBe$PE;
extern unsigned char D_proc_TS__DECODE36[];
void proc_TS__DECODE36( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_TS__DECODE36);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Long v0;
		Long v1;
		Long lj;
		Long lk;
		Txt lKey__t;
		Long lBase__i;
		Long lEncodingLength__i;
		new ( outResult) Long();
		lKey__t=ku4EHCbBe$PE.get();
		{
			Long t0;
			t0=lKey__t.get().fLength;
			lBase__i=t0.get();
		}
		{
			Long t1;
			c.f.fLine=26;
			t1=Parm<Txt>(ctx,inParams,inNbParam,1).get().fLength;
			lEncodingLength__i=t1.get();
		}
		if (ctx->doingAbort) goto _0;
		c.f.fLine=28;
		Res<Long>(outResult)=0;
		lk=1;
		lj=lEncodingLength__i.get();
		v0=1;
		goto _2;
_4:
		{
			Txt t2;
			c.f.fLine=32;
			g->GetStringChar(ctx,Parm<Txt>(ctx,inParams,inNbParam,1).get(),(sLONG) lj.get(),t2.get());
			Long t3;
			if (g->Call(ctx,(PCV[]){t3.cv(),t2.cv(),lKey__t.cv()},2,15)) goto _0;
			Long t4;
			t4=t3.get()-1;
			Long t5;
			t5=t4.get()*lk.get();
			Res<Long>(outResult)=Res<Long>(outResult).get()+t5.get();
		}
		if (ctx->doingAbort) goto _0;
		lk=lk.get()*lBase__i.get();
_3:
		lj=lj.get()+-1;
_2:
		if (lj.get()>=v0.get()) goto _4;
_5:
		c.f.fLine=36;
		Res<Long>(outResult)=Res<Long>(outResult).get();
_0:
_1:
;
	}

}
