extern Txt K00;
extern Txt K0000;
extern Txt KT;
extern Txt K_2D;
Asm4d_Proc proc_TS__DECODE36;
extern unsigned char D_proc_TS__READTIMESTAMP[];
void proc_TS__READTIMESTAMP( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_TS__READTIMESTAMP);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Txt lTimeSection__t;
		Txt lDateSection__t;
		Long lnumberOfDays__i;
		Date ltheDate__d;
		Long lnumberOfSeconds__i;
		new ( outResult) Txt();
		c.f.fLine=26;
		{
			Txt t0;
			if (g->Call(ctx,(PCV[]){t0.cv(),Parm<Txt>(ctx,inParams,inNbParam,1).cv(),Long(1).cv(),Long(3).cv()},3,12)) goto _0;
			lDateSection__t=t0.get();
		}
		if (ctx->doingAbort) goto _0;
		c.f.fLine=27;
		{
			Txt t1;
			if (g->Call(ctx,(PCV[]){t1.cv(),Parm<Txt>(ctx,inParams,inNbParam,1).cv(),Long(4).cv(),Long(4).cv()},3,12)) goto _0;
			lTimeSection__t=t1.get();
		}
		if (ctx->doingAbort) goto _0;
		{
			Txt t2;
			t2=lDateSection__t.get();
			Long t3;
			c.f.fLine=29;
			proc_TS__DECODE36(glob,ctx,1,1,(PCV[]){t2.cv()},t3.cv());
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
			lnumberOfDays__i=t3.get();
		}
		{
			Txt t4;
			t4=lTimeSection__t.get();
			Long t5;
			c.f.fLine=30;
			proc_TS__DECODE36(glob,ctx,1,1,(PCV[]){t4.cv()},t5.cv());
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
			Num t6;
			t6=t5.get();
			Num t7;
			t7=t6.get()/18;
			lnumberOfSeconds__i=(sLONG)lrint(t7.get());
		}
		{
			Date t9;
			t9=g->AddToDate(Date(1,1,1990).get(),lnumberOfDays__i.get());
			ltheDate__d=t9.get();
		}
		{
			Date t10;
			t10=ltheDate__d.get();
			Long t11;
			c.f.fLine=34;
			if (g->Call(ctx,(PCV[]){t11.cv(),t10.cv()},1,25)) goto _0;
			Txt t12;
			if (g->Call(ctx,(PCV[]){t12.cv(),t11.cv(),K0000.cv()},2,10)) goto _0;
			Txt t13;
			g->AddString(t12.get(),K_2D.get(),t13.get());
			Date t14;
			t14=ltheDate__d.get();
			Long t15;
			if (g->Call(ctx,(PCV[]){t15.cv(),t14.cv()},1,24)) goto _0;
			Txt t16;
			if (g->Call(ctx,(PCV[]){t16.cv(),t15.cv(),K00.cv()},2,10)) goto _0;
			Txt t17;
			g->AddString(t13.get(),t16.get(),t17.get());
			Txt t18;
			g->AddString(t17.get(),K_2D.get(),t18.get());
			Date t19;
			t19=ltheDate__d.get();
			Long t20;
			if (g->Call(ctx,(PCV[]){t20.cv(),t19.cv()},1,23)) goto _0;
			Txt t21;
			if (g->Call(ctx,(PCV[]){t21.cv(),t20.cv(),K00.cv()},2,10)) goto _0;
			Txt t22;
			g->AddString(t18.get(),t21.get(),t22.get());
			Txt t23;
			g->AddString(t22.get(),KT.get(),t23.get());
			Txt t24;
			if (g->Call(ctx,(PCV[]){t24.cv(),lnumberOfSeconds__i.cv()},1,180)) goto _0;
			g->AddString(t23.get(),t24.get(),Res<Txt>(outResult).get());
		}
_0:
_1:
;
	}

}
