extern int32_t vTS__NoOfTimesCalledThisSecond__i;
extern int32_t vTS__SecondLastCalled__i;
extern Txt K;
extern Txt K_2F;
extern Txt Kbracket;
extern Txt Kdt;
extern Txt KtimeStamp;
extern Txt Ktrigger;
Asm4d_Proc proc_TS__ENCODE36;
extern unsigned char D_proc_TS__GET__PE[];
void proc_TS__GET__PE( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_TS__GET__PE);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Long lNum;
		Long lj;
		Long lPosition__i;
		Txt lReturnedValue__t;
		Long lRange__i;
		Long lBase__i;
		Long lSeconds__i;
		Long lDateTimeDecrement__i;
		Long lDays__i;
		Long lCounter__i;
		Long lTemp__i;
		Obj l__4D__auto__mutex__0;
		Txt lBracketOfValues__t;
		Long lStation__i;
		Long lStart__i;
		{
			Variant t0;
			c.f.fLine=9;
			if (g->GetMember(ctx,Parm<Obj>(ctx,inParams,inNbParam,1).cv(),Kbracket.cv(),t0.cv())) goto _0;
			Txt t1;
			if (!g->GetValue(ctx,(PCV[]){t1.cv(),t0.cv(),nullptr})) goto _0;
			lBracketOfValues__t=t1.get();
		}
		if (ctx->doingAbort) goto _0;
		{
			Variant t2;
			c.f.fLine=10;
			if (g->GetMember(ctx,Parm<Obj>(ctx,inParams,inNbParam,1).cv(),Kdt.cv(),t2.cv())) goto _0;
			Long t3;
			if (!g->GetValue(ctx,(PCV[]){t3.cv(),t2.cv(),nullptr})) goto _0;
			lDateTimeDecrement__i=t3.get();
		}
		if (ctx->doingAbort) goto _0;
		lStation__i=0;
		lBase__i=1;
		lRange__i=800000;
		{
			Bool t4;
			t4=g->CompareString(ctx,lBracketOfValues__t.get(),K.get())!=0;
			if (!(t4.get())) goto _2;
		}
		{
			Long t5;
			c.f.fLine=21;
			if (g->Call(ctx,(PCV[]){t5.cv(),K_2F.cv(),lBracketOfValues__t.cv()},2,15)) goto _0;
			lPosition__i=t5.get();
		}
		{
			Long t6;
			t6=lPosition__i.get()+1;
			Txt t7;
			c.f.fLine=22;
			if (g->Call(ctx,(PCV[]){t7.cv(),lBracketOfValues__t.cv(),t6.cv()},2,12)) goto _0;
			Num t8;
			if (g->Call(ctx,(PCV[]){t8.cv(),t7.cv()},1,11)) goto _0;
			lBase__i=(sLONG)lrint(t8.get());
		}
		{
			Long t10;
			t10=lPosition__i.get()-1;
			Txt t11;
			c.f.fLine=23;
			if (g->Call(ctx,(PCV[]){t11.cv(),lBracketOfValues__t.cv(),Long(1).cv(),t10.cv()},3,12)) goto _0;
			Num t12;
			if (g->Call(ctx,(PCV[]){t12.cv(),t11.cv()},1,11)) goto _0;
			lStation__i=(sLONG)lrint(t12.get());
		}
		if (lBase__i.get()>=lStation__i.get()) goto _3;
		lTemp__i=lBase__i.get();
		lBase__i=lStation__i.get();
		lStation__i=lBase__i.get();
_3:
		{
			Num t15;
			t15=lBase__i.get();
			Num t16;
			t16=800000/t15.get();
			Num t17;
			t17=t16.get()+0x1.ff7ced916872bp-1;
			Num t18;
			c.f.fLine=29;
			if (g->Call(ctx,(PCV[]){t18.cv(),t17.cv()},1,8)) goto _0;
			lRange__i=(sLONG)lrint(t18.get());
		}
_2:
_5:
		{
			Date t20;
			c.f.fLine=34;
			if (g->Call(ctx,(PCV[]){t20.cv()},0,33)) goto _0;
			Num t21;
			t21=g->SubstractDate(t20.get(),Date(1,1,1990).get());
			lDays__i=(sLONG)lrint(t21.get());
		}
		{
			Time t23;
			c.f.fLine=35;
			if (g->Call(ctx,(PCV[]){t23.cv()},0,178)) goto _0;
			lSeconds__i=t23.get()-lDateTimeDecrement__i.get();
		}
		if (0==lDateTimeDecrement__i.get()) goto _7;
		{
			Num t26;
			t26=lSeconds__i.get();
			Num t27;
			t27=t26.get()/86400;
			Num t28;
			c.f.fLine=39;
			if (g->Call(ctx,(PCV[]){t28.cv(),t27.cv()},1,8)) goto _0;
			Num t29;
			t29=lDays__i.get();
			Num t30;
			t30=t29.get()+t28.get();
			lDays__i=(sLONG)lrint(t30.get());
		}
		{
			Num t32;
			t32=lSeconds__i.get();
			Num t33;
			t33=t32.get()/86400;
			Num t34;
			c.f.fLine=40;
			if (g->Call(ctx,(PCV[]){t34.cv(),t33.cv()},1,8)) goto _0;
			Num t35;
			t35=t34.get()*86400;
			Num t36;
			t36=lSeconds__i.get();
			Num t37;
			t37=t36.get()-t35.get();
			lSeconds__i=(sLONG)lrint(t37.get());
		}
_7:
		if (lSeconds__i.get()==Var<Long>(ctx,vTS__SecondLastCalled__i).get()) goto _8;
		Var<Long>(ctx,vTS__SecondLastCalled__i)=lSeconds__i.get();
		Touch(ctx,vTS__SecondLastCalled__i);
		Var<Long>(ctx,vTS__NoOfTimesCalledThisSecond__i)=1;
		Touch(ctx,vTS__NoOfTimesCalledThisSecond__i);
		if (1!=lBase__i.get()) goto _9;
		{
			Long t41;
			c.f.fLine=48;
			if (g->Call(ctx,(PCV[]){t41.cv()},0,100)) goto _0;
			Long t42;
			t42=t41.get()%10000;
			Var<Long>(ctx,vTS__NoOfTimesCalledThisSecond__i)=Var<Long>(ctx,vTS__NoOfTimesCalledThisSecond__i).get()+t42.get();
			Touch(ctx,vTS__NoOfTimesCalledThisSecond__i);
		}
_9:
		goto _10;
_8:
		Var<Long>(ctx,vTS__NoOfTimesCalledThisSecond__i)=Var<Long>(ctx,vTS__NoOfTimesCalledThisSecond__i).get()+1;
		Touch(ctx,vTS__NoOfTimesCalledThisSecond__i);
_10:
_4:
		if (Var<Long>(ctx,vTS__NoOfTimesCalledThisSecond__i).get()>lRange__i.get()) goto _5;
_6:
		{
			Long t46;
			t46=lStation__i.get()*lRange__i.get();
			lCounter__i=Var<Long>(ctx,vTS__NoOfTimesCalledThisSecond__i).get()+t46.get();
		}
		{
			Long t48;
			t48=lDays__i.get();
			Long t49;
			t49=3;
			Txt t50;
			c.f.fLine=56;
			proc_TS__ENCODE36(glob,ctx,2,2,(PCV[]){t49.cv(),t48.cv()},t50.cv());
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
			lReturnedValue__t=t50.get();
		}
		{
			Long t51;
			t51=lSeconds__i.get()*18;
			Long t52;
			t52=lCounter__i.get()/46000;
			Long t53;
			t53=t51.get()+t52.get();
			Long t54;
			t54=4;
			Txt t55;
			c.f.fLine=57;
			proc_TS__ENCODE36(glob,ctx,2,2,(PCV[]){t54.cv(),t53.cv()},t55.cv());
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
			g->AddString(lReturnedValue__t.get(),t55.get(),lReturnedValue__t.get());
		}
		{
			Long t57;
			t57=lCounter__i.get()%46000;
			Long t58;
			t58=3;
			Txt t59;
			c.f.fLine=58;
			proc_TS__ENCODE36(glob,ctx,2,2,(PCV[]){t58.cv(),t57.cv()},t59.cv());
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
			g->AddString(lReturnedValue__t.get(),t59.get(),lReturnedValue__t.get());
		}
		c.f.fLine=60;
		{
			Obj t61;
			if (g->Call(ctx,(PCV[]){t61.cv(),Parm<Obj>(ctx,inParams,inNbParam,1).cv()},1,1529)) goto _0;
			g->Check(ctx);
			l__4D__auto__mutex__0=t61.get();
		}
		if (ctx->doingAbort) goto _0;
		c.f.fLine=61;
		if (g->SetMember(ctx,Parm<Obj>(ctx,inParams,inNbParam,1).cv(),KtimeStamp.cv(),lReturnedValue__t.cv())) goto _0;
		if (ctx->doingAbort) goto _0;
		{
			Obj t62;
			l__4D__auto__mutex__0=t62.get();
		}
		c.f.fLine=64;
		if (g->Call(ctx,(PCV[]){nullptr,Parm<Obj>(ctx,inParams,inNbParam,1).cv(),Ktrigger.cv()},2,1500)) goto _0;
		g->Check(ctx);
		if (ctx->doingAbort) goto _0;
_0:
_1:
;
	}

}
