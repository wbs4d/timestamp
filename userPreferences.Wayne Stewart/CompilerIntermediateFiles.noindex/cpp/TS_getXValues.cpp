extern Txt Kbracket;
extern Txt Kdt;
extern Txt KreturnArray;
Asm4d_Proc proc_TS__GET__PE__SAMEPROCESS;
extern unsigned char D_proc_TS__GETXVALUES[];
void proc_TS__GETXVALUES( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_TS__GETXVALUES);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Long v0;
		Value_array_text ltemp__at;
		Long v1;
		Long li;
		Blb lresult__x;
		c.f.fLine=7;
		{
			Ref t0;
			t0.setLocalRef(ctx,ltemp__at.cv());
			if (g->Call(ctx,(PCV[]){nullptr,t0.cv(),Parm<Long>(ctx,inParams,inNbParam,1).cv()},2,222)) goto _0;
		}
		if (ctx->doingAbort) goto _0;
		li=1;
		c.f.fLine=8;
		v0=Parm<Long>(ctx,inParams,inNbParam,1).get();
		if (ctx->doingAbort) goto _0;
		goto _2;
_4:
		c.f.fLine=9;
		{
			Obj t1;
			if (g->Call(ctx,(PCV[]){t1.cv(),Kbracket.cv(),Parm<Txt>(ctx,inParams,inNbParam,2).cv(),Kdt.cv(),Long(0).cv()},4,1471)) goto _0;
			g->Check(ctx);
			Txt t2;
			proc_TS__GET__PE__SAMEPROCESS(glob,ctx,1,1,(PCV[]){t1.cv()},t2.cv());
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
			Txt t3;
			t3=t2.get();
			ltemp__at.arrayElem(ctx,li.get())=t3.get();
		}
		if (ctx->doingAbort) goto _0;
_3:
		li=li.get()+1;
_2:
		if (li.get()<=v0.get()) goto _4;
_5:
		{
			Ref t6;
			t6.setLocalRef(ctx,lresult__x.cv());
			Ref t7;
			t7.setLocalRef(ctx,ltemp__at.cv());
			c.f.fLine=12;
			if (g->Call(ctx,(PCV[]){nullptr,t7.cv(),t6.cv()},2,532)) goto _0;
			g->Check(ctx);
		}
		{
			Blb t8;
			t8=lresult__x.get();
			c.f.fLine=14;
			if (g->Call(ctx,(PCV[]){nullptr,Parm<Long>(ctx,inParams,inNbParam,3).cv(),KreturnArray.cv(),Parm<Txt>(ctx,inParams,inNbParam,2).cv(),t8.cv()},4,1391)) goto _0;
			g->Check(ctx);
		}
		if (ctx->doingAbort) goto _0;
_0:
_1:
;
	}

}
