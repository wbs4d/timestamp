Asm4d_Proc proc_TS__KEYVALUE;
extern unsigned char D_proc_TS__DECODE[];
void proc_TS__DECODE( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_TS__DECODE);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Long v0;
		Long v1;
		Long lj;
		Long lk;
		Txt lKey__t;
		Long lBase__i;
		Long lEncodingLength__i;
		new ( outResult) Long();
		{
			Txt t0;
			c.f.fLine=31;
			t0=Parm<Txt>(ctx,inParams,inNbParam,1).get();
			Txt t1;
			proc_TS__KEYVALUE(glob,ctx,1,1,(PCV[]){t0.cv()},t1.cv());
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
			lKey__t=t1.get();
		}
		if (ctx->doingAbort) goto _0;
		{
			Long t2;
			t2=lKey__t.get().fLength;
			lBase__i=t2.get();
		}
		{
			Long t3;
			c.f.fLine=34;
			t3=Parm<Txt>(ctx,inParams,inNbParam,2).get().fLength;
			lEncodingLength__i=t3.get();
		}
		if (ctx->doingAbort) goto _0;
		c.f.fLine=36;
		Res<Long>(outResult)=0;
		lk=1;
		lj=lEncodingLength__i.get();
		v0=1;
		goto _2;
_4:
		{
			Txt t4;
			c.f.fLine=40;
			g->GetStringChar(ctx,Parm<Txt>(ctx,inParams,inNbParam,2).get(),(sLONG) lj.get(),t4.get());
			Long t5;
			if (g->Call(ctx,(PCV[]){t5.cv(),t4.cv(),lKey__t.cv()},2,15)) goto _0;
			Long t6;
			t6=t5.get()-1;
			Long t7;
			t7=t6.get()*lk.get();
			Res<Long>(outResult)=Res<Long>(outResult).get()+t7.get();
		}
		if (ctx->doingAbort) goto _0;
		lk=lk.get()*lBase__i.get();
_3:
		lj=lj.get()+-1;
_2:
		if (lj.get()>=v0.get()) goto _4;
_5:
_0:
_1:
;
	}

}
