extern Txt K;
extern Txt KTS;
extern Txt K_2F;
extern Txt Kbracket;
extern Txt Kdt;
extern Txt KlastCall;
extern Txt KnumberOfCalls;
Asm4d_Proc proc_TS__ENCODE36;
Asm4d_Proc proc_TS__INIT;
extern unsigned char D_proc_TS__GET__PE__STORAGE[];
void proc_TS__GET__PE__STORAGE( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_TS__GET__PE__STORAGE);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Long lNum;
		Long lj;
		Long lPosition__i;
		Txt lReturnedValue__t;
		Long lRange__i;
		Long lBase__i;
		Long lSeconds__i;
		Long lDateTimeDecrement__i;
		Long lDays__i;
		Long lCounter__i;
		Long lTemp__i;
		Obj l__4D__auto__mutex__0;
		Obj l__4D__auto__mutex__1;
		Obj l__4D__auto__mutex__2;
		Txt lBracketOfValues__t;
		Long lStation__i;
		Long lStart__i;
		new ( outResult) Txt();
		c.f.fLine=10;
		proc_TS__INIT(glob,ctx,0,0,nullptr,nullptr);
		if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
		if (ctx->doingAbort) goto _0;
		{
			Variant t0;
			c.f.fLine=12;
			if (g->GetMember(ctx,Parm<Obj>(ctx,inParams,inNbParam,1).cv(),Kbracket.cv(),t0.cv())) goto _0;
			Txt t1;
			if (!g->GetValue(ctx,(PCV[]){t1.cv(),t0.cv(),nullptr})) goto _0;
			lBracketOfValues__t=t1.get();
		}
		if (ctx->doingAbort) goto _0;
		{
			Variant t2;
			c.f.fLine=13;
			if (g->GetMember(ctx,Parm<Obj>(ctx,inParams,inNbParam,1).cv(),Kdt.cv(),t2.cv())) goto _0;
			Long t3;
			if (!g->GetValue(ctx,(PCV[]){t3.cv(),t2.cv(),nullptr})) goto _0;
			lDateTimeDecrement__i=t3.get();
		}
		if (ctx->doingAbort) goto _0;
		lStation__i=0;
		lBase__i=1;
		lRange__i=800000;
		{
			Bool t4;
			t4=g->CompareString(ctx,lBracketOfValues__t.get(),K.get())!=0;
			if (!(t4.get())) goto _2;
		}
		{
			Long t5;
			c.f.fLine=20;
			if (g->Call(ctx,(PCV[]){t5.cv(),K_2F.cv(),lBracketOfValues__t.cv()},2,15)) goto _0;
			lPosition__i=t5.get();
		}
		{
			Long t6;
			t6=lPosition__i.get()+1;
			Txt t7;
			c.f.fLine=21;
			if (g->Call(ctx,(PCV[]){t7.cv(),lBracketOfValues__t.cv(),t6.cv()},2,12)) goto _0;
			Num t8;
			if (g->Call(ctx,(PCV[]){t8.cv(),t7.cv()},1,11)) goto _0;
			lBase__i=(sLONG)lrint(t8.get());
		}
		{
			Long t10;
			t10=lPosition__i.get()-1;
			Txt t11;
			c.f.fLine=22;
			if (g->Call(ctx,(PCV[]){t11.cv(),lBracketOfValues__t.cv(),Long(1).cv(),t10.cv()},3,12)) goto _0;
			Num t12;
			if (g->Call(ctx,(PCV[]){t12.cv(),t11.cv()},1,11)) goto _0;
			lStation__i=(sLONG)lrint(t12.get());
		}
		if (lBase__i.get()>=lStation__i.get()) goto _3;
		lTemp__i=lBase__i.get();
		lBase__i=lStation__i.get();
		lStation__i=lBase__i.get();
_3:
		{
			Num t15;
			t15=lBase__i.get();
			Num t16;
			t16=800000/t15.get();
			Num t17;
			t17=t16.get()+0x1.ff7ced916872bp-1;
			Num t18;
			c.f.fLine=28;
			if (g->Call(ctx,(PCV[]){t18.cv(),t17.cv()},1,8)) goto _0;
			lRange__i=(sLONG)lrint(t18.get());
		}
_2:
_5:
		{
			Date t20;
			c.f.fLine=33;
			if (g->Call(ctx,(PCV[]){t20.cv()},0,33)) goto _0;
			Num t21;
			t21=g->SubstractDate(t20.get(),Date(1,1,1990).get());
			lDays__i=(sLONG)lrint(t21.get());
		}
		{
			Time t23;
			c.f.fLine=34;
			if (g->Call(ctx,(PCV[]){t23.cv()},0,178)) goto _0;
			lSeconds__i=t23.get()-lDateTimeDecrement__i.get();
		}
		if (0==lDateTimeDecrement__i.get()) goto _7;
		{
			Num t26;
			t26=lSeconds__i.get();
			Num t27;
			t27=t26.get()/86400;
			Num t28;
			c.f.fLine=38;
			if (g->Call(ctx,(PCV[]){t28.cv(),t27.cv()},1,8)) goto _0;
			Num t29;
			t29=lDays__i.get();
			Num t30;
			t30=t29.get()+t28.get();
			lDays__i=(sLONG)lrint(t30.get());
		}
		{
			Num t32;
			t32=lSeconds__i.get();
			Num t33;
			t33=t32.get()/86400;
			Num t34;
			c.f.fLine=39;
			if (g->Call(ctx,(PCV[]){t34.cv(),t33.cv()},1,8)) goto _0;
			Num t35;
			t35=t34.get()*86400;
			Num t36;
			t36=lSeconds__i.get();
			Num t37;
			t37=t36.get()-t35.get();
			lSeconds__i=(sLONG)lrint(t37.get());
		}
_7:
		{
			Obj t39;
			c.f.fLine=43;
			if (g->Call(ctx,(PCV[]){t39.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t40;
			if (g->GetMember(ctx,t39.cv(),KTS.cv(),t40.cv())) goto _0;
			Variant t41;
			if (g->GetMember(ctx,t40.cv(),KlastCall.cv(),t41.cv())) goto _0;
			Bool t42;
			if (g->OperationOnAny(ctx,7,lSeconds__i.cv(),t41.cv(),t42.cv())) goto _0;
			if (!(t42.get())) goto _8;
		}
		{
			Obj t43;
			c.f.fLine=44;
			if (g->Call(ctx,(PCV[]){t43.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t44;
			if (g->GetMember(ctx,t43.cv(),KTS.cv(),t44.cv())) goto _0;
			Obj t45;
			if (g->Call(ctx,(PCV[]){t45.cv(),t44.cv()},1,1529)) goto _0;
			l__4D__auto__mutex__0=t45.get();
		}
		{
			Obj t46;
			c.f.fLine=45;
			if (g->Call(ctx,(PCV[]){t46.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t47;
			if (g->GetMember(ctx,t46.cv(),KTS.cv(),t47.cv())) goto _0;
			if (g->SetMember(ctx,t47.cv(),KlastCall.cv(),lSeconds__i.cv())) goto _0;
		}
		{
			Obj t48;
			c.f.fLine=46;
			if (g->Call(ctx,(PCV[]){t48.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t49;
			if (g->GetMember(ctx,t48.cv(),KTS.cv(),t49.cv())) goto _0;
			if (g->SetMember(ctx,t49.cv(),KnumberOfCalls.cv(),Long(1).cv())) goto _0;
		}
		{
			Obj t50;
			l__4D__auto__mutex__0=t50.get();
		}
		if (1!=lBase__i.get()) goto _9;
		{
			Obj t52;
			c.f.fLine=49;
			if (g->Call(ctx,(PCV[]){t52.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t53;
			if (g->GetMember(ctx,t52.cv(),KTS.cv(),t53.cv())) goto _0;
			Obj t54;
			if (g->Call(ctx,(PCV[]){t54.cv(),t53.cv()},1,1529)) goto _0;
			l__4D__auto__mutex__1=t54.get();
		}
		{
			Obj t55;
			c.f.fLine=50;
			if (g->Call(ctx,(PCV[]){t55.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t56;
			if (g->GetMember(ctx,t55.cv(),KTS.cv(),t56.cv())) goto _0;
			Obj t57;
			if (g->Call(ctx,(PCV[]){t57.cv()},0,1525)) goto _0;
			Variant t58;
			if (g->GetMember(ctx,t57.cv(),KTS.cv(),t58.cv())) goto _0;
			Variant t59;
			if (g->GetMember(ctx,t58.cv(),KnumberOfCalls.cv(),t59.cv())) goto _0;
			Long t60;
			if (g->Call(ctx,(PCV[]){t60.cv()},0,100)) goto _0;
			Long t61;
			t61=t60.get()%10000;
			Variant t62;
			if (g->OperationOnAny(ctx,0,t59.cv(),t61.cv(),t62.cv())) goto _0;
			if (g->SetMember(ctx,t56.cv(),KnumberOfCalls.cv(),t62.cv())) goto _0;
		}
		{
			Obj t63;
			l__4D__auto__mutex__1=t63.get();
		}
_9:
		goto _10;
_8:
		{
			Obj t64;
			c.f.fLine=54;
			if (g->Call(ctx,(PCV[]){t64.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t65;
			if (g->GetMember(ctx,t64.cv(),KTS.cv(),t65.cv())) goto _0;
			Obj t66;
			if (g->Call(ctx,(PCV[]){t66.cv(),t65.cv()},1,1529)) goto _0;
			l__4D__auto__mutex__2=t66.get();
		}
		{
			Obj t67;
			c.f.fLine=55;
			if (g->Call(ctx,(PCV[]){t67.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t68;
			if (g->GetMember(ctx,t67.cv(),KTS.cv(),t68.cv())) goto _0;
			Obj t69;
			if (g->Call(ctx,(PCV[]){t69.cv()},0,1525)) goto _0;
			Variant t70;
			if (g->GetMember(ctx,t69.cv(),KTS.cv(),t70.cv())) goto _0;
			Variant t71;
			if (g->GetMember(ctx,t70.cv(),KnumberOfCalls.cv(),t71.cv())) goto _0;
			Variant t72;
			if (g->OperationOnAny(ctx,0,t71.cv(),Num(1).cv(),t72.cv())) goto _0;
			if (g->SetMember(ctx,t68.cv(),KnumberOfCalls.cv(),t72.cv())) goto _0;
		}
		{
			Obj t73;
			l__4D__auto__mutex__2=t73.get();
		}
_10:
_4:
		{
			Obj t74;
			c.f.fLine=58;
			if (g->Call(ctx,(PCV[]){t74.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t75;
			if (g->GetMember(ctx,t74.cv(),KTS.cv(),t75.cv())) goto _0;
			Variant t76;
			if (g->GetMember(ctx,t75.cv(),KnumberOfCalls.cv(),t76.cv())) goto _0;
			Bool t77;
			if (g->OperationOnAny(ctx,11,t76.cv(),lRange__i.cv(),t77.cv())) goto _0;
			if (!(t77.get())) goto _5;
		}
_6:
		{
			Obj t78;
			c.f.fLine=60;
			if (g->Call(ctx,(PCV[]){t78.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t79;
			if (g->GetMember(ctx,t78.cv(),KTS.cv(),t79.cv())) goto _0;
			Variant t80;
			if (g->GetMember(ctx,t79.cv(),KnumberOfCalls.cv(),t80.cv())) goto _0;
			Long t81;
			t81=lStation__i.get()*lRange__i.get();
			Variant t82;
			if (g->OperationOnAny(ctx,0,t80.cv(),t81.cv(),t82.cv())) goto _0;
			Long t83;
			if (!g->GetValue(ctx,(PCV[]){t83.cv(),t82.cv(),nullptr})) goto _0;
			lCounter__i=t83.get();
		}
		{
			Long t84;
			t84=lDays__i.get();
			Long t85;
			t85=3;
			Txt t86;
			c.f.fLine=62;
			proc_TS__ENCODE36(glob,ctx,2,2,(PCV[]){t85.cv(),t84.cv()},t86.cv());
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
			lReturnedValue__t=t86.get();
		}
		{
			Long t87;
			t87=lSeconds__i.get()*18;
			Long t88;
			t88=lCounter__i.get()/46000;
			Long t89;
			t89=t87.get()+t88.get();
			Long t90;
			t90=4;
			Txt t91;
			c.f.fLine=63;
			proc_TS__ENCODE36(glob,ctx,2,2,(PCV[]){t90.cv(),t89.cv()},t91.cv());
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
			g->AddString(lReturnedValue__t.get(),t91.get(),lReturnedValue__t.get());
		}
		{
			Long t93;
			t93=lCounter__i.get()%46000;
			Long t94;
			t94=3;
			Txt t95;
			c.f.fLine=64;
			proc_TS__ENCODE36(glob,ctx,2,2,(PCV[]){t94.cv(),t93.cv()},t95.cv());
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
			g->AddString(lReturnedValue__t.get(),t95.get(),lReturnedValue__t.get());
		}
		c.f.fLine=66;
		Res<Txt>(outResult)=lReturnedValue__t.get();
_0:
_1:
;
	}

}
