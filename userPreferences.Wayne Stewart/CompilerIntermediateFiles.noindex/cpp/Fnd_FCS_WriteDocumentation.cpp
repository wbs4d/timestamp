extern Txt K;
extern Txt KAttributes_3A_20;
extern Txt KCourier;
extern Txt KCreated_20by;
extern Txt KDocumentation;
extern Txt KInvisible_2C_20;
extern Txt KMethods;
extern Txt KParameters_3A;
extern Txt KReturns_3A;
extern Txt KSQL_2C_20;
extern Txt KServer_2C_20;
extern Txt KShared_2C_20;
extern Txt KSoap_2C_20;
extern Txt KWSDL_2C_20;
extern Txt KWeb_2C_20;
extern Txt K_0D;
extern Txt K_0D_0D;
extern Txt K_20;
extern Txt K_20_20_2F_2F_20;
extern Txt K_20_3A_20;
extern Txt K_23_23_20;
extern Txt K_24;
extern Txt K_2A_2AReturns_2A_2A;
extern Txt K_2D_2D_3E;
extern Txt K_2D_3E;
extern Txt K_2F;
extern Txt K_2F_2F;
extern Txt K_2F_2F_20;
extern Txt K_2F_2F_20Parameters_3A;
extern Txt K_2F_2F_20Returns_3A;
extern Txt K_2F_2F_20_20_20_20_24;
extern Txt K_2F_2F_20_20_20_24;
extern Txt K_2F_2F_20_20_24;
extern Txt K_2F_2F_20_24;
extern Txt K_3A;
extern Txt K_3C_21_2D_2D;
extern Txt K_3Cbr_3E;
extern Txt K_40;
extern Txt K_40Created_20by_40;
extern Txt K_40Parameters_3A_40;
extern Txt K_40Returns_40;
extern Txt K_7C;
extern Txt Kcapable;
extern Txt Kincapable;
extern Txt Kindifferent;
extern Txt Kinvisible;
extern Txt Kpreemptive;
extern Txt KpublishedSoap;
extern Txt KpublishedSql;
extern Txt KpublishedWeb;
extern Txt KpublishedWsdl;
extern Txt Kshared;
extern Txt k$tdAxHfqfk0;
extern Txt k2ucr6KMltt4;
extern Txt kFztatg2AeEk;
extern Txt kHSK4e$VtNdY;
extern Txt kKqLvphAEyfI;
extern Txt kPk$4ELz88bk;
extern Txt kXztOPunYs9g;
extern Txt kasCU9KRLhsc;
extern Txt kbE413fCVMjA;
extern Txt kc$4IxpZB99g;
extern Txt kdAQ7fWUqce8;
extern Txt keYqLvjgs1KI;
extern Txt kemoKRlUtXYg;
extern Txt khAhd0qeKOrE;
extern Txt khj46qayLLyM;
extern Txt kq2BFNIvkFYU;
extern Txt kvUWSXMd0W2g;
extern Txt kx4DgsGRJUpg;
extern Txt kyUKfCm0Vmhg;
extern unsigned char D_proc_FND__FCS__WRITEDOCUMENTATION[];
void proc_FND__FCS__WRITEDOCUMENTATION( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_FND__FCS__WRITEDOCUMENTATION);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Long v0;
		Long v1;
		Long lProcessID__i;
		Long v2;
		Long lNumberOfMethods__i;
		Txt lMethodName__t;
		Txt lMethodCode__t;
		Long lPosition__i;
		Value_array_text lMethodNames__at;
		Long v3;
		Long lline__i;
		Txt lSpace;
		Bool lexcludePrivate__b;
		Value_array_text lmethodLines__at;
		Long v4;
		Long llineEnd__i;
		Txt lcallSyntaxParameters__t;
		Value_array_text lMethodComments__at;
		Long v5;
		Txt llastChar__t;
		Long lparameterBlock__i;
		Txt lparameterBlock__t;
		Txt ldocumentationPath__t;
		Long lStackSize__i;
		Txt lCR;
		Value_array_text lMethodCode__at;
		Long lreturns__i;
		Txt lFirstChars__t;
		Long lCurrentMethod__i;
		Txt lprocessName__t;
		Long lnumberofLines__i;
		Bool lToolTip__b;
		Obj lAttributes__o;
		Long lnextLine__i;
		Txt lAttributes__t;
		Txt lparameterline__t;
		Txt lcallSyntax__t;
		Txt lnextline__t;
		if (!(Bool(0).get())) goto _2;
_2:
		{
			Ref t0;
			t0.setLocalRef(ctx,lMethodCode__at.cv());
			c.f.fLine=38;
			if (g->Call(ctx,(PCV[]){nullptr,t0.cv(),Long(0).cv()},2,222)) goto _0;
		}
		{
			Ref t1;
			t1.setLocalRef(ctx,lmethodLines__at.cv());
			c.f.fLine=39;
			if (g->Call(ctx,(PCV[]){nullptr,t1.cv(),Long(0).cv()},2,222)) goto _0;
		}
		{
			Ref t2;
			t2.setLocalRef(ctx,lMethodNames__at.cv());
			c.f.fLine=40;
			if (g->Call(ctx,(PCV[]){nullptr,t2.cv(),Long(0).cv()},2,222)) goto _0;
		}
		lprocessName__t=kasCU9KRLhsc.get();
		lStackSize__i=0;
		{
			Txt t3;
			c.f.fLine=45;
			if (g->Call(ctx,(PCV[]){t3.cv()},0,1392)) goto _0;
			g->Check(ctx);
			Bool t4;
			t4=g->CompareString(ctx,t3.get(),lprocessName__t.get())==0;
			if (!(t4.get())) goto _3;
		}
		{
			Txt t5;
			c.f.fLine=47;
			if (g->Call(ctx,(PCV[]){t5.cv(),Long(13).cv()},1,90)) goto _0;
			lCR=t5.get();
		}
		lSpace=K_20.get();
		{
			Ref t6;
			t6.setLocalRef(ctx,lMethodNames__at.cv());
			c.f.fLine=50;
			if (g->Call(ctx,(PCV[]){nullptr,Long(1).cv(),t6.cv()},2,1163)) goto _0;
			g->Check(ctx);
		}
		c.f.fLine=52;
		lMethodName__t=Parm<Txt>(ctx,inParams,inNbParam,1).get();
		if (ctx->doingAbort) goto _0;
		c.f.fLine=53;
		lToolTip__b=Parm<Bool>(ctx,inParams,inNbParam,2).get();
		if (ctx->doingAbort) goto _0;
		c.f.fLine=54;
		lexcludePrivate__b=Parm<Bool>(ctx,inParams,inNbParam,3).get();
		if (ctx->doingAbort) goto _0;
		{
			Long t7;
			t7=lMethodName__t.get().fLength;
			if (0>=t7.get()) goto _4;
		}
		{
			Ref t9;
			t9.setLocalRef(ctx,lMethodNames__at.cv());
			Long t10;
			c.f.fLine=58;
			if (g->Call(ctx,(PCV[]){t10.cv(),t9.cv(),lMethodName__t.cv()},2,907)) goto _0;
			g->Check(ctx);
			lNumberOfMethods__i=t10.get();
		}
		if (1!=lNumberOfMethods__i.get()) goto _5;
		{
			Ref t12;
			t12.setLocalRef(ctx,lMethodNames__at.cv());
			c.f.fLine=61;
			if (g->Call(ctx,(PCV[]){nullptr,t12.cv(),Long(0).cv()},2,222)) goto _0;
		}
		{
			Ref t13;
			t13.setLocalRef(ctx,lMethodNames__at.cv());
			c.f.fLine=62;
			if (g->Call(ctx,(PCV[]){nullptr,t13.cv(),lMethodName__t.cv()},2,911)) goto _0;
			g->Check(ctx);
		}
		goto _6;
_5:
		{
			Ref t14;
			t14.setLocalRef(ctx,lMethodNames__at.cv());
			Long t15;
			c.f.fLine=65;
			if (g->Call(ctx,(PCV[]){t15.cv(),t14.cv()},1,274)) goto _0;
			lNumberOfMethods__i=t15.get();
		}
		lCurrentMethod__i=lNumberOfMethods__i.get();
		v0=1;
		goto _7;
_9:
		{
			Txt t16;
			g->AddString(lMethodName__t.get(),K_40.get(),t16.get());
			Txt t18;
			c.f.fLine=67;
			t18=lMethodNames__at.arrayElem(ctx,lCurrentMethod__i.get()).get();
			Bool t17;
			t17=g->CompareString(ctx,t18.get(),t16.get())==0;
			if (!(t17.get())) goto _11;
		}
		if (ctx->doingAbort) goto _0;
		goto _12;
_11:
		{
			Ref t19;
			t19.setLocalRef(ctx,lMethodNames__at.cv());
			c.f.fLine=69;
			if (g->Call(ctx,(PCV[]){nullptr,t19.cv(),lCurrentMethod__i.cv()},2,228)) goto _0;
		}
_12:
_8:
		lCurrentMethod__i=lCurrentMethod__i.get()+-1;
_7:
		if (lCurrentMethod__i.get()>=v0.get()) goto _9;
_10:
_6:
_4:
		{
			Ref t22;
			t22.setLocalRef(ctx,lMethodNames__at.cv());
			Long t23;
			c.f.fLine=78;
			if (g->Call(ctx,(PCV[]){t23.cv(),t22.cv()},1,274)) goto _0;
			lNumberOfMethods__i=t23.get();
		}
		{
			Ref t24;
			t24.setLocalRef(ctx,lMethodCode__at.cv());
			Ref t25;
			t25.setLocalRef(ctx,lMethodNames__at.cv());
			c.f.fLine=80;
			if (g->Call(ctx,(PCV[]){nullptr,t25.cv(),t24.cv()},2,1190)) goto _0;
			g->Check(ctx);
		}
		{
			Ref t26;
			t26.setLocalRef(ctx,lMethodComments__at.cv());
			c.f.fLine=82;
			if (g->Call(ctx,(PCV[]){nullptr,t26.cv(),lNumberOfMethods__i.cv()},2,222)) goto _0;
		}
		lCurrentMethod__i=1;
		v2=lNumberOfMethods__i.get();
		goto _13;
_15:
		c.f.fLine=86;
		lMethodName__t=lMethodNames__at.arrayElem(ctx,lCurrentMethod__i.get()).get();
		if (ctx->doingAbort) goto _0;
		c.f.fLine=88;
		lMethodCode__t=lMethodCode__at.arrayElem(ctx,lCurrentMethod__i.get()).get();
		if (ctx->doingAbort) goto _0;
		{
			Num t29;
			t29=32000;
			Ref t30;
			t30.setLocalRef(ctx,lmethodLines__at.cv());
			c.f.fLine=90;
			if (g->Call(ctx,(PCV[]){nullptr,lMethodCode__t.cv(),t30.cv(),t29.cv(),KCourier.cv(),Long(9).cv()},5,1149)) goto _0;
			g->Check(ctx);
		}
		{
			Ref t31;
			t31.setLocalRef(ctx,lmethodLines__at.cv());
			c.f.fLine=93;
			if (g->Call(ctx,(PCV[]){nullptr,t31.cv(),Long(1).cv(),Long(1).cv()},3,228)) goto _0;
		}
		{
			Long t32;
			c.f.fLine=94;
			if (g->Call(ctx,(PCV[]){t32.cv(),kq2BFNIvkFYU.cv(),lMethodCode__t.cv()},2,15)) goto _0;
			lPosition__i=t32.get();
		}
		{
			Long t33;
			t33=kq2BFNIvkFYU.get().fLength;
			Long t34;
			t34=lPosition__i.get()+t33.get();
			Txt t35;
			c.f.fLine=95;
			if (g->Call(ctx,(PCV[]){t35.cv(),lMethodCode__t.cv(),t34.cv()},2,12)) goto _0;
			lMethodCode__t=t35.get();
		}
		{
			Long t36;
			c.f.fLine=98;
			if (g->Call(ctx,(PCV[]){t36.cv(),KCreated_20by.cv(),lMethodCode__t.cv()},2,15)) goto _0;
			lPosition__i=t36.get();
		}
		{
			Long t37;
			t37=lPosition__i.get()-3;
			Txt t38;
			c.f.fLine=99;
			if (g->Call(ctx,(PCV[]){t38.cv(),lMethodCode__t.cv(),Long(1).cv(),t37.cv()},3,12)) goto _0;
			lMethodCode__t=t38.get();
		}
		{
			Ref t39;
			t39.setLocalRef(ctx,lmethodLines__at.cv());
			Long t40;
			c.f.fLine=100;
			if (g->Call(ctx,(PCV[]){t40.cv(),t39.cv(),K_40Created_20by_40.cv()},2,230)) goto _0;
			lline__i=t40.get();
		}
		if (0>=lline__i.get()) goto _17;
		{
			Ref t42;
			t42.setLocalRef(ctx,lmethodLines__at.cv());
			c.f.fLine=102;
			if (g->Call(ctx,(PCV[]){nullptr,t42.cv(),lline__i.cv(),Long(32000).cv()},3,228)) goto _0;
		}
_17:
		{
			Txt t43;
			c.f.fLine=106;
			if (g->Call(ctx,(PCV[]){t43.cv(),lMethodCode__t.cv(),kPk$4ELz88bk.cv(),K.cv()},3,233)) goto _0;
			lMethodCode__t=t43.get();
		}
		{
			Txt t44;
			c.f.fLine=107;
			if (g->Call(ctx,(PCV[]){t44.cv(),lMethodCode__t.cv(),kHSK4e$VtNdY.cv(),K.cv()},3,233)) goto _0;
			lMethodCode__t=t44.get();
		}
		{
			Long t45;
			c.f.fLine=110;
			if (g->Call(ctx,(PCV[]){t45.cv(),KParameters_3A.cv(),lMethodCode__t.cv()},2,15)) goto _0;
			lparameterBlock__i=t45.get();
		}
		if (0>=lparameterBlock__i.get()) goto _18;
		{
			Long t47;
			c.f.fLine=112;
			if (g->Call(ctx,(PCV[]){t47.cv(),kbE413fCVMjA.cv(),lMethodCode__t.cv()},2,15)) goto _0;
			if (0>=t47.get()) goto _19;
		}
		lparameterBlock__i=0;
_19:
_18:
		{
			Long t49;
			c.f.fLine=118;
			if (g->Call(ctx,(PCV[]){t49.cv(),KReturns_3A.cv(),lMethodCode__t.cv()},2,15)) goto _0;
			lreturns__i=t49.get();
		}
		if (0>=lreturns__i.get()) goto _20;
		{
			Long t51;
			c.f.fLine=120;
			if (g->Call(ctx,(PCV[]){t51.cv(),kc$4IxpZB99g.cv(),lMethodCode__t.cv()},2,15)) goto _0;
			if (0>=t51.get()) goto _21;
		}
		lreturns__i=0;
_21:
_20:
		{
			Txt t53;
			g->AddString(kemoKRlUtXYg.get(),lCR.get(),t53.get());
			Txt t54;
			g->AddString(t53.get(),kXztOPunYs9g.get(),t54.get());
			g->AddString(t54.get(),lCR.get(),lparameterBlock__t.get());
		}
		{
			Ref t56;
			t56.setLocalRef(ctx,lmethodLines__at.cv());
			Long t57;
			c.f.fLine=127;
			if (g->Call(ctx,(PCV[]){t57.cv(),t56.cv()},1,274)) goto _0;
			lnumberofLines__i=t57.get();
		}
		{
			Bool t58;
			t58=0<lparameterBlock__i.get();
			Bool t59;
			t59=0<lreturns__i.get();
			if (!( t58.get()&&t59.get())) goto _23;
		}
		{
			Ref t61;
			t61.setLocalRef(ctx,lmethodLines__at.cv());
			Long t62;
			c.f.fLine=130;
			if (g->Call(ctx,(PCV[]){t62.cv(),t61.cv(),K_40Parameters_3A_40.cv()},2,230)) goto _0;
			lparameterBlock__i=t62.get()+1;
		}
		c.f.fLine=131;
		lparameterline__t=lmethodLines__at.arrayElem(ctx,lparameterBlock__i.get()).get();
		if (ctx->doingAbort) goto _0;
_25:
		{
			Txt t65;
			c.f.fLine=134;
			if (g->Call(ctx,(PCV[]){t65.cv(),lparameterline__t.cv(),K_2F_2F_20_20_20_20_24.cv(),K_24.cv()},3,233)) goto _0;
			lparameterline__t=t65.get();
		}
		{
			Txt t66;
			c.f.fLine=135;
			if (g->Call(ctx,(PCV[]){t66.cv(),lparameterline__t.cv(),K_2F_2F_20_20_20_24.cv(),K_24.cv()},3,233)) goto _0;
			lparameterline__t=t66.get();
		}
		{
			Txt t67;
			c.f.fLine=136;
			if (g->Call(ctx,(PCV[]){t67.cv(),lparameterline__t.cv(),K_2F_2F_20_20_24.cv(),K_24.cv()},3,233)) goto _0;
			lparameterline__t=t67.get();
		}
		{
			Txt t68;
			c.f.fLine=137;
			if (g->Call(ctx,(PCV[]){t68.cv(),lparameterline__t.cv(),K_2F_2F_20_24.cv(),K_24.cv()},3,233)) goto _0;
			lparameterline__t=t68.get();
		}
		{
			Txt t69;
			c.f.fLine=138;
			if (g->Call(ctx,(PCV[]){t69.cv(),lparameterline__t.cv(),K_20_3A_20.cv(),K_7C.cv()},3,233)) goto _0;
			lparameterline__t=t69.get();
		}
		{
			Txt t70;
			g->AddString(lparameterBlock__t.get(),lparameterline__t.get(),t70.get());
			g->AddString(t70.get(),lCR.get(),lparameterBlock__t.get());
		}
		lparameterBlock__i=lparameterBlock__i.get()+1;
		c.f.fLine=143;
		lparameterline__t=lmethodLines__at.arrayElem(ctx,lparameterBlock__i.get()).get();
		if (ctx->doingAbort) goto _0;
_24:
		{
			Bool t74;
			t74=g->CompareString(ctx,lparameterline__t.get(),K_40Returns_40.get())==0;
			if (!(t74.get())) goto _25;
		}
_26:
		goto _22;
_23:
		if (0>=lparameterBlock__i.get()) goto _27;
		{
			Ref t76;
			t76.setLocalRef(ctx,lmethodLines__at.cv());
			Long t77;
			c.f.fLine=147;
			if (g->Call(ctx,(PCV[]){t77.cv(),t76.cv(),K_40Parameters_3A_40.cv()},2,230)) goto _0;
			lparameterBlock__i=t77.get()+1;
		}
		c.f.fLine=148;
		lparameterline__t=lmethodLines__at.arrayElem(ctx,lparameterBlock__i.get()).get();
		if (ctx->doingAbort) goto _0;
_29:
		{
			Txt t80;
			c.f.fLine=151;
			if (g->Call(ctx,(PCV[]){t80.cv(),lparameterline__t.cv(),K_2F_2F_20_20_20_20_24.cv(),K_24.cv()},3,233)) goto _0;
			lparameterline__t=t80.get();
		}
		{
			Txt t81;
			c.f.fLine=152;
			if (g->Call(ctx,(PCV[]){t81.cv(),lparameterline__t.cv(),K_2F_2F_20_20_20_24.cv(),K_24.cv()},3,233)) goto _0;
			lparameterline__t=t81.get();
		}
		{
			Txt t82;
			c.f.fLine=153;
			if (g->Call(ctx,(PCV[]){t82.cv(),lparameterline__t.cv(),K_2F_2F_20_20_24.cv(),K_24.cv()},3,233)) goto _0;
			lparameterline__t=t82.get();
		}
		{
			Txt t83;
			c.f.fLine=154;
			if (g->Call(ctx,(PCV[]){t83.cv(),lparameterline__t.cv(),K_2F_2F_20_24.cv(),K_24.cv()},3,233)) goto _0;
			lparameterline__t=t83.get();
		}
		{
			Txt t84;
			c.f.fLine=155;
			if (g->Call(ctx,(PCV[]){t84.cv(),lparameterline__t.cv(),K_20_3A_20.cv(),K_7C.cv()},3,233)) goto _0;
			lparameterline__t=t84.get();
		}
		{
			Txt t85;
			g->AddString(lparameterBlock__t.get(),lparameterline__t.get(),t85.get());
			g->AddString(t85.get(),lCR.get(),lparameterBlock__t.get());
		}
		lparameterBlock__i=lparameterBlock__i.get()+1;
		if (lparameterBlock__i.get()>lnumberofLines__i.get()) goto _31;
		c.f.fLine=162;
		lparameterline__t=lmethodLines__at.arrayElem(ctx,lparameterBlock__i.get()).get();
		if (ctx->doingAbort) goto _0;
		goto _32;
_31:
		lparameterline__t=K.get();
_32:
		lnextLine__i=lparameterBlock__i.get()+1;
		if (lnextLine__i.get()>lnumberofLines__i.get()) goto _33;
		c.f.fLine=169;
		lnextline__t=lmethodLines__at.arrayElem(ctx,lnextLine__i.get()).get();
		if (ctx->doingAbort) goto _0;
		goto _34;
_33:
		lnextline__t=K.get();
_34:
_28:
		{
			Bool t93;
			t93=lparameterBlock__i.get()>lnumberofLines__i.get();
			Bool t94;
			t94=g->CompareString(ctx,lparameterline__t.get(),K.get())==0;
			Bool t95;
			t95=g->CompareString(ctx,lnextline__t.get(),K.get())==0;
			Bool t96;
			t96=t94.get()&&t95.get();
			Bool t97;
			t97=t93.get()||t96.get();
			Bool t98;
			t98=g->CompareString(ctx,lparameterline__t.get(),K_40Returns_40.get())==0;
			if (!( t97.get()||t98.get())) goto _29;
		}
_30:
		goto _22;
_27:
		if (0>=lreturns__i.get()) goto _35;
		goto _22;
_35:
		lparameterBlock__t=K.get();
_22:
		{
			Txt t101;
			g->AddString(lCR.get(),lCR.get(),t101.get());
			Txt t102;
			c.f.fLine=189;
			if (g->Call(ctx,(PCV[]){t102.cv(),lparameterBlock__t.cv(),t101.cv(),lCR.cv()},3,233)) goto _0;
			lparameterBlock__t=t102.get();
		}
		if (0>=lreturns__i.get()) goto _36;
		{
			Ref t104;
			t104.setLocalRef(ctx,lmethodLines__at.cv());
			Long t105;
			c.f.fLine=192;
			if (g->Call(ctx,(PCV[]){t105.cv(),t104.cv(),K_40Returns_40.cv(),lparameterBlock__i.cv()},3,230)) goto _0;
			lparameterBlock__i=t105.get();
		}
		c.f.fLine=194;
		lparameterline__t=lmethodLines__at.arrayElem(ctx,lparameterBlock__i.get()).get();
		if (ctx->doingAbort) goto _0;
_38:
		{
			Txt t107;
			c.f.fLine=197;
			if (g->Call(ctx,(PCV[]){t107.cv(),lparameterline__t.cv(),K_2F_2F_20_20_20_20_24.cv(),K_24.cv()},3,233)) goto _0;
			lparameterline__t=t107.get();
		}
		{
			Txt t108;
			c.f.fLine=198;
			if (g->Call(ctx,(PCV[]){t108.cv(),lparameterline__t.cv(),K_2F_2F_20_20_20_24.cv(),K_24.cv()},3,233)) goto _0;
			lparameterline__t=t108.get();
		}
		{
			Txt t109;
			c.f.fLine=199;
			if (g->Call(ctx,(PCV[]){t109.cv(),lparameterline__t.cv(),K_2F_2F_20_20_24.cv(),K_24.cv()},3,233)) goto _0;
			lparameterline__t=t109.get();
		}
		{
			Txt t110;
			c.f.fLine=200;
			if (g->Call(ctx,(PCV[]){t110.cv(),lparameterline__t.cv(),K_2F_2F_20_24.cv(),K_24.cv()},3,233)) goto _0;
			lparameterline__t=t110.get();
		}
		{
			Txt t111;
			c.f.fLine=201;
			if (g->Call(ctx,(PCV[]){t111.cv(),lparameterline__t.cv(),K_20_3A_20.cv(),K_7C.cv()},3,233)) goto _0;
			lparameterline__t=t111.get();
		}
		{
			Txt t112;
			g->AddString(lparameterBlock__t.get(),lparameterline__t.get(),t112.get());
			g->AddString(t112.get(),lCR.get(),lparameterBlock__t.get());
		}
		lparameterBlock__i=lparameterBlock__i.get()+1;
		if (lparameterBlock__i.get()>lnumberofLines__i.get()) goto _40;
		c.f.fLine=208;
		lparameterline__t=lmethodLines__at.arrayElem(ctx,lparameterBlock__i.get()).get();
		if (ctx->doingAbort) goto _0;
		goto _41;
_40:
		lparameterline__t=K.get();
_41:
		lnextLine__i=lparameterBlock__i.get()+1;
		if (lnextLine__i.get()>lnumberofLines__i.get()) goto _42;
		c.f.fLine=215;
		lnextline__t=lmethodLines__at.arrayElem(ctx,lnextLine__i.get()).get();
		if (ctx->doingAbort) goto _0;
		goto _43;
_42:
		lnextline__t=K.get();
_43:
_37:
		{
			Bool t120;
			t120=lparameterBlock__i.get()>lnumberofLines__i.get();
			Bool t121;
			t121=g->CompareString(ctx,lparameterline__t.get(),K.get())==0;
			Bool t122;
			t122=g->CompareString(ctx,lnextline__t.get(),K.get())==0;
			Bool t123;
			t123=t121.get()&&t122.get();
			if (!( t120.get()||t123.get())) goto _38;
		}
_39:
_36:
		{
			Txt t125;
			g->AddString(lCR.get(),lCR.get(),t125.get());
			Txt t126;
			c.f.fLine=230;
			if (g->Call(ctx,(PCV[]){t126.cv(),lparameterBlock__t.cv(),t125.cv(),lCR.cv()},3,233)) goto _0;
			lparameterBlock__t=t126.get();
		}
		{
			Long t127;
			c.f.fLine=233;
			if (g->Call(ctx,(PCV[]){t127.cv(),K_2F_2F_20Parameters_3A.cv(),lMethodCode__t.cv()},2,15)) goto _0;
			lparameterBlock__i=t127.get();
		}
		{
			Long t128;
			c.f.fLine=234;
			if (g->Call(ctx,(PCV[]){t128.cv(),K_2F_2F_20Returns_3A.cv(),lMethodCode__t.cv()},2,15)) goto _0;
			lreturns__i=t128.get();
		}
		if (0>=lparameterBlock__i.get()) goto _44;
		{
			Txt t130;
			c.f.fLine=237;
			if (g->Call(ctx,(PCV[]){t130.cv(),lMethodCode__t.cv(),Long(1).cv(),lparameterBlock__i.cv()},3,12)) goto _0;
			lMethodCode__t=t130.get();
		}
		goto _45;
_44:
		if (0>=lreturns__i.get()) goto _46;
		{
			Txt t132;
			c.f.fLine=240;
			if (g->Call(ctx,(PCV[]){t132.cv(),lMethodCode__t.cv(),Long(1).cv(),lreturns__i.cv()},3,12)) goto _0;
			lMethodCode__t=t132.get();
		}
_46:
_45:
		{
			Ref t133;
			t133.setLocalRef(ctx,lAttributes__o.cv());
			Ref t134;
			t134.setLocalRef(ctx,lMethodName__t.cv());
			c.f.fLine=245;
			if (g->Call(ctx,(PCV[]){nullptr,t134.cv(),t133.cv()},2,1334)) goto _0;
			g->Check(ctx);
		}
		lAttributes__t=KAttributes_3A_20.get();
		{
			Variant t135;
			c.f.fLine=249;
			if (g->GetMember(ctx,lAttributes__o.cv(),Kshared.cv(),t135.cv())) goto _0;
			Bool t136;
			if (!g->GetValue(ctx,(PCV[]){t136.cv(),t135.cv(),nullptr})) goto _0;
			if (!(t136.get())) goto _47;
		}
		g->AddString(lAttributes__t.get(),KShared_2C_20.get(),lAttributes__t.get());
_47:
		{
			Variant t138;
			c.f.fLine=253;
			if (g->GetMember(ctx,lAttributes__o.cv(),kx4DgsGRJUpg.cv(),t138.cv())) goto _0;
			Bool t139;
			if (!g->GetValue(ctx,(PCV[]){t139.cv(),t138.cv(),nullptr})) goto _0;
			if (!(t139.get())) goto _48;
		}
		g->AddString(lAttributes__t.get(),KServer_2C_20.get(),lAttributes__t.get());
_48:
		{
			Variant t141;
			c.f.fLine=257;
			if (g->GetMember(ctx,lAttributes__o.cv(),Kinvisible.cv(),t141.cv())) goto _0;
			Bool t142;
			if (!g->GetValue(ctx,(PCV[]){t142.cv(),t141.cv(),nullptr})) goto _0;
			if (!(t142.get())) goto _49;
		}
		g->AddString(lAttributes__t.get(),KInvisible_2C_20.get(),lAttributes__t.get());
_49:
		{
			Variant t144;
			c.f.fLine=268;
			if (g->GetMember(ctx,lAttributes__o.cv(),Kpreemptive.cv(),t144.cv())) goto _0;
			Bool t145;
			if (g->OperationOnAny(ctx,6,t144.cv(),Kcapable.cv(),t145.cv())) goto _0;
			if (!(t145.get())) goto _51;
		}
		g->AddString(lAttributes__t.get(),khj46qayLLyM.get(),lAttributes__t.get());
		goto _50;
_51:
		{
			Variant t147;
			c.f.fLine=271;
			if (g->GetMember(ctx,lAttributes__o.cv(),Kpreemptive.cv(),t147.cv())) goto _0;
			Bool t148;
			if (g->OperationOnAny(ctx,6,t147.cv(),Kincapable.cv(),t148.cv())) goto _0;
			if (!(t148.get())) goto _52;
		}
		g->AddString(lAttributes__t.get(),keYqLvjgs1KI.get(),lAttributes__t.get());
		goto _50;
_52:
		{
			Variant t150;
			c.f.fLine=274;
			if (g->GetMember(ctx,lAttributes__o.cv(),Kpreemptive.cv(),t150.cv())) goto _0;
			Bool t151;
			if (g->OperationOnAny(ctx,6,t150.cv(),Kindifferent.cv(),t151.cv())) goto _0;
			if (!(t151.get())) goto _53;
		}
		g->AddString(lAttributes__t.get(),k$tdAxHfqfk0.get(),lAttributes__t.get());
		goto _50;
_53:
_50:
		{
			Variant t153;
			c.f.fLine=279;
			if (g->GetMember(ctx,lAttributes__o.cv(),KpublishedSoap.cv(),t153.cv())) goto _0;
			Bool t154;
			if (!g->GetValue(ctx,(PCV[]){t154.cv(),t153.cv(),nullptr})) goto _0;
			if (!(t154.get())) goto _54;
		}
		g->AddString(lAttributes__t.get(),KSoap_2C_20.get(),lAttributes__t.get());
_54:
		{
			Variant t156;
			c.f.fLine=283;
			if (g->GetMember(ctx,lAttributes__o.cv(),KpublishedSql.cv(),t156.cv())) goto _0;
			Bool t157;
			if (!g->GetValue(ctx,(PCV[]){t157.cv(),t156.cv(),nullptr})) goto _0;
			if (!(t157.get())) goto _55;
		}
		g->AddString(lAttributes__t.get(),KSQL_2C_20.get(),lAttributes__t.get());
_55:
		{
			Variant t159;
			c.f.fLine=287;
			if (g->GetMember(ctx,lAttributes__o.cv(),KpublishedWeb.cv(),t159.cv())) goto _0;
			Bool t160;
			if (!g->GetValue(ctx,(PCV[]){t160.cv(),t159.cv(),nullptr})) goto _0;
			if (!(t160.get())) goto _56;
		}
		g->AddString(lAttributes__t.get(),KWeb_2C_20.get(),lAttributes__t.get());
_56:
		{
			Variant t162;
			c.f.fLine=291;
			if (g->GetMember(ctx,lAttributes__o.cv(),KpublishedWsdl.cv(),t162.cv())) goto _0;
			Bool t163;
			if (!g->GetValue(ctx,(PCV[]){t163.cv(),t162.cv(),nullptr})) goto _0;
			if (!(t163.get())) goto _57;
		}
		g->AddString(lAttributes__t.get(),KWSDL_2C_20.get(),lAttributes__t.get());
_57:
		{
			Long t165;
			t165=lAttributes__t.get().fLength;
			if (1>=t165.get()) goto _58;
		}
		{
			Long t167;
			t167=lAttributes__t.get().fLength;
			Long t168;
			t168=t167.get()-2;
			Txt t169;
			c.f.fLine=296;
			if (g->Call(ctx,(PCV[]){t169.cv(),lAttributes__t.cv(),Long(1).cv(),t168.cv()},3,12)) goto _0;
			Txt t170;
			g->AddString(t169.get(),lCR.get(),t170.get());
			g->AddString(t170.get(),lCR.get(),lAttributes__t.get());
		}
_58:
		{
			Long t172;
			c.f.fLine=299;
			if (g->Call(ctx,(PCV[]){t172.cv(),kvUWSXMd0W2g.cv(),lMethodCode__t.cv()},2,15)) goto _0;
			lPosition__i=t172.get();
		}
		lPosition__i=lPosition__i.get()+15;
		{
			Long t174;
			c.f.fLine=301;
			if (g->Call(ctx,(PCV[]){t174.cv(),lCR.cv(),lMethodCode__t.cv(),lPosition__i.cv()},3,15)) goto _0;
			llineEnd__i=t174.get();
		}
		{
			Long t175;
			t175=llineEnd__i.get()-lPosition__i.get();
			Txt t176;
			c.f.fLine=303;
			if (g->Call(ctx,(PCV[]){t176.cv(),lMethodCode__t.cv(),lPosition__i.cv(),t175.cv()},3,12)) goto _0;
			lcallSyntax__t=t176.get();
		}
_59:
		{
			Txt t177;
			c.f.fLine=305;
			if (g->Call(ctx,(PCV[]){t177.cv(),lcallSyntax__t.cv(),Long(1).cv(),Long(1).cv()},3,12)) goto _0;
			Bool t178;
			t178=g->CompareString(ctx,t177.get(),lSpace.get())==0;
			if (!(t178.get())) goto _60;
		}
		{
			Txt t179;
			c.f.fLine=306;
			if (g->Call(ctx,(PCV[]){t179.cv(),lcallSyntax__t.cv(),Long(2).cv()},2,12)) goto _0;
			lcallSyntax__t=t179.get();
		}
		goto _59;
_60:
		{
			Txt t180;
			c.f.fLine=309;
			if (g->Call(ctx,(PCV[]){t180.cv(),lcallSyntax__t.cv(),K_2D_2D_3E.cv(),K_2D_3E.cv()},3,233)) goto _0;
			lcallSyntax__t=t180.get();
		}
		lcallSyntaxParameters__t=lparameterBlock__t.get();
		{
			Txt t181;
			c.f.fLine=313;
			if (g->Call(ctx,(PCV[]){t181.cv(),lcallSyntaxParameters__t.cv(),kXztOPunYs9g.cv(),K.cv()},3,233)) goto _0;
			lcallSyntaxParameters__t=t181.get();
		}
		{
			Txt t182;
			c.f.fLine=314;
			if (g->Call(ctx,(PCV[]){t182.cv(),lcallSyntaxParameters__t.cv(),K_7C.cv(),K_20_3A_20.cv()},3,233)) goto _0;
			lcallSyntaxParameters__t=t182.get();
		}
		{
			Txt t183;
			g->AddString(lcallSyntax__t.get(),lCR.get(),t183.get());
			g->AddString(t183.get(),lcallSyntaxParameters__t.get(),lcallSyntax__t.get());
		}
		{
			Txt t185;
			c.f.fLine=319;
			if (g->Call(ctx,(PCV[]){t185.cv(),lcallSyntax__t.cv(),k2ucr6KMltt4.cv(),K.cv()},3,233)) goto _0;
			lcallSyntax__t=t185.get();
		}
		{
			Txt t186;
			c.f.fLine=320;
			if (g->Call(ctx,(PCV[]){t186.cv(),lcallSyntax__t.cv(),K_2F_2F_20Returns_3A.cv(),K.cv()},3,233)) goto _0;
			lcallSyntax__t=t186.get();
		}
		{
			Txt t187;
			g->AddString(lCR.get(),lCR.get(),t187.get());
			Txt t188;
			c.f.fLine=321;
			if (g->Call(ctx,(PCV[]){t188.cv(),lcallSyntax__t.cv(),t187.cv(),lCR.cv()},3,233)) goto _0;
			lcallSyntax__t=t188.get();
		}
		{
			Long t189;
			t189=lcallSyntax__t.get().fLength;
			Long t190;
			t190=t189.get()-1;
			Txt t191;
			c.f.fLine=322;
			if (g->Call(ctx,(PCV[]){t191.cv(),lcallSyntax__t.cv(),Long(1).cv(),t190.cv()},3,12)) goto _0;
			lcallSyntax__t=t191.get();
		}
		{
			Txt t192;
			c.f.fLine=324;
			if (g->Call(ctx,(PCV[]){t192.cv(),lcallSyntax__t.cv(),Long(1).cv(),Long(1).cv()},3,12)) goto _0;
			lFirstChars__t=t192.get();
		}
_61:
		{
			Bool t193;
			t193=g->CompareString(ctx,lFirstChars__t.get(),K_20.get())==0;
			if (!(t193.get())) goto _62;
		}
		{
			Txt t194;
			c.f.fLine=326;
			if (g->Call(ctx,(PCV[]){t194.cv(),lcallSyntax__t.cv(),Long(2).cv()},2,12)) goto _0;
			lcallSyntax__t=t194.get();
		}
		{
			Txt t195;
			c.f.fLine=327;
			if (g->Call(ctx,(PCV[]){t195.cv(),lcallSyntax__t.cv(),Long(1).cv(),Long(1).cv()},3,12)) goto _0;
			lFirstChars__t=t195.get();
		}
		goto _61;
_62:
		{
			Txt t196;
			g->AddString(lCR.get(),lAttributes__t.get(),t196.get());
			Txt t197;
			c.f.fLine=330;
			if (g->Call(ctx,(PCV[]){t197.cv(),lMethodCode__t.cv(),kdAQ7fWUqce8.cv(),t196.cv()},3,233)) goto _0;
			lMethodCode__t=t197.get();
		}
		{
			Txt t198;
			g->AddString(lCR.get(),lAttributes__t.get(),t198.get());
			Txt t199;
			c.f.fLine=331;
			if (g->Call(ctx,(PCV[]){t199.cv(),lMethodCode__t.cv(),kKqLvphAEyfI.cv(),t198.cv()},3,233)) goto _0;
			lMethodCode__t=t199.get();
		}
		{
			Txt t200;
			c.f.fLine=335;
			if (g->Call(ctx,(PCV[]){t200.cv(),lMethodCode__t.cv(),kFztatg2AeEk.cv(),K.cv()},3,233)) goto _0;
			lMethodCode__t=t200.get();
		}
		{
			Txt t201;
			c.f.fLine=336;
			if (g->Call(ctx,(PCV[]){t201.cv(),lMethodCode__t.cv(),kyUKfCm0Vmhg.cv(),K.cv()},3,233)) goto _0;
			lMethodCode__t=t201.get();
		}
		{
			Txt t202;
			c.f.fLine=338;
			if (g->Call(ctx,(PCV[]){t202.cv(),lMethodCode__t.cv(),K_20_20_2F_2F_20.cv(),K.cv()},3,233)) goto _0;
			lMethodCode__t=t202.get();
		}
		{
			Txt t203;
			c.f.fLine=339;
			if (g->Call(ctx,(PCV[]){t203.cv(),lMethodCode__t.cv(),K_2F_2F_20.cv(),K.cv()},3,233)) goto _0;
			lMethodCode__t=t203.get();
		}
		{
			Txt t204;
			c.f.fLine=342;
			if (g->Call(ctx,(PCV[]){t204.cv(),lMethodCode__t.cv(),K_0D_0D.cv(),khAhd0qeKOrE.cv()},3,233)) goto _0;
			lMethodCode__t=t204.get();
		}
		{
			Txt t205;
			c.f.fLine=343;
			if (g->Call(ctx,(PCV[]){t205.cv(),lMethodCode__t.cv(),K_0D.cv(),K_3Cbr_3E.cv()},3,233)) goto _0;
			lMethodCode__t=t205.get();
		}
		{
			Txt t206;
			c.f.fLine=344;
			if (g->Call(ctx,(PCV[]){t206.cv(),lMethodCode__t.cv(),khAhd0qeKOrE.cv(),K_0D_0D.cv()},3,233)) goto _0;
			lMethodCode__t=t206.get();
		}
		{
			Txt t207;
			g->AddString(lMethodCode__t.get(),lCR.get(),t207.get());
			g->AddString(t207.get(),lparameterBlock__t.get(),lMethodCode__t.get());
		}
		{
			Txt t209;
			c.f.fLine=348;
			if (g->Call(ctx,(PCV[]){t209.cv(),lMethodCode__t.cv(),Long(1).cv(),Long(2).cv()},3,12)) goto _0;
			lFirstChars__t=t209.get();
		}
_63:
		{
			Bool t210;
			t210=g->CompareString(ctx,lFirstChars__t.get(),K_0D_0D.get())==0;
			if (!(t210.get())) goto _64;
		}
		{
			Txt t211;
			c.f.fLine=350;
			if (g->Call(ctx,(PCV[]){t211.cv(),lMethodCode__t.cv(),Long(2).cv()},2,12)) goto _0;
			lMethodCode__t=t211.get();
		}
		{
			Txt t212;
			c.f.fLine=351;
			if (g->Call(ctx,(PCV[]){t212.cv(),lMethodCode__t.cv(),Long(1).cv(),Long(2).cv()},3,12)) goto _0;
			lFirstChars__t=t212.get();
		}
		goto _63;
_64:
		{
			Long t213;
			t213=lMethodCode__t.get().fLength;
			Txt t214;
			c.f.fLine=354;
			if (g->Call(ctx,(PCV[]){t214.cv(),lMethodCode__t.cv(),t213.cv(),Long(1).cv()},3,12)) goto _0;
			llastChar__t=t214.get();
		}
_65:
		{
			Bool t215;
			t215=g->CompareString(ctx,llastChar__t.get(),lCR.get())==0;
			Bool t216;
			t216=g->CompareString(ctx,llastChar__t.get(),lSpace.get())==0;
			if (!( t215.get()||t216.get())) goto _66;
		}
		{
			Long t218;
			t218=lMethodCode__t.get().fLength;
			Long t219;
			t219=t218.get()-1;
			Txt t220;
			c.f.fLine=356;
			if (g->Call(ctx,(PCV[]){t220.cv(),lMethodCode__t.cv(),Long(1).cv(),t219.cv()},3,12)) goto _0;
			lMethodCode__t=t220.get();
		}
		{
			Long t221;
			t221=lMethodCode__t.get().fLength;
			Txt t222;
			c.f.fLine=357;
			if (g->Call(ctx,(PCV[]){t222.cv(),lMethodCode__t.cv(),t221.cv(),Long(1).cv()},3,12)) goto _0;
			llastChar__t=t222.get();
		}
		goto _65;
_66:
		{
			Txt t223;
			c.f.fLine=360;
			if (g->Call(ctx,(PCV[]){t223.cv(),lMethodCode__t.cv(),KReturns_3A.cv(),K_2A_2AReturns_2A_2A.cv()},3,233)) goto _0;
			lMethodCode__t=t223.get();
		}
		{
			Txt t224;
			g->AddString(K_3C_21_2D_2D.get(),lcallSyntax__t.get(),t224.get());
			Txt t225;
			g->AddString(t224.get(),K_2D_2D_3E.get(),t225.get());
			Txt t226;
			g->AddString(t225.get(),lCR.get(),t226.get());
			Txt t227;
			g->AddString(t226.get(),K_23_23_20.get(),t227.get());
			Txt t228;
			g->AddString(t227.get(),lMethodName__t.get(),t228.get());
			Txt t229;
			g->AddString(t228.get(),lCR.get(),t229.get());
			g->AddString(t229.get(),lMethodCode__t.get(),lMethodCode__t.get());
		}
		{
			Txt t231;
			g->AddString(lCR.get(),K_2F_2F.get(),t231.get());
			Txt t232;
			c.f.fLine=364;
			if (g->Call(ctx,(PCV[]){t232.cv(),lMethodCode__t.cv(),t231.cv(),lCR.cv()},3,233)) goto _0;
			lMethodCode__t=t232.get();
		}
		{
			Txt t233;
			g->AddString(lCR.get(),K_2F.get(),t233.get());
			Txt t234;
			c.f.fLine=365;
			if (g->Call(ctx,(PCV[]){t234.cv(),lMethodCode__t.cv(),t233.cv(),lCR.cv()},3,233)) goto _0;
			lMethodCode__t=t234.get();
		}
		{
			Variant t235;
			c.f.fLine=367;
			if (g->GetMember(ctx,lAttributes__o.cv(),Kshared.cv(),t235.cv())) goto _0;
			Bool t236;
			if (!g->GetValue(ctx,(PCV[]){t236.cv(),t235.cv(),nullptr})) goto _0;
			Bool t237;
			t237=!(t236.get());
			Bool t238;
			t238=lexcludePrivate__b.get()&&t237.get();
			if (!(t238.get())) goto _67;
		}
		lMethodCode__t=K.get();
_67:
		c.f.fLine=372;
		lMethodComments__at.arrayElem(ctx,lCurrentMethod__i.get())=lMethodCode__t.get();
		if (ctx->doingAbort) goto _0;
_14:
		lCurrentMethod__i=lCurrentMethod__i.get()+1;
_13:
		if (lCurrentMethod__i.get()<=v2.get()) goto _15;
_16:
		{
			Ref t242;
			t242.setLocalRef(ctx,lMethodComments__at.cv());
			Long t243;
			c.f.fLine=376;
			if (g->Call(ctx,(PCV[]){t243.cv(),t242.cv()},1,274)) goto _0;
			lNumberOfMethods__i=t243.get();
		}
		lCurrentMethod__i=lNumberOfMethods__i.get();
		v4=1;
		goto _68;
_70:
		{
			Txt t244;
			c.f.fLine=378;
			t244=lMethodComments__at.arrayElem(ctx,lCurrentMethod__i.get()).get();
			Txt t245;
			t245=t244.get();
			Long t246;
			t246=t245.get().fLength;
			Bool t247;
			t247=0<t246.get();
			if (!(t247.get())) goto _72;
		}
		if (ctx->doingAbort) goto _0;
		goto _73;
_72:
		{
			Ref t248;
			t248.setLocalRef(ctx,lMethodNames__at.cv());
			c.f.fLine=380;
			if (g->Call(ctx,(PCV[]){nullptr,t248.cv(),lCurrentMethod__i.cv()},2,228)) goto _0;
		}
		{
			Ref t249;
			t249.setLocalRef(ctx,lMethodComments__at.cv());
			c.f.fLine=381;
			if (g->Call(ctx,(PCV[]){nullptr,t249.cv(),lCurrentMethod__i.cv()},2,228)) goto _0;
		}
_73:
_69:
		lCurrentMethod__i=lCurrentMethod__i.get()+-1;
_68:
		if (lCurrentMethod__i.get()>=v4.get()) goto _70;
_71:
		{
			Ref t252;
			t252.setLocalRef(ctx,lMethodComments__at.cv());
			Ref t253;
			t253.setLocalRef(ctx,lMethodNames__at.cv());
			c.f.fLine=386;
			if (g->Call(ctx,(PCV[]){nullptr,t253.cv(),t252.cv()},2,1193)) goto _0;
			g->Check(ctx);
		}
		goto _74;
_3:
		lMethodName__t=K.get();
		lToolTip__b=Bool(1).get();
		lexcludePrivate__b=Bool(0).get();
		{
			Long t254;
			t254=inNbExplicitParam;
			if (1!=t254.get()) goto _76;
		}
		c.f.fLine=398;
		lMethodName__t=Parm<Txt>(ctx,inParams,inNbParam,1).get();
		if (ctx->doingAbort) goto _0;
		goto _75;
_76:
		{
			Long t256;
			t256=inNbExplicitParam;
			if (2!=t256.get()) goto _77;
		}
		c.f.fLine=401;
		lMethodName__t=Parm<Txt>(ctx,inParams,inNbParam,1).get();
		if (ctx->doingAbort) goto _0;
		c.f.fLine=402;
		lToolTip__b=Parm<Bool>(ctx,inParams,inNbParam,2).get();
		if (ctx->doingAbort) goto _0;
		goto _75;
_77:
		{
			Long t258;
			t258=inNbExplicitParam;
			if (3!=t258.get()) goto _78;
		}
		c.f.fLine=405;
		lMethodName__t=Parm<Txt>(ctx,inParams,inNbParam,1).get();
		if (ctx->doingAbort) goto _0;
		c.f.fLine=406;
		lToolTip__b=Parm<Bool>(ctx,inParams,inNbParam,2).get();
		if (ctx->doingAbort) goto _0;
		c.f.fLine=407;
		lexcludePrivate__b=Parm<Bool>(ctx,inParams,inNbParam,3).get();
		if (ctx->doingAbort) goto _0;
		goto _75;
_78:
_75:
		if (!(lexcludePrivate__b.get())) goto _79;
		{
			Txt t260;
			c.f.fLine=412;
			if (g->Call(ctx,(PCV[]){t260.cv(),Long(4).cv()},1,485)) goto _0;
			g->Check(ctx);
			Txt t261;
			g->AddString(t260.get(),KDocumentation.get(),t261.get());
			Txt t262;
			g->AddString(t261.get(),K_3A.get(),t262.get());
			Txt t263;
			g->AddString(t262.get(),KMethods.get(),t263.get());
			g->AddString(t263.get(),K_3A.get(),ldocumentationPath__t.get());
		}
		{
			Long t265;
			c.f.fLine=413;
			if (g->Call(ctx,(PCV[]){t265.cv(),ldocumentationPath__t.cv()},1,476)) goto _0;
			g->Check(ctx);
			if (0!=t265.get()) goto _80;
		}
		c.f.fLine=414;
		if (g->Call(ctx,(PCV[]){nullptr,ldocumentationPath__t.cv(),Long(1).cv()},2,693)) goto _0;
		g->Check(ctx);
_80:
		c.f.fLine=416;
		if (g->Call(ctx,(PCV[]){nullptr,ldocumentationPath__t.cv()},1,475)) goto _0;
		g->Check(ctx);
_79:
		{
			Txt t267;
			c.f.fLine=424;
			if (g->Call(ctx,(PCV[]){t267.cv()},0,684)) goto _0;
			g->Check(ctx);
			Bool t268;
			t268=lexcludePrivate__b.get();
			Bool t269;
			t269=lToolTip__b.get();
			Long t270;
			if (g->Call(ctx,(PCV[]){t270.cv(),t267.cv(),lStackSize__i.cv(),lprocessName__t.cv(),lMethodName__t.cv(),t269.cv(),t268.cv(),Ref((optyp)3).cv()},7,317)) goto _0;
			lProcessID__i=t270.get();
		}
		c.f.fLine=426;
		if (g->Call(ctx,(PCV[]){nullptr,lProcessID__i.cv()},1,320)) goto _0;
		g->Check(ctx);
		c.f.fLine=427;
		if (g->Call(ctx,(PCV[]){nullptr,lProcessID__i.cv()},1,325)) goto _0;
		g->Check(ctx);
		c.f.fLine=428;
		if (g->Call(ctx,(PCV[]){nullptr,lProcessID__i.cv()},1,326)) goto _0;
		g->Check(ctx);
_74:
_0:
_1:
;
	}

}
