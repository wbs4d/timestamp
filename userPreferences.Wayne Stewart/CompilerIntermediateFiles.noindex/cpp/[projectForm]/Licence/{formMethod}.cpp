extern unsigned char D_form_p_Licence[];
void form_p_Licence( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_form_p_Licence);
	if (!ctx->doingAbort && c.f.fLine==0) {
		{
			Long t0;
			c.f.fLine=1;
			if (g->Call(ctx,(PCV[]){t0.cv()},0,388)) goto _0;
			g->Check(ctx);
			Bool t1;
			t1=22==t0.get();
			Long t2;
			if (g->Call(ctx,(PCV[]){t2.cv()},0,388)) goto _0;
			Bool t3;
			t3=10==t2.get();
			if (!( t1.get()||t3.get())) goto _2;
		}
		c.f.fLine=2;
		if (g->Call(ctx,(PCV[]){nullptr},0,270)) goto _0;
		g->Check(ctx);
_2:
_0:
_1:
;
	}

}
