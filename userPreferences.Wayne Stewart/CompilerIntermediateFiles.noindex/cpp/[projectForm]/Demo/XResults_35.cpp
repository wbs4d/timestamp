extern Txt K1_2F5;
extern Txt K2_2F5;
extern Txt K3_2F5;
extern Txt K4_2F5;
extern Txt K5_2F5;
extern Txt KCol__01;
extern Txt KCol__02;
extern Txt KCol__03;
extern Txt KCol__04;
extern Txt KCol__05;
extern Txt KDuration_3A_20;
extern Txt KPreemptive;
extern Txt KSignal;
extern Txt KX;
extern Txt KXResults;
extern Txt K_20ms_2E;
extern Txt K_23_23_23_2C_23_230;
extern Txt koM_Vm4W5mf0;
extern int32_t R_proc_TS__GETTIMESTAMP;
extern int32_t R_proc_TS__GETTIMESTAMP__PE;
extern unsigned char D_obj_p_30_Demo_00XResults[];
void obj_p_30_Demo_00XResults( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_obj_p_30_Demo_00XResults);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Long lpreemptive__i;
		Long v0;
		Long v1;
		Long v2;
		Long v3;
		Txt lbuttonLabel__t;
		Long lStart;
		Long lsignal__i;
		Txt ltemplate__t;
		Ptr lCol__03__ptr;
		Long lMaxSize;
		Ptr lsignal__ptr;
		Ptr lCol__04__ptr;
		Ptr lpreemptive__ptr;
		Ptr lCol__05__ptr;
		Ptr lCol__01__ptr;
		Ptr lCol__02__ptr;
		Long lSize;
		ltemplate__t=koM_Vm4W5mf0.get();
		lMaxSize=1000;
		{
			Long t0;
			t0=lMaxSize.get()*5;
			Txt t1;
			c.f.fLine=12;
			if (g->Call(ctx,(PCV[]){t1.cv(),t0.cv(),K_23_23_23_2C_23_230.cv()},2,10)) goto _0;
			Txt t2;
			if (g->Call(ctx,(PCV[]){t2.cv(),ltemplate__t.cv(),KX.cv(),t1.cv()},3,233)) goto _0;
			lbuttonLabel__t=t2.get();
		}
		{
			Long t3;
			c.f.fLine=15;
			if (g->Call(ctx,(PCV[]){t3.cv()},0,388)) goto _0;
			g->Check(ctx);
			if (4!=t3.get()) goto _3;
		}
		{
			Ptr t5;
			c.f.fLine=21;
			if (g->Call(ctx,(PCV[]){t5.cv(),Long(3).cv(),KPreemptive.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lpreemptive__ptr=t5.get();
		}
		{
			Ptr t6;
			c.f.fLine=22;
			if (g->Call(ctx,(PCV[]){t6.cv(),Long(3).cv(),KSignal.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lsignal__ptr=t6.get();
		}
		{
			Long t7;
			c.f.fLine=23;
			if (!g->GetValue(ctx,(PCV[]){t7.cv(),lpreemptive__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			lpreemptive__i=t7.get();
		}
		lsignal__i=1;
		{
			Ptr t8;
			c.f.fLine=26;
			if (g->Call(ctx,(PCV[]){t8.cv(),Long(3).cv(),KCol__01.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__01__ptr=t8.get();
		}
		{
			Ref t9;
			c.f.fLine=27;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t9.cv(),lCol__01__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t9.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		{
			Ptr t10;
			c.f.fLine=29;
			if (g->Call(ctx,(PCV[]){t10.cv(),Long(3).cv(),KCol__02.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__02__ptr=t10.get();
		}
		{
			Ref t11;
			c.f.fLine=30;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t11.cv(),lCol__02__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t11.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		{
			Ptr t12;
			c.f.fLine=32;
			if (g->Call(ctx,(PCV[]){t12.cv(),Long(3).cv(),KCol__03.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__03__ptr=t12.get();
		}
		{
			Ref t13;
			c.f.fLine=33;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t13.cv(),lCol__03__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t13.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		{
			Ptr t14;
			c.f.fLine=35;
			if (g->Call(ctx,(PCV[]){t14.cv(),Long(3).cv(),KCol__04.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__04__ptr=t14.get();
		}
		{
			Ref t15;
			c.f.fLine=36;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t15.cv(),lCol__04__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t15.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		{
			Ptr t16;
			c.f.fLine=38;
			if (g->Call(ctx,(PCV[]){t16.cv(),Long(3).cv(),KCol__05.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__05__ptr=t16.get();
		}
		{
			Ref t17;
			c.f.fLine=39;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t17.cv(),lCol__05__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t17.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		{
			Long t18;
			c.f.fLine=41;
			if (g->Call(ctx,(PCV[]){t18.cv()},0,459)) goto _0;
			lStart=t18.get();
		}
		{
			Bool t19;
			t19=1==lpreemptive__i.get();
			Bool t20;
			t20=1==lsignal__i.get();
			if (!( t19.get()&&t20.get())) goto _5;
		}
		lSize=1;
		v0=lMaxSize.get();
		goto _6;
_8:
		{
			Long t22;
			t22=0;
			Txt t23;
			t23=K1_2F5.get();
			Txt t24;
			c.f.fLine=47;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP__PE,1,2,(PCV[]){t24.cv(),t23.cv(),t22.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t24.cv(),lCol__01__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Long t25;
			t25=0;
			Txt t26;
			t26=K2_2F5.get();
			Txt t27;
			c.f.fLine=48;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP__PE,1,2,(PCV[]){t27.cv(),t26.cv(),t25.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t27.cv(),lCol__02__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Long t28;
			t28=0;
			Txt t29;
			t29=K3_2F5.get();
			Txt t30;
			c.f.fLine=49;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP__PE,1,2,(PCV[]){t30.cv(),t29.cv(),t28.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t30.cv(),lCol__03__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Long t31;
			t31=0;
			Txt t32;
			t32=K4_2F5.get();
			Txt t33;
			c.f.fLine=50;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP__PE,1,2,(PCV[]){t33.cv(),t32.cv(),t31.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t33.cv(),lCol__04__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Long t34;
			t34=0;
			Txt t35;
			t35=K5_2F5.get();
			Txt t36;
			c.f.fLine=51;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP__PE,1,2,(PCV[]){t36.cv(),t35.cv(),t34.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t36.cv(),lCol__05__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
_7:
		lSize=lSize.get()+1;
_6:
		if (lSize.get()<=v0.get()) goto _8;
_9:
		goto _4;
_5:
		if (1!=lpreemptive__i.get()) goto _10;
		goto _4;
_10:
		lSize=1;
		v2=lMaxSize.get();
		goto _11;
_13:
		{
			Long t40;
			t40=0;
			Txt t41;
			t41=K1_2F5.get();
			Txt t42;
			c.f.fLine=62;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP,1,2,(PCV[]){t42.cv(),t41.cv(),t40.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t42.cv(),lCol__01__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Long t43;
			t43=0;
			Txt t44;
			t44=K2_2F5.get();
			Txt t45;
			c.f.fLine=63;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP,1,2,(PCV[]){t45.cv(),t44.cv(),t43.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t45.cv(),lCol__02__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Long t46;
			t46=0;
			Txt t47;
			t47=K3_2F5.get();
			Txt t48;
			c.f.fLine=64;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP,1,2,(PCV[]){t48.cv(),t47.cv(),t46.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t48.cv(),lCol__03__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Long t49;
			t49=0;
			Txt t50;
			t50=K4_2F5.get();
			Txt t51;
			c.f.fLine=65;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP,1,2,(PCV[]){t51.cv(),t50.cv(),t49.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t51.cv(),lCol__04__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Long t52;
			t52=0;
			Txt t53;
			t53=K5_2F5.get();
			Txt t54;
			c.f.fLine=66;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP,1,2,(PCV[]){t54.cv(),t53.cv(),t52.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t54.cv(),lCol__05__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
_12:
		lSize=lSize.get()+1;
_11:
		if (lSize.get()<=v2.get()) goto _13;
_14:
_4:
		{
			Variant t57;
			c.f.fLine=74;
			if (!g->GetValue(ctx,(PCV[]){t57.cv(),lpreemptive__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			Bool t58;
			if (g->OperationOnAny(ctx,6,t57.cv(),Num(1).cv(),t58.cv())) goto _0;
			if (!(t58.get())) goto _15;
		}
		goto _16;
_15:
_16:
		{
			Long t59;
			c.f.fLine=82;
			if (g->Call(ctx,(PCV[]){t59.cv()},0,459)) goto _0;
			Long t60;
			t60=t59.get()-lStart.get();
			Txt t61;
			if (g->Call(ctx,(PCV[]){t61.cv(),t60.cv()},1,10)) goto _0;
			Txt t62;
			g->AddString(KDuration_3A_20.get(),t61.get(),t62.get());
			Txt t63;
			g->AddString(t62.get(),K_20ms_2E.get(),t63.get());
			if (g->Call(ctx,(PCV[]){nullptr,t63.cv()},1,41)) goto _0;
			g->Check(ctx);
		}
		goto _2;
_3:
		{
			Long t64;
			c.f.fLine=85;
			if (g->Call(ctx,(PCV[]){t64.cv()},0,388)) goto _0;
			g->Check(ctx);
			if (1!=t64.get()) goto _17;
		}
		c.f.fLine=87;
		if (g->Call(ctx,(PCV[]){nullptr,Ref((optyp)3).cv(),KXResults.cv(),lbuttonLabel__t.cv()},3,194)) goto _0;
		g->Check(ctx);
		goto _2;
_17:
_2:
_0:
_1:
;
	}

}
