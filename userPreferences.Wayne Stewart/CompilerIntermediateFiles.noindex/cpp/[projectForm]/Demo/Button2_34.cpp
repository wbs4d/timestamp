extern Txt K1_2F5;
extern Txt K2_2F5;
extern Txt K3_2F5;
extern Txt K4_2F5;
extern Txt K5_2F5;
extern Txt KCol__01;
extern Txt KCol__02;
extern Txt KCol__03;
extern Txt KCol__04;
extern Txt KCol__05;
extern Txt KPreemptive;
extern Txt kin89jWOO9_0;
extern int32_t R_proc_TS__GETTIMESTAMP;
extern int32_t R_proc_TS__GETTIMESTAMP__PE;
extern unsigned char D_obj_p_29_Demo_00Button2[];
void obj_p_29_Demo_00Button2( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_obj_p_29_Demo_00Button2);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Long lStart;
		Ptr lCol__03__ptr;
		Long lMaxSize;
		Ptr lCol__04__ptr;
		Ptr lCol__05__ptr;
		Ptr lCol__01__ptr;
		Ptr lCol__02__ptr;
		Long lSize;
		Ptr lbutton__ptr;
		lMaxSize=100000;
		{
			Ptr t0;
			c.f.fLine=5;
			if (g->Call(ctx,(PCV[]){t0.cv(),Long(3).cv(),KPreemptive.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lbutton__ptr=t0.get();
		}
		{
			Ptr t1;
			c.f.fLine=7;
			if (g->Call(ctx,(PCV[]){t1.cv(),Long(3).cv(),KCol__01.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__01__ptr=t1.get();
		}
		{
			Ref t2;
			c.f.fLine=8;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t2.cv(),lCol__01__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t2.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		{
			Ptr t3;
			c.f.fLine=10;
			if (g->Call(ctx,(PCV[]){t3.cv(),Long(3).cv(),KCol__02.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__02__ptr=t3.get();
		}
		{
			Ref t4;
			c.f.fLine=11;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t4.cv(),lCol__02__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t4.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		{
			Ptr t5;
			c.f.fLine=13;
			if (g->Call(ctx,(PCV[]){t5.cv(),Long(3).cv(),KCol__03.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__03__ptr=t5.get();
		}
		{
			Ref t6;
			c.f.fLine=14;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t6.cv(),lCol__03__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t6.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		{
			Ptr t7;
			c.f.fLine=16;
			if (g->Call(ctx,(PCV[]){t7.cv(),Long(3).cv(),KCol__04.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__04__ptr=t7.get();
		}
		{
			Ref t8;
			c.f.fLine=17;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t8.cv(),lCol__04__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t8.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		{
			Ptr t9;
			c.f.fLine=19;
			if (g->Call(ctx,(PCV[]){t9.cv(),Long(3).cv(),KCol__05.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__05__ptr=t9.get();
		}
		{
			Ref t10;
			c.f.fLine=20;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t10.cv(),lCol__05__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t10.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		{
			Long t11;
			c.f.fLine=24;
			if (g->Call(ctx,(PCV[]){t11.cv()},0,459)) goto _0;
			lStart=t11.get()+1000;
		}
		lSize=0;
		{
			Variant t13;
			c.f.fLine=27;
			if (!g->GetValue(ctx,(PCV[]){t13.cv(),lbutton__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			Bool t14;
			if (g->OperationOnAny(ctx,6,t13.cv(),Num(1).cv(),t14.cv())) goto _0;
			if (!(t14.get())) goto _2;
		}
_4:
		lSize=lSize.get()+1;
		{
			Long t16;
			t16=0;
			Txt t17;
			t17=K1_2F5.get();
			Txt t18;
			c.f.fLine=30;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP__PE,1,2,(PCV[]){t18.cv(),t17.cv(),t16.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t18.cv(),lCol__01__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Long t19;
			t19=0;
			Txt t20;
			t20=K2_2F5.get();
			Txt t21;
			c.f.fLine=31;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP__PE,1,2,(PCV[]){t21.cv(),t20.cv(),t19.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t21.cv(),lCol__02__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Long t22;
			t22=0;
			Txt t23;
			t23=K3_2F5.get();
			Txt t24;
			c.f.fLine=32;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP__PE,1,2,(PCV[]){t24.cv(),t23.cv(),t22.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t24.cv(),lCol__03__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Long t25;
			t25=0;
			Txt t26;
			t26=K4_2F5.get();
			Txt t27;
			c.f.fLine=33;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP__PE,1,2,(PCV[]){t27.cv(),t26.cv(),t25.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t27.cv(),lCol__04__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Long t28;
			t28=0;
			Txt t29;
			t29=K5_2F5.get();
			Txt t30;
			c.f.fLine=34;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP__PE,1,2,(PCV[]){t30.cv(),t29.cv(),t28.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t30.cv(),lCol__05__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
_3:
		{
			Long t31;
			c.f.fLine=35;
			if (g->Call(ctx,(PCV[]){t31.cv()},0,459)) goto _0;
			Bool t32;
			t32=t31.get()>lStart.get();
			Bool t33;
			t33=lSize.get()>=lMaxSize.get();
			if (!( t32.get()||t33.get())) goto _4;
		}
_5:
		goto _6;
_2:
_8:
		lSize=lSize.get()+1;
		{
			Long t36;
			t36=0;
			Txt t37;
			t37=K1_2F5.get();
			Txt t38;
			c.f.fLine=39;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP,1,2,(PCV[]){t38.cv(),t37.cv(),t36.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t38.cv(),lCol__01__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Long t39;
			t39=0;
			Txt t40;
			t40=K2_2F5.get();
			Txt t41;
			c.f.fLine=40;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP,1,2,(PCV[]){t41.cv(),t40.cv(),t39.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t41.cv(),lCol__02__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Long t42;
			t42=0;
			Txt t43;
			t43=K3_2F5.get();
			Txt t44;
			c.f.fLine=41;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP,1,2,(PCV[]){t44.cv(),t43.cv(),t42.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t44.cv(),lCol__03__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Long t45;
			t45=0;
			Txt t46;
			t46=K4_2F5.get();
			Txt t47;
			c.f.fLine=42;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP,1,2,(PCV[]){t47.cv(),t46.cv(),t45.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t47.cv(),lCol__04__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
		{
			Long t48;
			t48=0;
			Txt t49;
			t49=K5_2F5.get();
			Txt t50;
			c.f.fLine=43;
			if (g->CallRemote(glob,ctx,0,R_proc_TS__GETTIMESTAMP,1,2,(PCV[]){t50.cv(),t49.cv(),t48.cv()})) goto _0;
			if (!g->SetValue(ctx,(PCV[]){t50.cv(),lCol__05__ptr.cv(),(PCV)-1,lSize.cv(),nullptr})) goto _0;
		}
_7:
		{
			Long t51;
			c.f.fLine=44;
			if (g->Call(ctx,(PCV[]){t51.cv()},0,459)) goto _0;
			Bool t52;
			t52=t51.get()>lStart.get();
			Bool t53;
			t53=lSize.get()>=lMaxSize.get();
			if (!( t52.get()||t53.get())) goto _8;
		}
_9:
_6:
		{
			Ref t55;
			c.f.fLine=47;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t55.cv(),lCol__01__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t55.cv(),lSize.cv()},2,222)) goto _0;
		}
		{
			Ref t56;
			c.f.fLine=48;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t56.cv(),lCol__02__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t56.cv(),lSize.cv()},2,222)) goto _0;
		}
		{
			Ref t57;
			c.f.fLine=49;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t57.cv(),lCol__03__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t57.cv(),lSize.cv()},2,222)) goto _0;
		}
		{
			Ref t58;
			c.f.fLine=50;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t58.cv(),lCol__04__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t58.cv(),lSize.cv()},2,222)) goto _0;
		}
		{
			Ref t59;
			c.f.fLine=51;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t59.cv(),lCol__05__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t59.cv(),lSize.cv()},2,222)) goto _0;
		}
		{
			Txt t60;
			c.f.fLine=55;
			if (g->Call(ctx,(PCV[]){t60.cv(),lSize.cv()},1,10)) goto _0;
			Txt t61;
			g->AddString(kin89jWOO9_0.get(),t60.get(),t61.get());
			if (g->Call(ctx,(PCV[]){nullptr,t61.cv()},1,41)) goto _0;
			g->Check(ctx);
		}
_0:
_1:
;
	}

}
