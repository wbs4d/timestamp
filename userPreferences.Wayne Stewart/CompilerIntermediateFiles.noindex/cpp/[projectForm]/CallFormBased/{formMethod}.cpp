extern Txt KCol__01;
extern Txt KCol__02;
extern Txt KCol__03;
extern Txt KCol__04;
extern Txt KCol__05;
extern unsigned char D_form_p_CallFormBased[];
void form_p_CallFormBased( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_form_p_CallFormBased);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Ptr lCol__03__ptr;
		Ptr lCol__04__ptr;
		Ptr lCol__05__ptr;
		Ptr lCol__01__ptr;
		Ptr lCol__02__ptr;
		{
			Long t0;
			c.f.fLine=3;
			if (g->Call(ctx,(PCV[]){t0.cv()},0,388)) goto _0;
			g->Check(ctx);
			if (1!=t0.get()) goto _2;
		}
		{
			Ptr t2;
			c.f.fLine=6;
			if (g->Call(ctx,(PCV[]){t2.cv(),Long(3).cv(),KCol__01.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__01__ptr=t2.get();
		}
		{
			Ref t3;
			c.f.fLine=7;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t3.cv(),lCol__01__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t3.cv(),Long(0).cv()},2,222)) goto _0;
		}
		{
			Ptr t4;
			c.f.fLine=9;
			if (g->Call(ctx,(PCV[]){t4.cv(),Long(3).cv(),KCol__02.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__02__ptr=t4.get();
		}
		{
			Ref t5;
			c.f.fLine=10;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t5.cv(),lCol__02__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t5.cv(),Long(0).cv()},2,222)) goto _0;
		}
		{
			Ptr t6;
			c.f.fLine=12;
			if (g->Call(ctx,(PCV[]){t6.cv(),Long(3).cv(),KCol__03.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__03__ptr=t6.get();
		}
		{
			Ref t7;
			c.f.fLine=13;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t7.cv(),lCol__03__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t7.cv(),Long(0).cv()},2,222)) goto _0;
		}
		{
			Ptr t8;
			c.f.fLine=15;
			if (g->Call(ctx,(PCV[]){t8.cv(),Long(3).cv(),KCol__04.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__04__ptr=t8.get();
		}
		{
			Ref t9;
			c.f.fLine=16;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t9.cv(),lCol__04__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t9.cv(),Long(0).cv()},2,222)) goto _0;
		}
		{
			Ptr t10;
			c.f.fLine=18;
			if (g->Call(ctx,(PCV[]){t10.cv(),Long(3).cv(),KCol__05.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__05__ptr=t10.get();
		}
		{
			Ref t11;
			c.f.fLine=19;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t11.cv(),lCol__05__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t11.cv(),Long(0).cv()},2,222)) goto _0;
		}
_2:
_0:
_1:
;
	}

}
