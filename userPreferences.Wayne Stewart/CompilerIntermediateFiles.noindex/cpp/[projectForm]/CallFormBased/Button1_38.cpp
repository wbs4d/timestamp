extern Txt K1_2F5;
extern Txt K2_2F5;
extern Txt K3_2F5;
extern Txt K4_2F5;
extern Txt K5_2F5;
extern Txt KCol__01;
extern Txt KCol__02;
extern Txt KCol__03;
extern Txt KCol__04;
extern Txt KCol__05;
extern Txt KGet_20Values;
extern Txt KTS__getXValues;
extern Txt Kstart;
extern Txt KwindowID;
extern unsigned char D_obj_p_33_CallFormBased_00Button1[];
void obj_p_33_CallFormBased_00Button1( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_obj_p_33_CallFormBased_00Button1);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Long lpreemptive__i;
		Long lStart;
		Long lsignal__i;
		Ptr lCol__03__ptr;
		Long lMaxSize;
		Ptr lsignal__ptr;
		Ptr lCol__04__ptr;
		Ptr lpreemptive__ptr;
		Ptr lCol__05__ptr;
		Ptr lCol__01__ptr;
		Ptr lCol__02__ptr;
		Long lSize;
		lMaxSize=0;
		{
			Ptr t0;
			c.f.fLine=8;
			if (g->Call(ctx,(PCV[]){t0.cv(),Long(3).cv(),KCol__01.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__01__ptr=t0.get();
		}
		{
			Ref t1;
			c.f.fLine=9;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t1.cv(),lCol__01__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t1.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		{
			Ptr t2;
			c.f.fLine=11;
			if (g->Call(ctx,(PCV[]){t2.cv(),Long(3).cv(),KCol__02.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__02__ptr=t2.get();
		}
		{
			Ref t3;
			c.f.fLine=12;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t3.cv(),lCol__02__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t3.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		{
			Ptr t4;
			c.f.fLine=14;
			if (g->Call(ctx,(PCV[]){t4.cv(),Long(3).cv(),KCol__03.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__03__ptr=t4.get();
		}
		{
			Ref t5;
			c.f.fLine=15;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t5.cv(),lCol__03__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t5.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		{
			Ptr t6;
			c.f.fLine=17;
			if (g->Call(ctx,(PCV[]){t6.cv(),Long(3).cv(),KCol__04.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__04__ptr=t6.get();
		}
		{
			Ref t7;
			c.f.fLine=18;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t7.cv(),lCol__04__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t7.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		{
			Ptr t8;
			c.f.fLine=20;
			if (g->Call(ctx,(PCV[]){t8.cv(),Long(3).cv(),KCol__05.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__05__ptr=t8.get();
		}
		{
			Ref t9;
			c.f.fLine=21;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t9.cv(),lCol__05__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t9.cv(),lMaxSize.cv()},2,222)) goto _0;
		}
		lMaxSize=1000;
		{
			Obj t10;
			c.f.fLine=26;
			if (g->Call(ctx,(PCV[]){t10.cv()},0,1466)) goto _0;
			Long t11;
			if (g->Call(ctx,(PCV[]){t11.cv()},0,459)) goto _0;
			if (g->SetMember(ctx,t10.cv(),Kstart.cv(),t11.cv())) goto _0;
		}
		{
			Obj t12;
			c.f.fLine=28;
			if (g->Call(ctx,(PCV[]){t12.cv()},0,1466)) goto _0;
			Variant t13;
			if (g->GetMember(ctx,t12.cv(),KwindowID.cv(),t13.cv())) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,KGet_20Values.cv(),KTS__getXValues.cv(),lMaxSize.cv(),K1_2F5.cv(),t13.cv()},5,1389)) goto _0;
			g->Check(ctx);
		}
		{
			Obj t14;
			c.f.fLine=29;
			if (g->Call(ctx,(PCV[]){t14.cv()},0,1466)) goto _0;
			Variant t15;
			if (g->GetMember(ctx,t14.cv(),KwindowID.cv(),t15.cv())) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,KGet_20Values.cv(),KTS__getXValues.cv(),lMaxSize.cv(),K2_2F5.cv(),t15.cv()},5,1389)) goto _0;
			g->Check(ctx);
		}
		{
			Obj t16;
			c.f.fLine=30;
			if (g->Call(ctx,(PCV[]){t16.cv()},0,1466)) goto _0;
			Variant t17;
			if (g->GetMember(ctx,t16.cv(),KwindowID.cv(),t17.cv())) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,KGet_20Values.cv(),KTS__getXValues.cv(),lMaxSize.cv(),K3_2F5.cv(),t17.cv()},5,1389)) goto _0;
			g->Check(ctx);
		}
		{
			Obj t18;
			c.f.fLine=31;
			if (g->Call(ctx,(PCV[]){t18.cv()},0,1466)) goto _0;
			Variant t19;
			if (g->GetMember(ctx,t18.cv(),KwindowID.cv(),t19.cv())) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,KGet_20Values.cv(),KTS__getXValues.cv(),lMaxSize.cv(),K4_2F5.cv(),t19.cv()},5,1389)) goto _0;
			g->Check(ctx);
		}
		{
			Obj t20;
			c.f.fLine=32;
			if (g->Call(ctx,(PCV[]){t20.cv()},0,1466)) goto _0;
			Variant t21;
			if (g->GetMember(ctx,t20.cv(),KwindowID.cv(),t21.cv())) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,KGet_20Values.cv(),KTS__getXValues.cv(),lMaxSize.cv(),K5_2F5.cv(),t21.cv()},5,1389)) goto _0;
			g->Check(ctx);
		}
_0:
_1:
;
	}

}
