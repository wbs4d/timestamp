extern Txt KTS;
extern Txt KlastCall;
extern Txt KnumberOfCalls;
extern unsigned char D_proc_TS__INIT[];
void proc_TS__INIT( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_TS__INIT);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Obj l__4D__auto__mutex__0;
		{
			Obj t0;
			c.f.fLine=1;
			if (g->Call(ctx,(PCV[]){t0.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Variant t1;
			if (g->GetMember(ctx,t0.cv(),KTS.cv(),t1.cv())) goto _0;
			Bool t2;
			if (g->OperationOnAny(ctx,6,t1.cv(),Value_null().cv(),t2.cv())) goto _0;
			if (!(t2.get())) goto _2;
		}
		{
			Obj t3;
			c.f.fLine=2;
			if (g->Call(ctx,(PCV[]){t3.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Obj t4;
			if (g->Call(ctx,(PCV[]){t4.cv(),t3.cv()},1,1529)) goto _0;
			l__4D__auto__mutex__0=t4.get();
		}
		{
			Obj t5;
			c.f.fLine=3;
			if (g->Call(ctx,(PCV[]){t5.cv()},0,1525)) goto _0;
			g->Check(ctx);
			Obj t6;
			if (g->Call(ctx,(PCV[]){t6.cv(),KlastCall.cv(),Long(0).cv(),KnumberOfCalls.cv(),Long(0).cv()},4,1526)) goto _0;
			if (g->SetMember(ctx,t5.cv(),KTS.cv(),t6.cv())) goto _0;
		}
		{
			Obj t7;
			l__4D__auto__mutex__0=t7.get();
		}
_2:
_0:
_1:
;
	}

}
