extern Txt K;
extern Txt K00;
extern Txt K0000;
extern Txt KT;
extern Txt K_2D;
extern Txt K_3A;
Asm4d_Proc proc_TS__DATEANDTIMETOISO;
extern unsigned char D_proc_TS__DATEANDTIMETOISO[];
void proc_TS__DATEANDTIMETOISO( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_TS__DATEANDTIMETOISO);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Txt ltime__t;
		Date ldate__d;
		Long lnumberOfParameters__i;
		Txt lminutes__t;
		Txt lhours__t;
		Txt lseconds__t;
		Txt ldateTimeStamp__t;
		Time ltime__h;
		new ( outResult) Txt();
		{
			Long t0;
			t0=inNbExplicitParam;
			lnumberOfParameters__i=t0.get();
		}
		if (0!=lnumberOfParameters__i.get()) goto _2;
		{
			Date t2;
			c.f.fLine=35;
			if (g->Call(ctx,(PCV[]){t2.cv()},0,33)) goto _0;
			Time t3;
			if (g->Call(ctx,(PCV[]){t3.cv()},0,178)) goto _0;
			Time t4;
			t4=t3.get();
			Date t5;
			t5=t2.get();
			Txt t6;
			proc_TS__DATEANDTIMETOISO(glob,ctx,2,2,(PCV[]){t5.cv(),t4.cv()},t6.cv());
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
			ldateTimeStamp__t=t6.get();
		}
		goto _3;
_2:
		if (1!=lnumberOfParameters__i.get()) goto _5;
		c.f.fLine=39;
		ldate__d=Parm<Date>(ctx,inParams,inNbParam,1).get();
		if (ctx->doingAbort) goto _0;
		ltime__h=Time(0).get();
		goto _4;
_5:
		c.f.fLine=43;
		ldate__d=Parm<Date>(ctx,inParams,inNbParam,1).get();
		if (ctx->doingAbort) goto _0;
		c.f.fLine=44;
		ltime__h=Parm<Time>(ctx,inParams,inNbParam,2).get();
		if (ctx->doingAbort) goto _0;
_4:
		{
			Bool t8;
			t8=ldate__d.get()==Date(0,0,0).get();
			Bool t9;
			t9=ltime__h.get()==Time(0).get();
			if (!( t8.get()&&t9.get())) goto _7;
		}
		ldateTimeStamp__t=K.get();
		goto _6;
_7:
		{
			Bool t11;
			t11=ldate__d.get()==Date(0,0,0).get();
			if (!(t11.get())) goto _8;
		}
		goto _6;
_8:
		if (1!=lnumberOfParameters__i.get()) goto _9;
		c.f.fLine=55;
		ldate__d=Parm<Date>(ctx,inParams,inNbParam,1).get();
		if (ctx->doingAbort) goto _0;
		ltime__h=Time(0).get();
		goto _6;
_9:
		c.f.fLine=59;
		ldate__d=Parm<Date>(ctx,inParams,inNbParam,1).get();
		if (ctx->doingAbort) goto _0;
		c.f.fLine=60;
		ltime__h=Parm<Time>(ctx,inParams,inNbParam,2).get();
		if (ctx->doingAbort) goto _0;
_6:
		{
			Date t13;
			t13=ldate__d.get();
			Long t14;
			c.f.fLine=64;
			if (g->Call(ctx,(PCV[]){t14.cv(),t13.cv()},1,25)) goto _0;
			Txt t15;
			if (g->Call(ctx,(PCV[]){t15.cv(),t14.cv(),K0000.cv()},2,10)) goto _0;
			ldateTimeStamp__t=t15.get();
		}
		{
			Txt t16;
			g->AddString(ldateTimeStamp__t.get(),K_2D.get(),t16.get());
			Date t17;
			t17=ldate__d.get();
			Long t18;
			c.f.fLine=65;
			if (g->Call(ctx,(PCV[]){t18.cv(),t17.cv()},1,24)) goto _0;
			Txt t19;
			if (g->Call(ctx,(PCV[]){t19.cv(),t18.cv(),K00.cv()},2,10)) goto _0;
			g->AddString(t16.get(),t19.get(),ldateTimeStamp__t.get());
		}
		{
			Txt t21;
			g->AddString(ldateTimeStamp__t.get(),K_2D.get(),t21.get());
			Date t22;
			t22=ldate__d.get();
			Long t23;
			c.f.fLine=66;
			if (g->Call(ctx,(PCV[]){t23.cv(),t22.cv()},1,23)) goto _0;
			Txt t24;
			if (g->Call(ctx,(PCV[]){t24.cv(),t23.cv(),K00.cv()},2,10)) goto _0;
			g->AddString(t21.get(),t24.get(),ldateTimeStamp__t.get());
		}
		{
			Long t26;
			t26=inNbExplicitParam;
			if (2>t26.get()) goto _10;
		}
		{
			Bool t28;
			t28=ldate__d.get()==Date(0,0,0).get();
			if (!(t28.get())) goto _11;
		}
		ldateTimeStamp__t=K.get();
		goto _12;
_11:
		g->AddString(ldateTimeStamp__t.get(),KT.get(),ldateTimeStamp__t.get());
_12:
		{
			Time t30;
			c.f.fLine=75;
			t30=Parm<Time>(ctx,inParams,inNbParam,2).get();
			Txt t31;
			if (g->Call(ctx,(PCV[]){t31.cv(),t30.cv(),Long(1).cv()},2,10)) goto _0;
			ltime__t=t31.get();
		}
		if (ctx->doingAbort) goto _0;
		{
			Txt t32;
			c.f.fLine=76;
			if (g->Call(ctx,(PCV[]){t32.cv(),ltime__t.cv(),Long(1).cv(),Long(2).cv()},3,12)) goto _0;
			lhours__t=t32.get();
		}
		{
			Txt t33;
			c.f.fLine=77;
			if (g->Call(ctx,(PCV[]){t33.cv(),ltime__t.cv(),Long(4).cv(),Long(2).cv()},3,12)) goto _0;
			lminutes__t=t33.get();
		}
		{
			Txt t34;
			c.f.fLine=78;
			if (g->Call(ctx,(PCV[]){t34.cv(),ltime__t.cv(),Long(7).cv(),Long(2).cv()},3,12)) goto _0;
			lseconds__t=t34.get();
		}
		{
			Txt t35;
			g->AddString(ldateTimeStamp__t.get(),lhours__t.get(),t35.get());
			Txt t36;
			g->AddString(t35.get(),K_3A.get(),t36.get());
			Txt t37;
			g->AddString(t36.get(),lminutes__t.get(),t37.get());
			Txt t38;
			g->AddString(t37.get(),K_3A.get(),t38.get());
			g->AddString(t38.get(),lseconds__t.get(),ldateTimeStamp__t.get());
		}
_10:
_3:
		c.f.fLine=85;
		Res<Txt>(outResult)=ldateTimeStamp__t.get();
_0:
_1:
;
	}

}
