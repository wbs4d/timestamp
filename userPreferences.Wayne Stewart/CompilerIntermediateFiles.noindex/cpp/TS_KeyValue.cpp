extern Txt K01;
extern Txt K01234567;
extern Txt K0123456789;
extern Txt K_2F10;
extern Txt K_2F16;
extern Txt K_2F2;
extern Txt K_2F36;
extern Txt K_2F8;
extern Txt ki25VdPDQYr4;
extern Txt ku4EHCbBe$PE;
extern unsigned char D_proc_TS__KEYVALUE[];
void proc_TS__KEYVALUE( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_TS__KEYVALUE);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Txt lReturnedValue__t;
		Txt lBaseNumbering__t;
		new ( outResult) Txt();
		c.f.fLine=7;
		lBaseNumbering__t=Parm<Txt>(ctx,inParams,inNbParam,1).get();
		if (ctx->doingAbort) goto _0;
		{
			Bool t0;
			c.f.fLine=10;
			t0=g->CompareString(ctx,Parm<Txt>(ctx,inParams,inNbParam,1).get(),K_2F36.get())==0;
			if (!(t0.get())) goto _3;
		}
		if (ctx->doingAbort) goto _0;
		lReturnedValue__t=ku4EHCbBe$PE.get();
		goto _2;
_3:
		{
			Bool t1;
			c.f.fLine=13;
			t1=g->CompareString(ctx,Parm<Txt>(ctx,inParams,inNbParam,1).get(),K_2F16.get())==0;
			if (!(t1.get())) goto _4;
		}
		if (ctx->doingAbort) goto _0;
		lReturnedValue__t=ki25VdPDQYr4.get();
		goto _2;
_4:
		{
			Bool t2;
			c.f.fLine=16;
			t2=g->CompareString(ctx,Parm<Txt>(ctx,inParams,inNbParam,1).get(),K_2F10.get())==0;
			if (!(t2.get())) goto _5;
		}
		if (ctx->doingAbort) goto _0;
		lReturnedValue__t=K0123456789.get();
		goto _2;
_5:
		{
			Bool t3;
			c.f.fLine=19;
			t3=g->CompareString(ctx,Parm<Txt>(ctx,inParams,inNbParam,1).get(),K_2F8.get())==0;
			if (!(t3.get())) goto _6;
		}
		if (ctx->doingAbort) goto _0;
		lReturnedValue__t=K01234567.get();
		goto _2;
_6:
		{
			Bool t4;
			c.f.fLine=22;
			t4=g->CompareString(ctx,Parm<Txt>(ctx,inParams,inNbParam,1).get(),K_2F2.get())==0;
			if (!(t4.get())) goto _7;
		}
		if (ctx->doingAbort) goto _0;
		lReturnedValue__t=K01.get();
		goto _2;
_7:
		c.f.fLine=25;
		lReturnedValue__t=Parm<Txt>(ctx,inParams,inNbParam,1).get();
		if (ctx->doingAbort) goto _0;
_2:
		c.f.fLine=28;
		Res<Txt>(outResult)=lReturnedValue__t.get();
_0:
_1:
;
	}

}
