extern Txt K_24;
extern unsigned char D_proc_DEMO[];
void proc_DEMO( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_DEMO);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Long lProcessID__i;
		Txt lDesiredProcessName__t;
		Long lStackSize__i;
		Obj lForm__o;
		Long lWindowID__i;
		Txt lForm__t;
		lStackSize__i=0;
		{
			Txt t0;
			c.f.fLine=15;
			if (g->Call(ctx,(PCV[]){t0.cv()},0,684)) goto _0;
			g->Check(ctx);
			lForm__t=t0.get();
		}
		g->AddString(K_24.get(),lForm__t.get(),lDesiredProcessName__t.get());
		{
			Txt t2;
			c.f.fLine=18;
			if (g->Call(ctx,(PCV[]){t2.cv()},0,1392)) goto _0;
			g->Check(ctx);
			Bool t3;
			t3=g->CompareString(ctx,t2.get(),lDesiredProcessName__t.get())==0;
			if (!(t3.get())) goto _2;
		}
		{
			Obj t4;
			c.f.fLine=19;
			if (g->Call(ctx,(PCV[]){t4.cv()},0,1471)) goto _0;
			g->Check(ctx);
			lForm__o=t4.get();
		}
		{
			Long t5;
			c.f.fLine=22;
			if (g->Call(ctx,(PCV[]){t5.cv(),lForm__t.cv(),Long(8).cv(),Long(65536).cv(),Long(262144).cv(),Ref((optyp)3).cv()},5,675)) goto _0;
			g->Check(ctx);
			lWindowID__i=t5.get();
		}
		c.f.fLine=23;
		if (g->Call(ctx,(PCV[]){nullptr,lForm__t.cv(),lForm__o.cv()},2,40)) goto _0;
		g->Check(ctx);
		c.f.fLine=24;
		if (g->Call(ctx,(PCV[]){nullptr},0,154)) goto _0;
		g->Check(ctx);
		goto _3;
_2:
		{
			Txt t6;
			c.f.fLine=33;
			if (g->Call(ctx,(PCV[]){t6.cv()},0,684)) goto _0;
			g->Check(ctx);
			Long t7;
			if (g->Call(ctx,(PCV[]){t7.cv(),t6.cv(),lStackSize__i.cv(),lDesiredProcessName__t.cv(),Ref((optyp)3).cv()},4,317)) goto _0;
			lProcessID__i=t7.get();
		}
		c.f.fLine=35;
		if (g->Call(ctx,(PCV[]){nullptr,lProcessID__i.cv()},1,320)) goto _0;
		g->Check(ctx);
		c.f.fLine=36;
		if (g->Call(ctx,(PCV[]){nullptr,lProcessID__i.cv()},1,325)) goto _0;
		g->Check(ctx);
		c.f.fLine=37;
		if (g->Call(ctx,(PCV[]){nullptr,lProcessID__i.cv()},1,326)) goto _0;
		g->Check(ctx);
_3:
_0:
_1:
;
	}

}
