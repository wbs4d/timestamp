extern Txt K;
Asm4d_Proc proc_TS__ENCODE;
Asm4d_Proc proc_TS__KEYVALUE;
extern unsigned char D_proc_TS__ENCODE[];
void proc_TS__ENCODE( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_TS__ENCODE);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Txt lReturnedValue__t;
		Txt lKey__t;
		Long lBase__i;
		Long lEncodingLength__i;
		Long lTheNumberToEncode__i;
		Txt lBaseNumberingScheme__t;
		Long lNumber2__r;
		new ( outResult) Txt();
		if (!(Bool(0).get())) goto _2;
		{
			Long t0;
			t0=0;
			Long t2;
			t2=0;
			Txt t1;
			Txt t3;
			c.f.fLine=19;
			proc_TS__ENCODE(glob,ctx,0,3,(PCV[]){t0.cv(),t1.cv(),t2.cv()},t3.cv());
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
		}
_2:
		c.f.fLine=36;
		lEncodingLength__i=Parm<Long>(ctx,inParams,inNbParam,1).get();
		if (ctx->doingAbort) goto _0;
		c.f.fLine=37;
		lBaseNumberingScheme__t=Parm<Txt>(ctx,inParams,inNbParam,2).get();
		if (ctx->doingAbort) goto _0;
		c.f.fLine=38;
		lTheNumberToEncode__i=Parm<Long>(ctx,inParams,inNbParam,3).get();
		if (ctx->doingAbort) goto _0;
		{
			Txt t4;
			t4=lBaseNumberingScheme__t.get();
			Txt t5;
			c.f.fLine=40;
			proc_TS__KEYVALUE(glob,ctx,1,1,(PCV[]){t4.cv()},t5.cv());
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
			lKey__t=t5.get();
		}
		{
			Long t6;
			t6=lKey__t.get().fLength;
			lBase__i=t6.get();
		}
		if (0<=lTheNumberToEncode__i.get()) goto _3;
		{
			Num t8;
			t8=lTheNumberToEncode__i.get();
			Num t9;
			t9=0x1p+32+t8.get();
			lTheNumberToEncode__i=(sLONG)lrint(t9.get());
		}
_3:
		lReturnedValue__t=K.get();
_4:
		if (1>lTheNumberToEncode__i.get()) goto _5;
		{
			Long t12;
			t12=lTheNumberToEncode__i.get()/lBase__i.get();
			Long t13;
			t13=lBase__i.get()*t12.get();
			Long t14;
			t14=lTheNumberToEncode__i.get()-t13.get();
			Long t15;
			t15=t14.get()+1;
			Txt t16;
			c.f.fLine=51;
			g->GetStringChar(ctx,lKey__t.get(),(sLONG) t15.get(),t16.get());
			g->AddString(t16.get(),lReturnedValue__t.get(),lReturnedValue__t.get());
		}
		lTheNumberToEncode__i=lTheNumberToEncode__i.get()/lBase__i.get();
		goto _4;
_5:
		{
			Long t19;
			t19=lReturnedValue__t.get().fLength;
			if (t19.get()<=lEncodingLength__i.get()) goto _6;
		}
		lReturnedValue__t=K.get();
		goto _7;
_6:
		{
			Txt t21;
			c.f.fLine=58;
			g->GetStringChar(ctx,lKey__t.get(),(sLONG) 1,t21.get());
			Long t22;
			t22=lReturnedValue__t.get().fLength;
			Long t23;
			t23=lEncodingLength__i.get()-t22.get();
			Txt t24;
			g->MultiplyString(t21.get(),(sLONG)t23.get(),t24.get());
			g->AddString(t24.get(),lReturnedValue__t.get(),lReturnedValue__t.get());
		}
_7:
		c.f.fLine=61;
		Res<Txt>(outResult)=lReturnedValue__t.get();
_0:
_1:
;
	}

}
