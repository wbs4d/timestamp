extern int32_t vOK;
extern Txt K;
extern Txt KBuilds;
extern Txt KCFBundleName;
extern Txt KCancel;
extern Txt KComponents;
extern Txt KInfoPlist_2Ejson;
extern Txt KOK;
extern Txt KUTF_2D8;
extern Txt K_20;
extern Txt K_22_3B;
extern Txt K_2C_20;
extern Txt K_2C_20Mac_20to_20Basics;
extern Txt K_3A;
extern Txt K_A92004_2D;
extern Txt k4U5TfKSo$uA;
extern Txt k4_qKxzSuqlE;
extern Txt k92btG4OdU$s;
extern Txt k9gS5UdrV0QY;
extern Txt kCnQAYXvVWVM;
extern Txt kDXuhM3FS$_c;
extern Txt kGOAxKe4tGP0;
extern Txt kINGgez3jd8Q;
extern Txt kMaGss3dDlbI;
extern Txt km1Evpxhby54;
extern Txt knwtALt8HcvA;
extern Txt kwFfMvNyrN60;
Asm4d_Proc proc_FND__FCS__WRITEDOCUMENTATION;
Asm4d_Proc proc_TS__DATEANDTIMETOISO;
extern unsigned char D_proc_UTIL__BUILDCOMPONENT[];
void proc_UTIL__BUILDCOMPONENT( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_UTIL__BUILDCOMPONENT);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Txt larchive__t;
		Txt lyear__t;
		Txt lversionNumber__t;
		Txt lbuildFolderPath__t;
		Txt lbuiltComponent__t;
		Txt lLF;
		Txt ldate__t;
		Txt ljson__t;
		Obj linfo__o;
		Txt lnewName__t;
		Txt lstrings__t;
		Txt lbuildSettingsPath__t;
		Txt lenteredValue__t;
		Txt ltemplatePath__t;
		{
			Txt t0;
			c.f.fLine=22;
			if (g->Call(ctx,(PCV[]){t0.cv(),Long(6).cv()},1,485)) goto _0;
			g->Check(ctx);
			g->AddString(t0.get(),KInfoPlist_2Ejson.get(),ltemplatePath__t.get());
		}
		{
			Long t2;
			c.f.fLine=23;
			if (g->Call(ctx,(PCV[]){t2.cv(),ltemplatePath__t.cv()},1,476)) goto _0;
			g->Check(ctx);
			if (1!=t2.get()) goto _2;
		}
		{
			Txt t4;
			c.f.fLine=24;
			if (g->Call(ctx,(PCV[]){t4.cv(),Long(10).cv()},1,90)) goto _0;
			lLF=t4.get();
		}
		{
			Txt t5;
			c.f.fLine=25;
			if (g->Call(ctx,(PCV[]){t5.cv(),ltemplatePath__t.cv()},1,1236)) goto _0;
			g->Check(ctx);
			ljson__t=t5.get();
		}
		{
			Variant t6;
			c.f.fLine=26;
			if (g->Call(ctx,(PCV[]){t6.cv(),ljson__t.cv()},1,1218)) goto _0;
			g->Check(ctx);
			Obj t7;
			if (!g->GetValue(ctx,(PCV[]){t7.cv(),t6.cv(),nullptr})) goto _0;
			linfo__o=t7.get();
		}
		{
			Date t8;
			c.f.fLine=27;
			if (g->Call(ctx,(PCV[]){t8.cv()},0,33)) goto _0;
			Time t10;
			t10=Time(0).get();
			Date t11;
			t11=t8.get();
			Txt t12;
			proc_TS__DATEANDTIMETOISO(glob,ctx,1,2,(PCV[]){t11.cv(),t10.cv()},t12.cv());
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
			ldate__t=t12.get();
		}
		{
			Txt t13;
			c.f.fLine=28;
			if (g->Call(ctx,(PCV[]){t13.cv(),ldate__t.cv(),Long(1).cv(),Long(4).cv()},3,12)) goto _0;
			lyear__t=t13.get();
		}
		{
			Long t14;
			t14=inNbExplicitParam;
			if (1!=t14.get()) goto _3;
		}
		c.f.fLine=31;
		lversionNumber__t=Parm<Txt>(ctx,inParams,inNbParam,1).get();
		if (ctx->doingAbort) goto _0;
		Var<Long>(ctx,vOK)=1;
		Touch(ctx,vOK);
		goto _4;
_3:
		{
			Variant t16;
			c.f.fLine=34;
			if (g->GetMember(ctx,linfo__o.cv(),km1Evpxhby54.cv(),t16.cv())) goto _0;
			Txt t17;
			if (g->Call(ctx,(PCV[]){t17.cv(),kCnQAYXvVWVM.cv(),t16.cv(),KOK.cv(),KCancel.cv()},4,163)) goto _0;
			g->Check(ctx);
			lenteredValue__t=t17.get();
		}
		if (1!=Var<Long>(ctx,vOK).get()) goto _5;
		lversionNumber__t=lenteredValue__t.get();
_5:
_4:
		goto _6;
_2:
		Var<Long>(ctx,vOK)=0;
		Touch(ctx,vOK);
_6:
		if (1!=Var<Long>(ctx,vOK).get()) goto _7;
		c.f.fLine=48;
		if (g->SetMember(ctx,linfo__o.cv(),km1Evpxhby54.cv(),lversionNumber__t.cv())) goto _0;
		{
			Variant t20;
			c.f.fLine=49;
			if (g->GetMember(ctx,linfo__o.cv(),KCFBundleName.cv(),t20.cv())) goto _0;
			Variant t21;
			if (g->OperationOnAny(ctx,0,t20.cv(),K_20.cv(),t21.cv())) goto _0;
			Variant t22;
			if (g->OperationOnAny(ctx,0,t21.cv(),lversionNumber__t.cv(),t22.cv())) goto _0;
			Variant t23;
			if (g->OperationOnAny(ctx,0,t22.cv(),K_2C_20.cv(),t23.cv())) goto _0;
			Variant t24;
			if (g->OperationOnAny(ctx,0,t23.cv(),ldate__t.cv(),t24.cv())) goto _0;
			if (g->SetMember(ctx,linfo__o.cv(),kMaGss3dDlbI.cv(),t24.cv())) goto _0;
		}
		{
			Txt t25;
			g->AddString(K_A92004_2D.get(),lyear__t.get(),t25.get());
			Txt t26;
			g->AddString(t25.get(),K_2C_20Mac_20to_20Basics.get(),t26.get());
			c.f.fLine=50;
			if (g->SetMember(ctx,linfo__o.cv(),k92btG4OdU$s.cv(),t26.cv())) goto _0;
		}
		{
			Txt t27;
			c.f.fLine=52;
			if (g->Call(ctx,(PCV[]){t27.cv(),linfo__o.cv(),Ref((optyp)3).cv()},2,1217)) goto _0;
			g->Check(ctx);
			ljson__t=t27.get();
		}
		c.f.fLine=54;
		if (g->Call(ctx,(PCV[]){nullptr,ltemplatePath__t.cv()},1,159)) goto _0;
		g->Check(ctx);
		c.f.fLine=55;
		if (g->Call(ctx,(PCV[]){nullptr,ltemplatePath__t.cv(),ljson__t.cv(),KUTF_2D8.cv(),Long(4).cv()},4,1237)) goto _0;
		g->Check(ctx);
		{
			Txt t28;
			g->AddString(kwFfMvNyrN60.get(),lLF.get(),t28.get());
			g->AddString(t28.get(),lLF.get(),lstrings__t.get());
		}
		{
			Txt t30;
			g->AddString(lstrings__t.get(),k4U5TfKSo$uA.get(),t30.get());
			Variant t31;
			c.f.fLine=59;
			if (g->GetMember(ctx,linfo__o.cv(),KCFBundleName.cv(),t31.cv())) goto _0;
			Variant t32;
			if (g->OperationOnAny(ctx,0,t30.cv(),t31.cv(),t32.cv())) goto _0;
			Variant t33;
			if (g->OperationOnAny(ctx,0,t32.cv(),K_22_3B.cv(),t33.cv())) goto _0;
			Variant t34;
			if (g->OperationOnAny(ctx,0,t33.cv(),lLF.cv(),t34.cv())) goto _0;
			Txt t35;
			if (!g->GetValue(ctx,(PCV[]){t35.cv(),t34.cv(),nullptr})) goto _0;
			lstrings__t=t35.get();
		}
		{
			Txt t36;
			g->AddString(lstrings__t.get(),kINGgez3jd8Q.get(),t36.get());
			Variant t37;
			c.f.fLine=60;
			if (g->GetMember(ctx,linfo__o.cv(),km1Evpxhby54.cv(),t37.cv())) goto _0;
			Variant t38;
			if (g->OperationOnAny(ctx,0,t36.cv(),t37.cv(),t38.cv())) goto _0;
			Variant t39;
			if (g->OperationOnAny(ctx,0,t38.cv(),K_22_3B.cv(),t39.cv())) goto _0;
			Variant t40;
			if (g->OperationOnAny(ctx,0,t39.cv(),lLF.cv(),t40.cv())) goto _0;
			Txt t41;
			if (!g->GetValue(ctx,(PCV[]){t41.cv(),t40.cv(),nullptr})) goto _0;
			lstrings__t=t41.get();
		}
		{
			Txt t42;
			g->AddString(lstrings__t.get(),knwtALt8HcvA.get(),t42.get());
			Variant t43;
			c.f.fLine=61;
			if (g->GetMember(ctx,linfo__o.cv(),kMaGss3dDlbI.cv(),t43.cv())) goto _0;
			Variant t44;
			if (g->OperationOnAny(ctx,0,t42.cv(),t43.cv(),t44.cv())) goto _0;
			Variant t45;
			if (g->OperationOnAny(ctx,0,t44.cv(),K_22_3B.cv(),t45.cv())) goto _0;
			Variant t46;
			if (g->OperationOnAny(ctx,0,t45.cv(),lLF.cv(),t46.cv())) goto _0;
			Txt t47;
			if (!g->GetValue(ctx,(PCV[]){t47.cv(),t46.cv(),nullptr})) goto _0;
			lstrings__t=t47.get();
		}
		{
			Txt t48;
			g->AddString(lstrings__t.get(),k4_qKxzSuqlE.get(),t48.get());
			Variant t49;
			c.f.fLine=62;
			if (g->GetMember(ctx,linfo__o.cv(),k92btG4OdU$s.cv(),t49.cv())) goto _0;
			Variant t50;
			if (g->OperationOnAny(ctx,0,t48.cv(),t49.cv(),t50.cv())) goto _0;
			Variant t51;
			if (g->OperationOnAny(ctx,0,t50.cv(),K_22_3B.cv(),t51.cv())) goto _0;
			Txt t52;
			if (!g->GetValue(ctx,(PCV[]){t52.cv(),t51.cv(),nullptr})) goto _0;
			lstrings__t=t52.get();
		}
		{
			Txt t53;
			c.f.fLine=64;
			if (g->Call(ctx,(PCV[]){t53.cv(),Long(6).cv()},1,485)) goto _0;
			g->Check(ctx);
			g->AddString(t53.get(),k9gS5UdrV0QY.get(),ltemplatePath__t.get());
		}
		c.f.fLine=66;
		if (g->Call(ctx,(PCV[]){nullptr,ltemplatePath__t.cv()},1,159)) goto _0;
		g->Check(ctx);
		c.f.fLine=67;
		if (g->Call(ctx,(PCV[]){nullptr,ltemplatePath__t.cv(),lstrings__t.cv(),KUTF_2D8.cv(),Long(4).cv()},4,1237)) goto _0;
		g->Check(ctx);
_7:
		if (1!=Var<Long>(ctx,vOK).get()) goto _8;
		{
			Txt t56;
			c.f.fLine=72;
			if (g->Call(ctx,(PCV[]){t56.cv(),Long(20).cv()},1,1418)) goto _0;
			g->Check(ctx);
			lbuildSettingsPath__t=t56.get();
		}
		{
			Txt t57;
			c.f.fLine=73;
			if (g->Call(ctx,(PCV[]){t57.cv(),Long(4).cv()},1,485)) goto _0;
			g->Check(ctx);
			Txt t58;
			g->AddString(t57.get(),KBuilds.get(),t58.get());
			Txt t59;
			g->AddString(t58.get(),K_3A.get(),t59.get());
			Txt t60;
			g->AddString(t59.get(),KComponents.get(),t60.get());
			g->AddString(t60.get(),K_3A.get(),lbuildFolderPath__t.get());
		}
		{
			Bool t62;
			t62=Bool(1).get();
			Bool t63;
			t63=Bool(1).get();
			Txt t64;
			t64=K.get();
			c.f.fLine=75;
			proc_FND__FCS__WRITEDOCUMENTATION(glob,ctx,3,3,(PCV[]){t64.cv(),t63.cv(),t62.cv()},nullptr);
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
		}
		c.f.fLine=76;
		if (g->Call(ctx,(PCV[]){nullptr,lbuildSettingsPath__t.cv()},1,871)) goto _0;
		g->Check(ctx);
		{
			Bool t65;
			t65=Bool(0).get();
			Bool t66;
			t66=Bool(1).get();
			Txt t67;
			t67=K.get();
			c.f.fLine=77;
			proc_FND__FCS__WRITEDOCUMENTATION(glob,ctx,3,3,(PCV[]){t67.cv(),t66.cv(),t65.cv()},nullptr);
			if (ctx->checkPendingErrors) g->CheckErr(ctx,0);
			if (ctx->doingAbort) goto _0;
		}
		if (1!=Var<Long>(ctx,vOK).get()) goto _9;
		c.f.fLine=80;
		if (g->Call(ctx,(PCV[]){nullptr,lbuildFolderPath__t.cv()},1,922)) goto _0;
		g->Check(ctx);
		c.f.fLine=81;
		if (g->Call(ctx,(PCV[]){nullptr,kDXuhM3FS$_c.cv()},1,41)) goto _0;
		g->Check(ctx);
		goto _10;
_9:
		c.f.fLine=83;
		if (g->Call(ctx,(PCV[]){nullptr,kGOAxKe4tGP0.cv()},1,41)) goto _0;
		g->Check(ctx);
_10:
_8:
_0:
_1:
;
	}

}
