#include "$asm4d.h"
#include "cp4drt_shared.h"
#include "legacy_language_types.h"
Txt K;
Txt K0;
Txt K00;
Txt K0000;
Txt K01;
Txt K01234567;
Txt K0123456789;
Txt K1_2F5;
Txt K20_2E1_20_5B;
Txt K2_2F5;
Txt K3_2F5;
Txt K4D_20;
Txt K4_2F5;
Txt K5_2F5;
Txt KAttributes_3A_20;
Txt KBuilds;
Txt KCFBundleName;
Txt KCallFormBased;
Txt KCancel;
Txt KCol__0;
Txt KCol__01;
Txt KCol__02;
Txt KCol__03;
Txt KCol__04;
Txt KCol__05;
Txt KComponents;
Txt KCourier;
Txt KCreated_20by;
Txt KDemo;
Txt KDocumentation;
Txt KDuration_3A_20;
Txt KGet_20Values;
Txt KInfoPlist_2Ejson;
Txt KInvisible_2C_20;
Txt KLicence;
Txt KList_20Box;
Txt KMethods;
Txt KOK;
Txt KParameters_3A;
Txt KPreemptive;
Txt KReturns_3A;
Txt KSQL_2C_20;
Txt KServer_2C_20;
Txt KShared_2C_20;
Txt KSignal;
Txt KSoap_2C_20;
Txt KT;
Txt KTS;
Txt KTS__Get__PE;
Txt KTS__getXValues;
Txt KUTF_2D8;
Txt KWSDL_2C_20;
Txt KWeb_2C_20;
Txt KX;
Txt KXResults;
Txt K_0D;
Txt K_0D_0D;
Txt K_20;
Txt K_20R;
Txt K_20_20_2F_2F_20;
Txt K_20_28;
Txt K_20_3A_20;
Txt K_20ms_2E;
Txt K_22_3B;
Txt K_23_23_20;
Txt K_23_23_23_2C_23_230;
Txt K_24;
Txt K_29;
Txt K_2A_2AReturns_2A_2A;
Txt K_2C_20;
Txt K_2C_20Mac_20to_20Basics;
Txt K_2D;
Txt K_2D_2D_3E;
Txt K_2D_3E;
Txt K_2E;
Txt K_2F;
Txt K_2F10;
Txt K_2F16;
Txt K_2F2;
Txt K_2F36;
Txt K_2F5;
Txt K_2F8;
Txt K_2F_2F;
Txt K_2F_2F_20;
Txt K_2F_2F_20Parameters_3A;
Txt K_2F_2F_20Returns_3A;
Txt K_2F_2F_20_20_20_20_24;
Txt K_2F_2F_20_20_20_24;
Txt K_2F_2F_20_20_24;
Txt K_2F_2F_20_24;
Txt K_3A;
Txt K_3C_21_2D_2D;
Txt K_3Cbr_3E;
Txt K_40;
Txt K_40Created_20by_40;
Txt K_40Parameters_3A_40;
Txt K_40Returns_40;
Txt K_5D;
Txt K_7C;
Txt K_A92004_2D;
Txt Kbracket;
Txt Kcapable;
Txt Kdt;
Txt Kincapable;
Txt Kindifferent;
Txt Kinvisible;
Txt KlastCall;
Txt KnumberOfCalls;
Txt Kpreemptive;
Txt KpublishedSoap;
Txt KpublishedSql;
Txt KpublishedWeb;
Txt KpublishedWsdl;
Txt KreturnArray;
Txt Kshared;
Txt Kstart;
Txt KtheStamper;
Txt KtimeStamp;
Txt Ktrigger;
Txt Kwait;
Txt KwindowID;
Txt k$tdAxHfqfk0;
Txt k2ucr6KMltt4;
Txt k4U5TfKSo$uA;
Txt k4_qKxzSuqlE;
Txt k92btG4OdU$s;
Txt k9gS5UdrV0QY;
Txt kCnQAYXvVWVM;
Txt kD2KcHmnsXaM;
Txt kDXuhM3FS$_c;
Txt kFztatg2AeEk;
Txt kGOAxKe4tGP0;
Txt kHSK4e$VtNdY;
Txt kHYo54JMnmMc;
Txt kINGgez3jd8Q;
Txt kKqLvphAEyfI;
Txt kMaGss3dDlbI;
Txt kPk$4ELz88bk;
Txt kXztOPunYs9g;
Txt kasCU9KRLhsc;
Txt kbE413fCVMjA;
Txt kc$4IxpZB99g;
Txt kdAQ7fWUqce8;
Txt keYqLvjgs1KI;
Txt kemoKRlUtXYg;
Txt khAhd0qeKOrE;
Txt khj46qayLLyM;
Txt ki25VdPDQYr4;
Txt kin89jWOO9_0;
Txt km1Evpxhby54;
Txt knwtALt8HcvA;
Txt koM_Vm4W5mf0;
Txt kq2BFNIvkFYU;
Txt ku4EHCbBe$PE;
Txt kvUWSXMd0W2g;
Txt kwFfMvNyrN60;
Txt kwWsRQdd4SCg;
Txt kx4DgsGRJUpg;
Txt kyUKfCm0Vmhg;

struct Txt_info { Txt *t; const char16_t *s; size_t n;};
static const Txt_info constants[]=
{
	{&K,nullptr,0},
	{&K0,u"0",1},
	{&K00,u"00",2},
	{&K0000,u"0000",4},
	{&K01,u"01",2},
	{&K01234567,u"01234567",8},
	{&K0123456789,u"0123456789",10},
	{&K1_2F5,u"1/5",3},
	{&K20_2E1_20_5B,u"20.1 [",6},
	{&K2_2F5,u"2/5",3},
	{&K3_2F5,u"3/5",3},
	{&K4D_20,u"4D ",3},
	{&K4_2F5,u"4/5",3},
	{&K5_2F5,u"5/5",3},
	{&KAttributes_3A_20,u"Attributes: ",12},
	{&KBuilds,u"Builds",6},
	{&KCFBundleName,u"CFBundleName",12},
	{&KCallFormBased,u"CallFormBased",13},
	{&KCancel,u"Cancel",6},
	{&KCol__0,u"Col_0",5},
	{&KCol__01,u"Col_01",6},
	{&KCol__02,u"Col_02",6},
	{&KCol__03,u"Col_03",6},
	{&KCol__04,u"Col_04",6},
	{&KCol__05,u"Col_05",6},
	{&KComponents,u"Components",10},
	{&KCourier,u"Courier",7},
	{&KCreated_20by,u"Created by",10},
	{&KDemo,u"Demo",4},
	{&KDocumentation,u"Documentation",13},
	{&KDuration_3A_20,u"Duration: ",10},
	{&KGet_20Values,u"Get Values",10},
	{&KInfoPlist_2Ejson,u"InfoPlist.json",14},
	{&KInvisible_2C_20,u"Invisible, ",11},
	{&KLicence,u"Licence",7},
	{&KList_20Box,u"List Box",8},
	{&KMethods,u"Methods",7},
	{&KOK,u"OK",2},
	{&KParameters_3A,u"Parameters:",11},
	{&KPreemptive,u"Preemptive",10},
	{&KReturns_3A,u"Returns:",8},
	{&KSQL_2C_20,u"SQL, ",5},
	{&KServer_2C_20,u"Server, ",8},
	{&KShared_2C_20,u"Shared, ",8},
	{&KSignal,u"Signal",6},
	{&KSoap_2C_20,u"Soap, ",6},
	{&KT,u"T",1},
	{&KTS,u"TS",2},
	{&KTS__Get__PE,u"TS_Get_PE",9},
	{&KTS__getXValues,u"TS_getXValues",13},
	{&KUTF_2D8,u"UTF-8",5},
	{&KWSDL_2C_20,u"WSDL, ",6},
	{&KWeb_2C_20,u"Web, ",5},
	{&KX,u"X",1},
	{&KXResults,u"XResults",8},
	{&K_0D,u"\xd",1},
	{&K_0D_0D,u"\xd\xd",2},
	{&K_20,u" ",1},
	{&K_20R,u" R",2},
	{&K_20_20_2F_2F_20,u"  // ",5},
	{&K_20_28,u" (",2},
	{&K_20_3A_20,u" : ",3},
	{&K_20ms_2E,u" ms.",4},
	{&K_22_3B,u"\x22;",2},
	{&K_23_23_20,u"## ",3},
	{&K_23_23_23_2C_23_230,u"###,##0",7},
	{&K_24,u"$",1},
	{&K_29,u")",1},
	{&K_2A_2AReturns_2A_2A,u"**Returns**",11},
	{&K_2C_20,u", ",2},
	{&K_2C_20Mac_20to_20Basics,u", Mac to Basics",15},
	{&K_2D,u"-",1},
	{&K_2D_2D_3E,u"-->",3},
	{&K_2D_3E,u"->",2},
	{&K_2E,u".",1},
	{&K_2F,u"/",1},
	{&K_2F10,u"/10",3},
	{&K_2F16,u"/16",3},
	{&K_2F2,u"/2",2},
	{&K_2F36,u"/36",3},
	{&K_2F5,u"/5",2},
	{&K_2F8,u"/8",2},
	{&K_2F_2F,u"//",2},
	{&K_2F_2F_20,u"// ",3},
	{&K_2F_2F_20Parameters_3A,u"// Parameters:",14},
	{&K_2F_2F_20Returns_3A,u"// Returns:",11},
	{&K_2F_2F_20_20_20_20_24,u"//    $",7},
	{&K_2F_2F_20_20_20_24,u"//   $",6},
	{&K_2F_2F_20_20_24,u"//  $",5},
	{&K_2F_2F_20_24,u"// $",4},
	{&K_3A,u":",1},
	{&K_3C_21_2D_2D,u"<!--",4},
	{&K_3Cbr_3E,u"<br>",4},
	{&K_40,u"@",1},
	{&K_40Created_20by_40,u"@Created by@",12},
	{&K_40Parameters_3A_40,u"@Parameters:@",13},
	{&K_40Returns_40,u"@Returns@",9},
	{&K_5D,u"]",1},
	{&K_7C,u"|",1},
	{&K_A92004_2D,u"\xa9\x32\x30\x30\x34-",6},
	{&Kbracket,u"bracket",7},
	{&Kcapable,u"capable",7},
	{&Kdt,u"dt",2},
	{&Kincapable,u"incapable",9},
	{&Kindifferent,u"indifferent",11},
	{&Kinvisible,u"invisible",9},
	{&KlastCall,u"lastCall",8},
	{&KnumberOfCalls,u"numberOfCalls",13},
	{&Kpreemptive,u"preemptive",10},
	{&KpublishedSoap,u"publishedSoap",13},
	{&KpublishedSql,u"publishedSql",12},
	{&KpublishedWeb,u"publishedWeb",12},
	{&KpublishedWsdl,u"publishedWsdl",13},
	{&KreturnArray,u"returnArray",11},
	{&Kshared,u"shared",6},
	{&Kstart,u"start",5},
	{&KtheStamper,u"theStamper",10},
	{&KtimeStamp,u"timeStamp",9},
	{&Ktrigger,u"trigger",7},
	{&Kwait,u"wait",4},
	{&KwindowID,u"windowID",8},
	{&k$tdAxHfqfk0,u"Preemptive indifferent, ",24},
	{&k2ucr6KMltt4,u"Parameters : Type : Description",31},
	{&k4U5TfKSo$uA,u"CFBundleName = \x22",16},
	{&k4_qKxzSuqlE,u"NSHumanReadableCopyright = \x22",28},
	{&k92btG4OdU$s,u"NSHumanReadableCopyright",24},
	{&k9gS5UdrV0QY,u"InfoPlist.strings",17},
	{&kCnQAYXvVWVM,u"Please enter the new version number:",36},
	{&kD2KcHmnsXaM,u"Co-op or Preemptive",19},
	{&kDXuhM3FS$_c,u"Successful Build",16},
	{&kFztatg2AeEk,u"  // Project Method: ",21},
	{&kGOAxKe4tGP0,u"Component Build Failed",22},
	{&kHSK4e$VtNdY,u"// ----------------------------------------------------\xd",56},
	{&kHYo54JMnmMc,u"Licence Acknowledgement",23},
	{&kINGgez3jd8Q,u"CFBundleShortVersionString = \x22",30},
	{&kKqLvphAEyfI,u"\xd// Access: Private\xd",20},
	{&kMaGss3dDlbI,u"CFBundleGetInfoString",21},
	{&kPk$4ELz88bk,u"  // ----------------------------------------------------\xd",58},
	{&kXztOPunYs9g,u"----------|----|-----------",27},
	{&kasCU9KRLhsc,u"$WriteDocumentation",19},
	{&kbE413fCVMjA,u"Parameters: None",16},
	{&kc$4IxpZB99g,u"Returns: Nothing",16},
	{&kdAQ7fWUqce8,u"\xd// Access: Shared\xd",19},
	{&keYqLvjgs1KI,u"Preemptive incapable, ",22},
	{&kemoKRlUtXYg,u"Parameters|Type|Description",27},
	{&khAhd0qeKOrE,u"slartibartfast123456-Ford-Prefect",33},
	{&khj46qayLLyM,u"Preemptive capable, ",20},
	{&ki25VdPDQYr4,u"0123456789ABCDEF",16},
	{&kin89jWOO9_0,u"Number of calculations: ",24},
	{&km1Evpxhby54,u"CFBundleShortVersionString",26},
	{&knwtALt8HcvA,u"CFBundleGetInfoString = \x22",25},
	{&koM_Vm4W5mf0,u"Generate X Results",18},
	{&kq2BFNIvkFYU,u"comment added and reserved by 4D.\xd",34},
	{&ku4EHCbBe$PE,u"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ",36},
	{&kvUWSXMd0W2g,u"Project Method: ",16},
	{&kwFfMvNyrN60,u"/* Localized versions of Info.plist keys */",43},
	{&kwWsRQdd4SCg,u"$TimeStampSemaphore",19},
	{&kx4DgsGRJUpg,u"executedOnServer",16},
	{&kyUKfCm0Vmhg,u"// Project Method: ",19},
	{nullptr,nullptr,0}
};

void InitConstants()
{
	for( const Txt_info *i = constants ; i->t != nullptr; ++i)
		g->AssignUniChars(i->t->cv(),i->s,i->n);
}

void DeInitConstants()
{
	for( const Txt_info *i = constants ; i->t != nullptr; ++i)
		i->t->setNull();
}
