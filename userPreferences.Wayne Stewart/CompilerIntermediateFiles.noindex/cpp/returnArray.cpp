extern Txt K;
extern Txt KCol__0;
extern Txt KCol__01;
extern Txt KCol__02;
extern Txt KCol__03;
extern Txt KCol__04;
extern Txt KCol__05;
extern Txt KDuration_3A_20;
extern Txt KList_20Box;
extern Txt K_20ms_2E;
extern Txt K_2F5;
extern Txt Kstart;
extern unsigned char D_proc_RETURNARRAY[];
void proc_RETURNARRAY( Asm4d_globals *glob, tProcessGlobals *ctx, int32_t inNbExplicitParam, int32_t inNbParam, PCV inParams[], CV *outResult)
{
	CallChain c(ctx,D_proc_RETURNARRAY);
	if (!ctx->doingAbort && c.f.fLine==0) {
		Value_array_text ltemp__at;
		Ptr llistBox__ptr;
		Ptr lthisCol__ptr;
		Txt lbracket__t;
		Ptr lCol__03__ptr;
		Long lMaxSize;
		Ptr lCol__04__ptr;
		Ptr lCol__05__ptr;
		Ptr lCol__01__ptr;
		Ptr lCol__02__ptr;
		Long lSize;
		Ptr lbutton__ptr;
		{
			Ref t0;
			t0.setLocalRef(ctx,ltemp__at.cv());
			c.f.fLine=9;
			if (g->Call(ctx,(PCV[]){nullptr,t0.cv(),Long(0).cv()},2,222)) goto _0;
		}
		{
			Ref t1;
			t1.setLocalRef(ctx,ltemp__at.cv());
			Ref t2;
			t2.setLocalRef(ctx,Parm<Blb>(ctx,inParams,inNbParam,2).cv());
			c.f.fLine=11;
			if (g->Call(ctx,(PCV[]){nullptr,t2.cv(),t1.cv()},2,533)) goto _0;
			g->Check(ctx);
		}
		if (ctx->doingAbort) goto _0;
		c.f.fLine=13;
		lbracket__t=Parm<Txt>(ctx,inParams,inNbParam,1).get();
		if (ctx->doingAbort) goto _0;
		{
			Txt t3;
			c.f.fLine=14;
			if (g->Call(ctx,(PCV[]){t3.cv(),lbracket__t.cv(),K_2F5.cv(),K.cv()},3,233)) goto _0;
			lbracket__t=t3.get();
		}
		g->AddString(KCol__0.get(),lbracket__t.get(),lbracket__t.get());
		{
			Ptr t5;
			c.f.fLine=17;
			if (g->Call(ctx,(PCV[]){t5.cv(),Long(3).cv(),lbracket__t.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lthisCol__ptr=t5.get();
		}
		{
			Ref t6;
			c.f.fLine=19;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t6.cv(),lthisCol__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			Ref t7;
			t7.setLocalRef(ctx,ltemp__at.cv());
			if (g->Call(ctx,(PCV[]){nullptr,t7.cv(),t6.cv()},2,226)) goto _0;
		}
		{
			Ptr t8;
			c.f.fLine=21;
			if (g->Call(ctx,(PCV[]){t8.cv(),Long(3).cv(),KCol__01.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__01__ptr=t8.get();
		}
		{
			Ptr t9;
			c.f.fLine=23;
			if (g->Call(ctx,(PCV[]){t9.cv(),Long(3).cv(),KCol__02.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__02__ptr=t9.get();
		}
		{
			Ptr t10;
			c.f.fLine=25;
			if (g->Call(ctx,(PCV[]){t10.cv(),Long(3).cv(),KCol__03.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__03__ptr=t10.get();
		}
		{
			Ptr t11;
			c.f.fLine=27;
			if (g->Call(ctx,(PCV[]){t11.cv(),Long(3).cv(),KCol__04.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__04__ptr=t11.get();
		}
		{
			Ptr t12;
			c.f.fLine=29;
			if (g->Call(ctx,(PCV[]){t12.cv(),Long(3).cv(),KCol__05.cv()},2,1124)) goto _0;
			g->Check(ctx);
			lCol__05__ptr=t12.get();
		}
		{
			Ref t13;
			c.f.fLine=31;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t13.cv(),lCol__01__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			Long t14;
			if (g->Call(ctx,(PCV[]){t14.cv(),t13.cv()},1,274)) goto _0;
			Bool t15;
			t15=0<t14.get();
			Ref t16;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t16.cv(),lCol__02__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			Long t17;
			if (g->Call(ctx,(PCV[]){t17.cv(),t16.cv()},1,274)) goto _0;
			Bool t18;
			t18=0<t17.get();
			Bool t19;
			t19=t15.get()&&t18.get();
			Ref t20;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t20.cv(),lCol__03__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			Long t21;
			if (g->Call(ctx,(PCV[]){t21.cv(),t20.cv()},1,274)) goto _0;
			Bool t22;
			t22=0<t21.get();
			Bool t23;
			t23=t19.get()&&t22.get();
			Ref t24;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t24.cv(),lCol__04__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			Long t25;
			if (g->Call(ctx,(PCV[]){t25.cv(),t24.cv()},1,274)) goto _0;
			Bool t26;
			t26=0<t25.get();
			Bool t27;
			t27=t23.get()&&t26.get();
			Ref t28;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t28.cv(),lCol__05__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			Long t29;
			if (g->Call(ctx,(PCV[]){t29.cv(),t28.cv()},1,274)) goto _0;
			Bool t30;
			t30=0<t29.get();
			if (!( t27.get()&&t30.get())) goto _2;
		}
		{
			Ptr t32;
			c.f.fLine=33;
			if (g->Call(ctx,(PCV[]){t32.cv(),Long(3).cv(),KList_20Box.cv()},2,1124)) goto _0;
			g->Check(ctx);
			llistBox__ptr=t32.get();
		}
		{
			Ref t33;
			c.f.fLine=34;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t33.cv(),lthisCol__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			Long t34;
			if (g->Call(ctx,(PCV[]){t34.cv(),t33.cv()},1,274)) goto _0;
			Ref t35;
			if (!g->CastPointerToRef(ctx,4,(PCV[]){t35.cv(),llistBox__ptr.cv(),(PCV)-1,nullptr})) goto _0;
			if (g->Call(ctx,(PCV[]){nullptr,t35.cv(),t34.cv()},2,223)) goto _0;
		}
		{
			Long t36;
			c.f.fLine=36;
			if (g->Call(ctx,(PCV[]){t36.cv()},0,459)) goto _0;
			Obj t37;
			if (g->Call(ctx,(PCV[]){t37.cv()},0,1466)) goto _0;
			Variant t38;
			if (g->GetMember(ctx,t37.cv(),Kstart.cv(),t38.cv())) goto _0;
			Num t39;
			if (g->Call(ctx,(PCV[]){t39.cv(),t38.cv()},1,11)) goto _0;
			Num t40;
			t40=t36.get();
			Num t41;
			t41=t40.get()-t39.get();
			Txt t42;
			if (g->Call(ctx,(PCV[]){t42.cv(),t41.cv()},1,10)) goto _0;
			Txt t43;
			g->AddString(KDuration_3A_20.get(),t42.get(),t43.get());
			Txt t44;
			g->AddString(t43.get(),K_20ms_2E.get(),t44.get());
			if (g->Call(ctx,(PCV[]){nullptr,t44.cv()},1,41)) goto _0;
			g->Check(ctx);
		}
_2:
_0:
_1:
;
	}

}
