//%attributes = {}
// ----------------------------------------------------
// Project Method: GNT_DateTime
//    Method Type: Protected
// Genro Routine
// ----------------------------------------------------

C_TEXT:C284($0;$1;$2;$3;$ReturnedValue)
C_DATE:C307($4;$5)
C_TEXT:C284($ReturnedValue;$CurrentMethodName)
$CurrentMethodName:="GNT_DateTime"
$ReturnedValue:=""
C_BOOLEAN:C305(<>GNT_TOOLS_BS_Started)
If (<>GNT_TOOLS_BS_Started=False:C215)
	GNT_GenRoTools("InternalRegister")
End if 
Case of 
		
	: (False:C215)
		C_LONGINT:C283($j;$n;$h;$k;$p)
		
	: ($1="TimeStampGet")
		C_LONGINT:C283(<>GNT_STR_LS_Count;$Base)
		C_LONGINT:C283(<>GNT_STR_LS_LastSec)
		C_LONGINT:C283($j;$n;$h;$k;$p;$Range;$Base;$Station)
		C_LONGINT:C283($Decrement)
		C_TEXT:C284($Semaphore)
		
		$Semaphore:="$TimeStampSemaphore"
		C_LONGINT:C283($Days;$Seconds;$Num;$j;$Counter)
		$Station:=0
		$Base:=1
		$Range:=800000
		If (Count parameters:C259>=2)
			If ($2#"")
				$Base:=Num:C11(GNT_Strings("GetFrom";"/";$2))
				$Station:=Num:C11(GNT_Strings("GetUntil";"/";$2))-1
				$Range:=Int:C8((800000/$Base)+0.999)
			End if 
		End if 
		
		If (Count parameters:C259>=3)
			$Decrement:=Num:C11($3)
		Else 
			$Decrement:=0
		End if 
		
		If (Not:C34(Semaphore:C143($Semaphore;60)))
			Repeat 
				$Days:=Current date:C33(*)-!1990-01-01!
				$Seconds:=(Current time:C178(*)-$Decrement)
				
				//BEGIN added to cater for day differences
				If ($Decrement#0)
					$Days:=$Days+Int:C8($Seconds/86400)
					$Seconds:=$Seconds-(Int:C8($Seconds/86400)*86400)
				End if 
				//END  added to cater for day differences
				
				
				If ($Seconds#<>GNT_STR_LS_LastSec)
					<>GNT_STR_LS_LastSec:=$Seconds
					<>GNT_STR_LS_Count:=1
					If ($Base=1)
						<>GNT_STR_LS_Count:=<>GNT_STR_LS_Count+(Random:C100%10000)
					End if 
				Else 
					<>GNT_STR_LS_Count:=<>GNT_STR_LS_Count+1
				End if 
			Until (<>GNT_STR_LS_Count<=$Range)
			$Counter:=<>GNT_STR_LS_Count+($Station*$Range)
			CLEAR SEMAPHORE:C144($Semaphore)
		End if 
		$ReturnedValue:=GNT_Strings("Encode:3";"/36";String:C10($Days))+GNT_Strings("Encode:4";"/36";String:C10(($Seconds*18)+($Counter\46000)))+GNT_Strings("Encode:3";"/36";String:C10($Counter%46000))
		
		
		
		
	: ($1="TimeStampDecode")
		C_DATE:C307($Date)
		C_TEXT:C284($Format)
		If (Count parameters:C259=3)
			$Format:=$3
		Else 
			$Format:=""
		End if 
		
		$j:=Int:C8(Num:C11(GNT_Strings("Decode";"/36";Substring:C12($2;4;4)))/18)
		$k:=Num:C11(GNT_Strings("Decode";"/36";Substring:C12($2;1;3)))
		If ($Format="YMDT")
			$Date:=!1990-01-01!+$k
			$ReturnedValue:=String:C10(Year of:C25($Date);"0000")+"/"+String:C10(Month of:C24($Date);"00")+"/"+String:C10(Day of:C23($Date);"00")+" - "+Time string:C180($j)
		Else 
			$ReturnedValue:=String:C10(!1990-01-01!+$k)+" - "+Time string:C180($j)
		End if 
		
	: ($1="DateFromID")
		$ReturnedValue:=String:C10(!1990-01-01!+Num:C11(GNT_Strings("Decode";"/36";Substring:C12($2;1;3))))
		
	: ($1="TimeFromID")
		$ReturnedValue:=Time string:C180(Int:C8(Num:C11(GNT_Strings("Decode";"/36";Substring:C12($2;4;4)))/18))
		
	: ($1="IDFromDate")
		C_LONGINT:C283(<>GNT_STR_LS_Count;$Base)
		C_LONGINT:C283(<>GNT_STR_LS_LastSec)
		C_LONGINT:C283($j;$n;$h;$k;$p;$Range;$Base;$Station)
		C_TEXT:C284($Semaphore)
		$Semaphore:="$TimeStampSemaphore"
		C_LONGINT:C283($Days;$Seconds;$Num;$j;$Counter)
		$Station:=0
		$Base:=1
		$Range:=800000
		$Days:=Date:C102($2)-!1990-01-01!
		$Seconds:=0
		$Counter:=0
		$ReturnedValue:=GNT_Strings("Encode:3";"/36";String:C10($Days))+GNT_Strings("Encode:4";"/36";String:C10(($Seconds*18)+($Counter\46000)))+GNT_Strings("Encode:3";"/36";String:C10($Counter%46000))
		
		
	: ($1="GetIntDate&Time")
		C_DATE:C307($DateNow)
		If (Count parameters:C259>1)
			$DateNow:=Current date:C33
		Else 
			$DateNow:=Date:C102($2)
		End if 
		$ReturnedValue:=String:C10(Year of:C25($DateNow);"0000")+"-"+String:C10(Month of:C24($DateNow);"00")+"-"+String:C10(Day of:C23($DateNow);"00")+" ["+Substring:C12(Time string:C180(Current time:C178);1;5)+"]"
		
	: ($1="PlistDate")
		$DateNow:=Date:C102($2)
		$ReturnedValue:=String:C10(Year of:C25($DateNow);"0000")+"-"+String:C10(Month of:C24($DateNow);"00")+"-"+String:C10(Day of:C23($DateNow);"00")+"T23:00:00Z"
	: ($1="IsoDate")
		$DateNow:=Date:C102($2)
		$ReturnedValue:=String:C10(Year of:C25($DateNow);"0000")+"-"+String:C10(Month of:C24($DateNow);"00")+"-"+String:C10(Day of:C23($DateNow);"00")
		
	Else 
		GNT_GenRoTools("UndefinedEntryPoint";$1;$CurrentMethodName)
End case 
$0:=$ReturnedValue