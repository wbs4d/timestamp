//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: util_Get4DVersion --> Text

// Returns a neatly formatted string describing the version of 4D
// Eg. 4D 20 R3 (100359)

// Access: Shared

// Returns: 
//   $0 : Text : Description

// Created by Wayne Stewart (2024-05-18)
//     waynestewart@mac.com
// ----------------------------------------------------

#DECLARE()->$buildVersion : Text

var $build : Integer
var $version; $Txt_release : Text

$version:=Application version:C493($build)


$buildVersion:=$version[[1]]+$version[[2]]  //version number, e.g. 20
$Txt_release:=$version[[3]]  //Rx

$buildVersion:="4D "+$buildVersion

If ($Txt_release="0")  //4D v20.x
	$Txt_release:=$version[[4]]  //.x
	$buildVersion:=$buildVersion+Choose:C955($Txt_release#"0"; "."+$Txt_release; "")
	
Else   //4D v20 Rx
	$buildVersion:=$buildVersion+" R"+$Txt_release
End if 

$buildVersion+=" ("+String:C10($build)+")"


