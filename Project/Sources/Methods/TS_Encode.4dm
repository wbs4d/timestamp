//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: TS_Encode (encodingLength; baseNumberingScheme; theNumberToEncode) --> Text

// Historical version of method, no longer used

// Parameters: 
//   $1 : Longint : Encoding Length
//   $2 : Text : Base Numbering Scheme
//   $3 : Longint : The Number to Encode

// Returns: 
//   $0 : Text : Returned Value

// Created by Wayne Stewart
//     wayne@4dsupport.guru
// Based on work ©2004 Giovanni Porcari
// ----------------------------------------------------
If (False:C215)
	TS_Encode
End if 

// Parameters
C_LONGINT:C283($EncodingLength_i;$1)
C_TEXT:C284($BaseNumberingScheme_t;$2)
C_LONGINT:C283($TheNumberToEncode_i;$3)

C_TEXT:C284($0;$ReturnedValue_t)

// Local Variables
C_TEXT:C284($Key_t)
C_LONGINT:C283($Base_i;$EncodingLength_i)
C_LONGINT:C283($Number2_r)



$EncodingLength_i:=$1
$BaseNumberingScheme_t:=$2
$TheNumberToEncode_i:=$3

$Key_t:=TS_KeyValue($BaseNumberingScheme_t)

$Base_i:=Length:C16($Key_t)

If ($TheNumberToEncode_i<0)
	$TheNumberToEncode_i:=(2^32)+$TheNumberToEncode_i
End if 

$ReturnedValue_t:=""

While ($TheNumberToEncode_i>=1)
	$ReturnedValue_t:=$Key_t[[($TheNumberToEncode_i-($Base_i*($TheNumberToEncode_i\$Base_i)))+1]]+$ReturnedValue_t
	$TheNumberToEncode_i:=$TheNumberToEncode_i\$Base_i
End while 

If (Length:C16($ReturnedValue_t)>$EncodingLength_i)
	$ReturnedValue_t:=""
Else 
	$ReturnedValue_t:=($Key_t[[1]]*($EncodingLength_i-Length:C16($ReturnedValue_t)))+$ReturnedValue_t
End if 

$0:=$ReturnedValue_t
