//%attributes = {"invisible":true}
// Historical version of method, no longer used


C_TEXT:C284($BaseNumbering_t;$1)
C_TEXT:C284($0;$ReturnedValue_t)

$BaseNumbering_t:=$1

Case of 
	: ($1="/36")
		$ReturnedValue_t:=TS Base 36 Key
		
	: ($1="/16")
		$ReturnedValue_t:=TS Base 16 Key
		
	: ($1="/10")
		$ReturnedValue_t:=TS Base 10 Key
		
	: ($1="/8")
		$ReturnedValue_t:=TS Base 8 Key
		
	: ($1="/2")
		$ReturnedValue_t:=TS Base 2 Key
	Else 
		$ReturnedValue_t:=$1
End case 

$0:=$ReturnedValue_t