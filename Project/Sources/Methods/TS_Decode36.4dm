//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: TS_Decode36 (TimeStamp) --> Longint

// Decodes a number that has been formatted in Base 36

// Access: Shared

// Parameters: 
//   $1 : Text : A Base 36 number

// Returns: 
//   $0 : Longint : The number

// Created by Wayne Stewart (2021-08-15T14:00:00Z)
//     wayne@4dsupport.guru
// ----------------------------------------------------

#DECLARE($theTextToDecode_t : Text)\
->$Result_i : Integer

var $Base_i;$EncodingLength_i;$j;$k : Integer
var $Key_t : Text

$Key_t:=TS Base 36 Key

$Base_i:=Length:C16($Key_t)
$EncodingLength_i:=Length:C16($theTextToDecode_t)

$Result_i:=0

$k:=1
For ($j;$EncodingLength_i;1;-1)
	$Result_i:=$Result_i+((Position:C15($theTextToDecode_t[[$j]];$Key_t)-1)*$k)
	$k:=$k*$Base_i
End for 

$0:=$Result_i