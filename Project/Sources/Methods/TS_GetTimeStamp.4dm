//%attributes = {"invisible":true,"executedOnServer":true}
// ----------------------------------------------------
// Project Method: TS_GetTimeStamp

// Method Type:    Shared

// Parameters:
C_TEXT:C284($BracketOfValues_t;$1)
C_LONGINT:C283($DateTimeDecrement_i;$2)
C_TEXT:C284($0;$ReturnedValue_t)

// Interprocess Variables:
C_LONGINT:C283(<>TS_NoOfTimesCalledThisSecond_i)
C_LONGINT:C283(<>TS_SecondLastCalled_i)

// Process Variables:

// Local Variables:
C_LONGINT:C283($Position_i;$Range_i;$Base_i;$Station_i;$Temp_i)
C_LONGINT:C283($DateTimeDecrement_i)
//C_TEXT(TS Semaphore)
C_LONGINT:C283($Days_i;$Seconds_i;$Num;$j;$Counter_i;$Start_i)

C_TEXT:C284($UUID_t)

// Created by Wayne Stewart (Sep 27, 2013)
//     waynestewart@mac.com

//  Unashamedly stolen from Giovanni Porcari   
//    who posted it on the iNUG

//From: Giovanni Porcari
//Date: Wednesday, 8 December 2004
//Subject: Unique Identifier Suggestions
//To: 4D iNUG Technical<4D_Tech@lists.4dinug.org>

//  My version has been tweaked for easier readbility
//    and broken up into a few methods rather than keep calling itself
// ----------------------------------------------------


$Station_i:=0
$Base_i:=1
$Range_i:=800000

If (Count parameters:C259>=1)
	$BracketOfValues_t:=$1
	If ($BracketOfValues_t#"")  //  "23/100"  =>  Find items in 23rd bracket
		$Position_i:=Position:C15("/";$BracketOfValues_t)  //  3
		$Base_i:=Num:C11(Substring:C12($BracketOfValues_t;$Position_i+1))  //  100
		$Station_i:=Num:C11(Substring:C12($BracketOfValues_t;1;$Position_i-1))  //  23
		If ($Base_i<$Station_i)  // if they have the order the wrong way round
			$Temp_i:=$Base_i
			$Base_i:=$Station_i
			$Station_i:=$Base_i
		End if 
		$Range_i:=Int:C8((800000/$Base_i)+0.999)  //  Now discover how many elements per bracket
		
	End if 
End if 

If (Count parameters:C259>=2)
	$DateTimeDecrement_i:=$2
	
Else 
	$DateTimeDecrement_i:=0
End if 

If (Not:C34(Semaphore:C143(TS Semaphore;TS Semaphore Timeout)))  //  Semaphore time out can be tweaked by adjusting xliff
	Repeat 
		$Days_i:=Current date:C33-!1990-01-01!
		$Seconds_i:=(Current time:C178-$DateTimeDecrement_i)
		
		//BEGIN added to cater for day differences
		If ($DateTimeDecrement_i#0)
			$Days_i:=$Days_i+Int:C8($Seconds_i/86400)
			$Seconds_i:=$Seconds_i-(Int:C8($Seconds_i/86400)*86400)
		End if 
		//END  added to cater for day differences
		
		If ($Seconds_i#<>TS_SecondLastCalled_i)
			<>TS_SecondLastCalled_i:=$Seconds_i
			<>TS_NoOfTimesCalledThisSecond_i:=1
			If ($Base_i=1)
				<>TS_NoOfTimesCalledThisSecond_i:=<>TS_NoOfTimesCalledThisSecond_i+(Random:C100%10000)
			End if 
		Else 
			<>TS_NoOfTimesCalledThisSecond_i:=<>TS_NoOfTimesCalledThisSecond_i+1
		End if 
	Until (<>TS_NoOfTimesCalledThisSecond_i<=$Range_i)
	$Counter_i:=<>TS_NoOfTimesCalledThisSecond_i+($Station_i*$Range_i)
	CLEAR SEMAPHORE:C144(TS Semaphore)
	
	$ReturnedValue_t:=TS_Encode36(3;$Days_i)
	$ReturnedValue_t:=$ReturnedValue_t+TS_Encode36(4;($Seconds_i*18)+($Counter_i\46000))
	$ReturnedValue_t:=$ReturnedValue_t+TS_Encode36(3;($Counter_i%46000))
	
	
Else 
	//  Ignore this:  I was chasing my tail for a while :(
	//  Leaving code here in case it proves relevant again
	
	
	// The semaphore timed out this shouldn't really happen :(
	
	// I've seen it happen when there are
	//  hundreds of processes running simultaneously
	//  in one test with 750 processes there was
	//  an incidence of 2% with a 10 tick time out on the semaphore
	
	// Increasing the delay to 120 ticks (2 seconds!)
	//  Incidence of time out was reduced to 1%
	//  still too high and by now the delay
	//  is really unacceptable
	
	// If this is the case the returned value will be "0000000000"
	//  Not very helpful :(
	
	// So swap to a different technique
	//   possible to get ids out of order with this version
	
	//<>TS_NoOfTimesCalledThisSecond_i:=<>TS_NoOfTimesCalledThisSecond_i+1
	
	//$Days_i:=Current date-!2000-01-01!
	//$Seconds_i:=(Current time-$DateTimeDecrement_i)
	
	////BEGIN added to cater for day differences
	//If ($DateTimeDecrement_i#0)
	//$Days_i:=$Days_i+Int($Seconds_i/86400)
	//$Seconds_i:=$Seconds_i-(Int($Seconds_i/86400)*86400)
	//End if 
	////END  added to cater for day differences
	
	//$Counter_i:=Milliseconds%$Range_i
	//$Counter_i:=<>TS_NoOfTimesCalledThisSecond_i+($Station_i*$Range_i)
	
	//$ReturnedValue_t:=TS_Encode36(3;$Days_i)
	//$ReturnedValue_t:=$ReturnedValue_t+TS_Encode36(4;($Seconds_i*18)+($Counter_i\46000))
	
	//$UUID_t:=Generate UUID
	//$Start_i:=Random%16
	//$UUID_t:=Substring($UUID_t;$Start_i;3)
	
	
	////$ReturnedValue_t:=$ReturnedValue_t+TS_Encode36 (3;($Counter_i%46000))
	//$ReturnedValue_t:=$ReturnedValue_t+$UUID_t  //TS_Encode36 (3;($Counter_i%46000))
	
	//$ReturnedValue_t[[1]]:="+"  //  Just so I can tell
	
End if 


$0:=$ReturnedValue_t

