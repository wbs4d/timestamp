//%attributes = {"invisible":true,"shared":true,"executedOnServer":true,"preemptive":"capable"}
// ----------------------------------------------------
// Project Method: TS_GetTimeStamp

// Method Type:    Shared

// Parameters:

C_TEXT:C284($BracketOfValues_t;$1)
C_LONGINT:C283($DateTimeDecrement_i;$2)
C_TEXT:C284($0;$ReturnedValue_t)

// Created by Wayne Stewart (Sep 27, 2013)
//     waynestewart@mac.com

//  Unashamedly stolen from Giovanni Porcari   
//    who posted it on the iNUG

//From: Giovanni Porcari
//Date: Wednesday, 8 December 2004
//Subject: Unique Identifier Suggestions
//To: 4D iNUG Technical<4D_Tech@lists.4dinug.org>

//  My version has been tweaked for easier readbility
//    and broken up into a few methods rather than keep calling itself
// ----------------------------------------------------

C_LONGINT:C283($numParameters_i)
C_OBJECT:C1216($signal_o)
C_BOOLEAN:C305($signaled_b)
$numParameters_i:=Count parameters:C259
$ReturnedValue_t:=""

Case of 
	: ($numParameters_i=1)
		$BracketOfValues_t:=$1
		
	: ($numParameters_i=2)
		$BracketOfValues_t:=$1
		$DateTimeDecrement_i:=$2
		
End case 



$signal_o:=New signal:C1641()

Use ($signal_o)
	$signal_o.bracket:=$BracketOfValues_t
	$signal_o.dt:=$DateTimeDecrement_i
End use 

CALL WORKER:C1389("theStamper";"TS_Get_PE";$signal_o)

$signaled_b:=$signal_o.wait()

If ($signaled_b)
	$ReturnedValue_t:=$signal_o.timeStamp
	
End if 


$0:=$ReturnedValue_t

