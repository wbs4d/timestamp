//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: TS_Decode --> ReturnType

// Decodes a number that has been formatted in 
// a different Base

// Access: Shared

// Parameters: 
//   $1 : text : The base please use one of the following constants
// TS Base 2 Key : : Base two
// TS Base 8 Key : : Base eight
// TS Base 10 Key : : Now you're being silly!
// TS Base 16 Key : : Base 16
// TS Base 36 Key : : Base 36 (A-Z,0-9)
//   $2 : Text : The encoded number

// Returns: 
//   $0 : Longint : The decoded number

// Created by Wayne Stewart (2021-08-13T14:00:00Z)
//     wayne@4dsupport.guru
// ----------------------------------------------------


#DECLARE($BaseNumberingScheme_t : Text;\
$theTextToDecode_t : Text)\
->$Result_i : Integer

var $Base_i;$EncodingLength_i;$j;$k : Integer
var $Key_t : Text

$Key_t:=TS_KeyValue($BaseNumberingScheme_t)

$Base_i:=Length:C16($Key_t)
$EncodingLength_i:=Length:C16($theTextToDecode_t)

$Result_i:=0

$k:=1
For ($j;$EncodingLength_i;1;-1)
	$Result_i:=$Result_i+((Position:C15($theTextToDecode_t[[$j]];$Key_t)-1)*$k)
	$k:=$k*$Base_i
End for 
