//%attributes = {"invisible":true,"preemptive":"capable"}
C_OBJECT:C1216($1)
C_TEXT:C284($0)

C_TEXT:C284($BracketOfValues_t)
C_TEXT:C284($ReturnedValue_t)
C_LONGINT:C283($Position_i; $Range_i; $Base_i; $Station_i; $Temp_i)
C_LONGINT:C283($DateTimeDecrement_i)
C_LONGINT:C283($Days_i; $Seconds_i; $Num; $j; $Counter_i; $Start_i)

TS_Init

$BracketOfValues_t:=$1.bracket
$DateTimeDecrement_i:=$1.dt

$Station_i:=0
$Base_i:=1
$Range_i:=800000

If ($BracketOfValues_t#"")  //  "23/100"  =>  Find items in 23rd bracket
	$Position_i:=Position:C15("/"; $BracketOfValues_t)  //  3
	$Base_i:=Num:C11(Substring:C12($BracketOfValues_t; $Position_i+1))  //  100
	$Station_i:=Num:C11(Substring:C12($BracketOfValues_t; 1; $Position_i-1))  //  23
	If ($Base_i<$Station_i)  // if they have the order the wrong way round
		$Temp_i:=$Base_i
		$Base_i:=$Station_i
		$Station_i:=$Base_i
	End if 
	$Range_i:=Int:C8((800000/$Base_i)+0.999)  //  Now discover how many elements per bracket
	
End if 

Repeat 
	$Days_i:=Current date:C33-!1990-01-01!
	$Seconds_i:=(Current time:C178-$DateTimeDecrement_i)
	
	//BEGIN added to cater for day differences
	If ($DateTimeDecrement_i#0)
		$Days_i:=$Days_i+Int:C8($Seconds_i/86400)
		$Seconds_i:=$Seconds_i-(Int:C8($Seconds_i/86400)*86400)
	End if 
	//END  added to cater for day differences
	
	If ($Seconds_i#Storage:C1525.TS.lastCall)
		Use (Storage:C1525.TS)
			Storage:C1525.TS.lastCall:=$Seconds_i
			Storage:C1525.TS.numberOfCalls:=1
		End use 
		If ($Base_i=1)
			Use (Storage:C1525.TS)
				Storage:C1525.TS.numberOfCalls:=Storage:C1525.TS.numberOfCalls+(Random:C100%10000)
			End use 
		End if 
	Else 
		Use (Storage:C1525.TS)
			Storage:C1525.TS.numberOfCalls:=Storage:C1525.TS.numberOfCalls+1
		End use 
	End if 
Until (Storage:C1525.TS.numberOfCalls<=$Range_i)

$Counter_i:=Storage:C1525.TS.numberOfCalls+($Station_i*$Range_i)

$ReturnedValue_t:=TS_Encode36(3; $Days_i)
$ReturnedValue_t:=$ReturnedValue_t+TS_Encode36(4; ($Seconds_i*18)+($Counter_i\46000))
$ReturnedValue_t:=$ReturnedValue_t+TS_Encode36(3; ($Counter_i%46000))

$0:=$ReturnedValue_t

//Use ($1)
//$1.timeStamp:=$ReturnedValue_t
//End use 

//$1.trigger()
