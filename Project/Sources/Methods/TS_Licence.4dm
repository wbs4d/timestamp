//%attributes = {"invisible":true,"shared":true}
//  Copyright ©2004 Giovanni Porcari
//
//  Permission is hereby granted, free of charge, to any
//  person obtaining a copy of this software and associated
//  documentation files (the "Software"), to deal in the
//  Software without restriction, including without
//  limitation the rights to use, copy, modify, merge,
//  publish, distribute, subLicence, and/or sell copies of
//  the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the
//  following conditions:
//
//  The above copyright notice and this permission notice
//  shall be included in all copies or substantial portions
//  of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
//  ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
//  TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
//  SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
//  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
//  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//  OTHER DEALINGS IN THE SOFTWARE.


// TS_Licence
// Created by Wayne Stewart (Sep 30, 2013)
//  Method is an autostart type
//     waynestewart@mac.com

C_LONGINT:C283($1; $ProcessID_i; $window_i)

If (False:C215)  //  Copy this to your Compiler Method!
	C_LONGINT:C283(TS_Licence; $1)
End if 

If (Count parameters:C259=1)
	$window_i:=Open form window:C675("Licence"; Palette form window:K39:9; Horizontally centered:K39:1; Vertically centered:K39:4)
	SET WINDOW TITLE:C213("Licence Acknowledgement")
	DIALOG:C40("Licence")
	CLOSE WINDOW:C154($window_i)
	
Else 
	// This version allows for one unique process
	$ProcessID_i:=New process:C317(Current method name:C684; 0; Current method name:C684; 0; *)
	RESUME PROCESS:C320($ProcessID_i)
	SHOW PROCESS:C325($ProcessID_i)
	BRING TO FRONT:C326($ProcessID_i)
End if 

