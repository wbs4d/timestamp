//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: TS_Encode36 --> ReturnType

// Description

// Access: Shared

// Parameters: 
//   $1 : Type : Description
//   $x : Type : Description (optional)

// Returns: 
//   $0 : Type : Description

// Created by Wayne Stewart (2021-07-30T14:00:00Z)
//     wayne@4dsupport.guru
// ----------------------------------------------------


// ----------------------------------------------------
// User name (OS): Wayne Stewart
// Date and time: Jan 14, 2011, 15:58:48
// ----------------------------------------------------
// Method: TS_Encode
// Description
// 
//
// Parameters
C_LONGINT:C283($EncodingLength_i; $1)
C_LONGINT:C283($TheNumberToEncode_i; $2)

C_TEXT:C284($0; $ReturnedValue_t)

// Local Variables
C_TEXT:C284($Key_t)
C_LONGINT:C283($Base_i; $EncodingLength_i)
C_LONGINT:C283($Number2_r)

//  Unashamedly stolen from Giovanni Porcari   
//    who posted it on the iNUG

//From: Giovanni Porcari
//Date: Wednesday, 8 December 2004
//Subject: Unique Identifier Suggestions
//To: 4D iNUG Technical<4D_Tech@lists.4dinug.org>

//  My version has been tweaked for easier readbility
// and broken up into a few methods rather than keep calling itself
// ----------------------------------------------------

$EncodingLength_i:=$1
$TheNumberToEncode_i:=$2

$Key_t:=TS Base 36 Key

$Base_i:=Length:C16($Key_t)

If ($TheNumberToEncode_i<0)
	$TheNumberToEncode_i:=(2^32)+$TheNumberToEncode_i
End if 

$ReturnedValue_t:=""

While ($TheNumberToEncode_i>=1)
	$ReturnedValue_t:=$Key_t[[($TheNumberToEncode_i-($Base_i*($TheNumberToEncode_i\$Base_i)))+1]]+$ReturnedValue_t
	$TheNumberToEncode_i:=$TheNumberToEncode_i\$Base_i
End while 

If (Length:C16($ReturnedValue_t)>$EncodingLength_i)
	$ReturnedValue_t:=""
Else 
	$ReturnedValue_t:=("0"*($EncodingLength_i-Length:C16($ReturnedValue_t)))+$ReturnedValue_t
End if 


$0:=$ReturnedValue_t
