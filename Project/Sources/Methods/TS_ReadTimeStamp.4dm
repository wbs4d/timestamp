//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: TS_ReadTimeStamp (Base36 timestamp)--> Text

// Returns in ISO date format the date and time embedded in a timestamp
// Timestamp: 8WUKLM02VQ
// ISO format: 2021-08-16T14:49:56

// Access: Shared

// Parameters: 
//   $1 : Text : A time stamp encoded by TS_GetTimeStamp

// Returns: 
//   $0 : Text : date and time in ISO format

// Created by Wayne Stewart (2021-08-15T14:00:00Z)
//     wayne@4dsupport.guru
// ----------------------------------------------------

#DECLARE($theTimeStamp_t : Text)->$ISOTimeStamp_t : Text

var $numberOfDays_i;$numberOfSeconds_i : Integer
var $DateSection_t;$TimeSection_t : Text
var $theDate_d : Date

$DateSection_t:=Substring:C12($theTimeStamp_t;1;3)
$TimeSection_t:=Substring:C12($theTimeStamp_t;4;4)

$numberOfDays_i:=TS_Decode36($DateSection_t)
$numberOfSeconds_i:=TS_Decode36($TimeSection_t)/18

$theDate_d:=!1990-01-01!+$numberOfDays_i

$ISOTimeStamp_t:=String:C10(Year of:C25($theDate_d);"0000")+"-"+String:C10(Month of:C24($theDate_d);"00")+"-"+String:C10(Day of:C23($theDate_d);"00")+"T"+Time string:C180($numberOfSeconds_i)




