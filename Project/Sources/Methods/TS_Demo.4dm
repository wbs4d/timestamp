//%attributes = {"shared":true}
// Demo
// Created by Wayne Stewart (Jul 17, 2013)
//  Method is an autostart type
//     waynestewart@mac.com

C_LONGINT:C283($1; $ProcessID_i)

If (False:C215)  //  Copy this to your Compiler Method!
	C_LONGINT:C283(TS_Demo; $1)
End if 

If (Count parameters:C259=1)
	C_LONGINT:C283($win)
	
	$win:=Open form window:C675("Demo"; Plain form window:K39:10; On the left:K39:2; Vertically centered:K39:4)
	SET WINDOW TITLE:C213("Co-op or Preemptive")
	DIALOG:C40("Demo"; New object:C1471("windowID"; $win))
	CLOSE WINDOW:C154($win)
Else 
	// This version allows for any number of processes
	// $ProcessID_i:=New Process(Current method name;128*1024;Current method name;0)
	// This version allows for one unique process
	$ProcessID_i:=New process:C317(Current method name:C684; 128*1024; Current method name:C684; 0; *)
	RESUME PROCESS:C320($ProcessID_i)
	SHOW PROCESS:C325($ProcessID_i)
	BRING TO FRONT:C326($ProcessID_i)
End if 

