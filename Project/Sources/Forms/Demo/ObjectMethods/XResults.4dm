

C_POINTER:C301($Col_01_ptr; $Col_02_ptr; $Col_03_ptr; $Col_05_ptr; $Col_04_ptr; $preemptive_ptr; $signal_ptr)
C_LONGINT:C283($Size; $MaxSize; $preemptive_i; $signal_i)
C_LONGINT:C283($Start)
var $template_t; $buttonLabel_t : Text

$template_t:="Generate X Results"

$MaxSize:=1000

$buttonLabel_t:=Replace string:C233($template_t; "X"; String:C10($MaxSize*5; "###,##0"))

Case of 
	: (Form event code:C388=On Clicked:K2:4)
		
		
		
		
		
		$preemptive_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "Preemptive")
		$signal_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "Signal")
		$preemptive_i:=$preemptive_ptr->
		$signal_i:=1
		
		$Col_01_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "Col_01")
		ARRAY TEXT:C222($Col_01_ptr->; $MaxSize)
		
		$Col_02_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "Col_02")
		ARRAY TEXT:C222($Col_02_ptr->; $MaxSize)
		
		$Col_03_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "Col_03")
		ARRAY TEXT:C222($Col_03_ptr->; $MaxSize)
		
		$Col_04_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "Col_04")
		ARRAY TEXT:C222($Col_04_ptr->; $MaxSize)
		
		$Col_05_ptr:=OBJECT Get pointer:C1124(Object named:K67:5; "Col_05")
		ARRAY TEXT:C222($Col_05_ptr->; $MaxSize)
		
		$Start:=Milliseconds:C459
		
		
		Case of 
			: ($preemptive_i=1) & ($signal_i=1)
				For ($Size; 1; $MaxSize)
					$Col_01_ptr->{$Size}:=TS_GetTimeStamp_PE("1/5")
					$Col_02_ptr->{$Size}:=TS_GetTimeStamp_PE("2/5")
					$Col_03_ptr->{$Size}:=TS_GetTimeStamp_PE("3/5")
					$Col_04_ptr->{$Size}:=TS_GetTimeStamp_PE("4/5")
					$Col_05_ptr->{$Size}:=TS_GetTimeStamp_PE("5/5")
				End for 
				
			: ($preemptive_i=1)
				
				
				
				
			Else 
				
				For ($Size; 1; $MaxSize)
					$Col_01_ptr->{$Size}:=TS_GetTimeStamp("1/5")
					$Col_02_ptr->{$Size}:=TS_GetTimeStamp("2/5")
					$Col_03_ptr->{$Size}:=TS_GetTimeStamp("3/5")
					$Col_04_ptr->{$Size}:=TS_GetTimeStamp("4/5")
					$Col_05_ptr->{$Size}:=TS_GetTimeStamp("5/5")
				End for 
		End case 
		
		
		
		
		
		If ($preemptive_ptr->=1)
			
		Else 
			
		End if 
		
		
		
		ALERT:C41("Duration: "+String:C10(Milliseconds:C459-$Start)+" ms.")
		
		
	: (Form event code:C388=On Load:K2:1)
		
		OBJECT SET TITLE:C194(*; "XResults"; $buttonLabel_t)
End case 





