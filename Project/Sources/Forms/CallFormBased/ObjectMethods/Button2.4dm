C_POINTER:C301($Col_01_ptr;$Col_02_ptr;$Col_03_ptr;$Col_05_ptr;$Col_04_ptr;$button_ptr)
C_LONGINT:C283($Size;$MaxSize)

$MaxSize:=100000
$button_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"Preemptive")

$Col_01_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"Col_01")
ARRAY TEXT:C222($Col_01_ptr->;$MaxSize)

$Col_02_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"Col_02")
ARRAY TEXT:C222($Col_02_ptr->;$MaxSize)

$Col_03_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"Col_03")
ARRAY TEXT:C222($Col_03_ptr->;$MaxSize)

$Col_04_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"Col_04")
ARRAY TEXT:C222($Col_04_ptr->;$MaxSize)

$Col_05_ptr:=OBJECT Get pointer:C1124(Object named:K67:5;"Col_05")
ARRAY TEXT:C222($Col_05_ptr->;$MaxSize)

C_LONGINT:C283($Start)

$Start:=Milliseconds:C459+1000

$Size:=0
If ($button_ptr->=1)
	Repeat 
		$Size:=$Size+1
		$Col_01_ptr->{$Size}:=TS_GetTimeStamp_PE("1/5")
		$Col_02_ptr->{$Size}:=TS_GetTimeStamp_PE("2/5")
		$Col_03_ptr->{$Size}:=TS_GetTimeStamp_PE("3/5")
		$Col_04_ptr->{$Size}:=TS_GetTimeStamp_PE("4/5")
		$Col_05_ptr->{$Size}:=TS_GetTimeStamp_PE("5/5")
	Until (Milliseconds:C459>$Start) | ($size>=$MaxSize)
Else 
	Repeat 
		$Size:=$Size+1
		$Col_01_ptr->{$Size}:=TS_GetTimeStamp("1/5")
		$Col_02_ptr->{$Size}:=TS_GetTimeStamp("2/5")
		$Col_03_ptr->{$Size}:=TS_GetTimeStamp("3/5")
		$Col_04_ptr->{$Size}:=TS_GetTimeStamp("4/5")
		$Col_05_ptr->{$Size}:=TS_GetTimeStamp("5/5")
	Until (Milliseconds:C459>$Start) | ($size>=$MaxSize)
End if 

ARRAY TEXT:C222($Col_01_ptr->;$Size)
ARRAY TEXT:C222($Col_02_ptr->;$Size)
ARRAY TEXT:C222($Col_03_ptr->;$Size)
ARRAY TEXT:C222($Col_04_ptr->;$Size)
ARRAY TEXT:C222($Col_05_ptr->;$Size)



ALERT:C41("Number of calculations: "+String:C10($size))